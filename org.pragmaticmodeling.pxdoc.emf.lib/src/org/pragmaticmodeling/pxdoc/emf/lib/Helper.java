package org.pragmaticmodeling.pxdoc.emf.lib;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;

public class Helper {

	/**
	 * Introduced to avoid SWT invalid thread access encountered with Papyrus.
	 * @param laberProvider
	 * @param object
	 * @return
	 */
	public static Image getImage(ILabelProvider laberProvider, Object object) {
		Set<Image> result = new HashSet<Image>();
		Display.getDefault().syncExec(new Runnable() {
			
			@Override
			public void run() {
				result.add(laberProvider.getImage(object));
			}
			
		});
		if (result.isEmpty()) {
			return null;
		} else {
			return result.iterator().next();
		}
	}
}
