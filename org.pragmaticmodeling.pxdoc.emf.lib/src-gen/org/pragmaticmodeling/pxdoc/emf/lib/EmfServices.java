package org.pragmaticmodeling.pxdoc.emf.lib;

import com.google.common.collect.Iterables;
import fr.pragmaticmodeling.pxdoc.Bold;
import fr.pragmaticmodeling.pxdoc.Bookmark;
import fr.pragmaticmodeling.pxdoc.Cell;
import fr.pragmaticmodeling.pxdoc.ContainerElement;
import fr.pragmaticmodeling.pxdoc.HeadingN;
import fr.pragmaticmodeling.pxdoc.Hyperlink;
import fr.pragmaticmodeling.pxdoc.Measure;
import fr.pragmaticmodeling.pxdoc.Row;
import fr.pragmaticmodeling.pxdoc.ShadingPattern;
import fr.pragmaticmodeling.pxdoc.StyleApplication;
import fr.pragmaticmodeling.pxdoc.Table;
import fr.pragmaticmodeling.pxdoc.Underline;
import fr.pragmaticmodeling.pxdoc.UnderlineType;
import fr.pragmaticmodeling.pxdoc.UnitKind;
import fr.pragmaticmodeling.pxdoc.VerticalAlignmentType;
import fr.pragmaticmodeling.pxdoc.runtime.AbstractPxDocModule;
import fr.pragmaticmodeling.pxdoc.runtime.IPxDocGenerator;
import fr.pragmaticmodeling.pxdoc.runtime.PxDocGenerationException;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.swt.graphics.Image;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.pragmaticmodeling.pxdoc.common.lib.CommonServices;
import org.pragmaticmodeling.pxdoc.common.lib.IDescriptionProvider;
import org.pragmaticmodeling.pxdoc.diagrams.Diagram;
import org.pragmaticmodeling.pxdoc.diagrams.IDiagramProvider;
import org.pragmaticmodeling.pxdoc.diagrams.IDiagramQueryBuilder;
import org.pragmaticmodeling.pxdoc.diagrams.lib.DiagramServices;
import org.pragmaticmodeling.pxdoc.emf.lib.DocKind;
import org.pragmaticmodeling.pxdoc.emf.lib.Helper;
import org.pragmaticmodeling.pxdoc.images.lib.ImageHelper;

@SuppressWarnings("all")
public class EmfServices extends AbstractPxDocModule {
  private Logger logger = Logger.getLogger(getClass());
  
  public Set<EStructuralFeature> excludedFeatures = new HashSet<EStructuralFeature>();
  
  public Set<EClass> excludedEClasses = new HashSet<EClass>();
  
  public ComposedAdapterFactory composedAdapterFactory = new ComposedAdapterFactory(
    ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
  
  public ILabelProvider emfLabelProvider = new AdapterFactoryLabelProvider(this.composedAdapterFactory);
  
  public ITreeContentProvider emfContentProvider = new AdapterFactoryContentProvider(this.composedAdapterFactory);
  
  public Map<Image, BufferedImage> convertedIcons = new HashMap<Image, BufferedImage>();
  
  private List<String> _stylesToBind;
  
  public CommonServices _CommonServices = (CommonServices)getGenerator().getModule(CommonServices.class);
  
  public String getDescription(final Object element) {
    return _CommonServices.getDescription(element);
  }
  
  public IDescriptionProvider getDescriptionProvider() {
    return _CommonServices.getDescriptionProvider();
  }
  
  public boolean getSuggestImprovements() {
    return _CommonServices.getSuggestImprovements();
  }
  
  public boolean hasBookmark(final Object object) {
    return _CommonServices.hasBookmark(object);
  }
  
  public boolean isEmptyDocumentation(final String text) {
    return _CommonServices.isEmptyDocumentation(text);
  }
  
  public void setDescriptionProvider(final IDescriptionProvider descProvider) {
    _CommonServices.setDescriptionProvider(descProvider);
  }
  
  public void setSuggestImprovements(final boolean value) {
    _CommonServices.setSuggestImprovements(value);
  }
  
  public String suffixedFile(final String doc, final String suffix) {
    return _CommonServices.suffixedFile(doc, suffix);
  }
  
  public String validBookmark(final Object object) {
    return _CommonServices.validBookmark(object);
  }
  
  public DiagramServices _DiagramServices = (DiagramServices)getGenerator().getModule(DiagramServices.class);
  
  public IDiagramProvider<? extends IDiagramQueryBuilder> getDiagramProvider() {
    return _DiagramServices.getDiagramProvider();
  }
  
  public IDiagramQueryBuilder queryDiagrams(final Object namespace) {
    return _DiagramServices.queryDiagrams(namespace);
  }
  
  public void setDiagramProvider(final IDiagramProvider<? extends IDiagramQueryBuilder> diagramProvider) {
    _DiagramServices.setDiagramProvider(diagramProvider);
  }
  
  public EmfServices(final IPxDocGenerator generator) {
    super(generator);
  }
  
  /**
   * Get the children of the object
   */
  public List<EObject> getChildren(final EObject eObject) {
    return IterableExtensions.<EObject>toList(Iterables.<EObject>filter(((Iterable<?>)Conversions.doWrapArray(this.emfContentProvider.getChildren(eObject))), EObject.class));
  }
  
  /**
   * Get the label of the object
   */
  public String getText(final EObject eObject) {
    return this.emfLabelProvider.getText(eObject);
  }
  
  /**
   * Set the Label Provider
   */
  public void setLabelProvider(final ILabelProvider labelProvider) {
    this.emfLabelProvider = labelProvider;
  }
  
  /**
   * Initialize containment features that should not produce numbered titles.
   */
  public Set<EClass> getExcludedEClasses() {
    return this.excludedEClasses;
  }
  
  /**
   * Initialize features to exclude from the generated document.
   */
  public Set<EStructuralFeature> getExcludedFeatures() {
    return this.excludedFeatures;
  }
  
  /**
   * Returns the name of a EFeature
   */
  public String getPrettyName(final EStructuralFeature feature) {
    return feature.getName();
  }
  
  /**
   * Check is the current EFeature is a EProperty
   */
  public boolean isEProperty(final EStructuralFeature feature, final EObject eObject) {
    boolean isProperty = false;
    boolean _contains = this.excludedFeatures.contains(feature);
    boolean _not = (!_contains);
    if (_not) {
      if ((feature instanceof EAttribute)) {
        String _name = ((EAttribute)feature).getEType().getName();
        if (_name != null) {
          switch (_name) {
            case "EString":
              boolean _isMany = ((EAttribute)feature).isMany();
              if (_isMany) {
                isProperty = (eObject.eIsSet(feature) && (!((List<String>) eObject.eGet(feature)).isEmpty()));
              } else {
                isProperty = (eObject.eIsSet(feature) && 
                  (!this.isEmptyDocumentation(((String) eObject.eGet(feature)))));
              }
              break;
            case "String":
              boolean _isMany_1 = ((EAttribute)feature).isMany();
              if (_isMany_1) {
                isProperty = (eObject.eIsSet(feature) && (!((List<String>) eObject.eGet(feature)).isEmpty()));
              } else {
                isProperty = (eObject.eIsSet(feature) && 
                  (!this.isEmptyDocumentation(((String) eObject.eGet(feature)))));
              }
              break;
            default:
              {
                Object oValue = eObject.eGet(feature);
                if ((oValue != null)) {
                  String sValue = ("" + oValue);
                  boolean _isEmptyDocumentation = this.isEmptyDocumentation(sValue);
                  return (!_isEmptyDocumentation);
                }
              }
              break;
          }
        } else {
          {
            Object oValue = eObject.eGet(feature);
            if ((oValue != null)) {
              String sValue = ("" + oValue);
              boolean _isEmptyDocumentation = this.isEmptyDocumentation(sValue);
              return (!_isEmptyDocumentation);
            }
          }
        }
      } else {
        if (((feature instanceof EReference) && (!((EReference) feature).isContainment()))) {
          isProperty = eObject.eIsSet(feature);
        }
      }
    }
    return isProperty;
  }
  
  /**
   * Properties that should be included in the generated document, in the order they are defined.
   * - Properties having no values are excluded.
   * - the excludedFeatures set is taken into account to filter indesirable features.
   */
  public List<EStructuralFeature> getProperties(final EObject eObject) {
    final Function1<EStructuralFeature, Boolean> _function = (EStructuralFeature eAttr) -> {
      return Boolean.valueOf(this.isEProperty(eAttr, eObject));
    };
    return IterableExtensions.<EStructuralFeature>toList(IterableExtensions.<EStructuralFeature>filter(eObject.eClass().getEAllStructuralFeatures(), _function));
  }
  
  /**
   * Returns the comment for the hyperlinks: either the description of the target element or "No description"
   */
  public String getHyperlinkComment(final EObject eObject) {
    String _xblockexpression = null;
    {
      String doc = this.getDescription(eObject);
      String _xifexpression = null;
      boolean _isEmptyDocumentation = this.isEmptyDocumentation(doc);
      if (_isEmptyDocumentation) {
        return "No description";
      } else {
        _xifexpression = doc;
      }
      _xblockexpression = _xifexpression;
    }
    return _xblockexpression;
  }
  
  /**
   * Specify if a sub-element shall be descried in a sub-chapter (e.g. section with a title) of its container, or not
   */
  public boolean isExcluded(final EObject element) {
    EClass eClass = element.eClass();
    for (final EClass exEClass : this.excludedEClasses) {
      boolean _isSuperTypeOf = exEClass.isSuperTypeOf(eClass);
      if (_isSuperTypeOf) {
        return true;
      }
    }
    return false;
  }
  
  /**
   * Convert EMF icons into an AWT object (only type of image object than pxDoc can use)
   * Refer to the pxDoc Images library
   */
  public BufferedImage getImage(final Object object) {
    Image img = Helper.getImage(this.emfLabelProvider, object);
    boolean _containsKey = this.convertedIcons.containsKey(img);
    boolean _not = (!_containsKey);
    if (_not) {
      BufferedImage result = ImageHelper.convertToAWT(img);
      this.convertedIcons.put(img, result);
    }
    return this.convertedIcons.get(img);
  }
  
  public void error(final String message) {
    logger.error(message);
  }
  
  public void info(final String message) {
    logger.info(message);
  }
  
  /**
   * Include an element as a hyperlink and with its icon, in a bullet list
   */
  public void bulletedItem(final ContainerElement parent, final EObject item) throws PxDocGenerationException {
    StyleApplication lObj0 = createStyleApplication(parent, false, getBindedStyle("BulletList"), null, null, null);
    boolean _hasBookmark = this.hasBookmark(item);
    if (_hasBookmark) {
      Hyperlink lObj1 = createHyperlink(lObj0, getBookmarkString(this.validBookmark(item)), this.getHyperlinkComment(item), null);
      iconAndText(lObj1, item);
    } else {
      iconAndText(lObj0, item);
    }
  }
  
  /**
   * A basic description of an element
   */
  public void basicDescription(final ContainerElement parent, final EObject element, final int level) throws PxDocGenerationException {
    HeadingN lObj0 = createHeadingN(parent, level, true);
    boolean _hasBookmark = this.hasBookmark(element);
    if (_hasBookmark) {
      String lObj1 = this.validBookmark(element);
      Bookmark lObj2 = createBookmark(lObj0, getBookmarkString(lObj1));
      iconAndText(lObj2, element);
    } else {
      iconAndText(lObj0, element);
    }
    _CommonServices.smartDocumentation(parent, element);
    List<Diagram> diagrams = this.queryDiagrams(element).notEmpty().execute();
    boolean _isEmpty = diagrams.isEmpty();
    boolean _not = (!_isEmpty);
    if (_not) {
      Iterator<org.pragmaticmodeling.pxdoc.diagrams.Diagram> lObj3 = getIterator(diagrams);
      while (lObj3.hasNext()) {
        _DiagramServices.includeDiagram(parent, lObj3.next());
        
      }
    }
  }
  
  /**
   * Used by nodeAttributeRow to include list of properties in the cell
   */
  public void featureValue(final ContainerElement parent, final EStructuralFeature feature, final EObject eObject) throws PxDocGenerationException {
    boolean _isMany = feature.isMany();
    if (_isMany) {
      Object _eGet = eObject.eGet(feature);
      List<?> values = ((List<?>) _eGet);
      for (final Object value : values) {
        if ((feature instanceof EAttribute)) {
          String sValue = ("" + value);
          StyleApplication lObj0 = createStyleApplication(parent, false, getBindedStyle("BulletList"), null, null, null);
          createText(lObj0, sValue);
        } else {
          if ((value instanceof EObject)) {
            Measure shift1 = createMeasure((float)0.1, UnitKind.CM);
            StyleApplication lObj2 = createStyleApplication(parent, false, getBindedStyle("BulletList"), null, shift1, null);
            boolean _hasBookmark = this.hasBookmark(value);
            if (_hasBookmark) {
              Hyperlink lObj3 = createHyperlink(lObj2, getBookmarkString(this.validBookmark(value)), this.getHyperlinkComment(((EObject)value)), null);
              iconAndText(lObj3, ((EObject)value));
            } else {
              iconAndText(lObj2, ((EObject)value));
            }
          }
        }
      }
    } else {
      Object value_1 = eObject.eGet(feature);
      if ((feature instanceof EAttribute)) {
        String sValue_1 = ("" + value_1);
        StyleApplication lObj4 = createStyleApplication(parent, false, getBindedStyle("BodyText"), null, null, null);
        createText(lObj4, sValue_1);
      } else {
        if ((value_1 instanceof EObject)) {
          StyleApplication lObj5 = createStyleApplication(parent, false, getBindedStyle("BodyText"), null, null, null);
          boolean _hasBookmark_1 = this.hasBookmark(value_1);
          if (_hasBookmark_1) {
            Hyperlink lObj6 = createHyperlink(lObj5, getBookmarkString(this.validBookmark(value_1)), this.getHyperlinkComment(((EObject)value_1)), null);
            iconAndText(lObj6, ((EObject)value_1));
          } else {
            iconAndText(lObj5, ((EObject)value_1));
          }
        }
      }
    }
  }
  
  /**
   * Template used by docWithTables to include one row per attribute
   */
  public void nodeAttributeRow(final ContainerElement parent, final EStructuralFeature feature, final EObject eObject) throws PxDocGenerationException {
    Row lObj0 = createRow(parent, null, false, null, ShadingPattern.NO_VALUE, false, null);
    {
      Cell lObj1 = createCell(lObj0);
      createText(lObj1, this.getPrettyName(feature));
      Cell lObj2 = createCell(lObj0);
      featureValue(lObj2, feature, eObject);
    }
  }
  
  /**
   * Template used by the docWithText template to properly include the list of attributes
   */
  public void propertyFormDescription(final ContainerElement parent, final EStructuralFeature feature, final EObject eObject) throws PxDocGenerationException {
    boolean _isMany = feature.isMany();
    if (_isMany) {
      StyleApplication lObj0 = createStyleApplication(parent, false, getBindedStyle("BodyText"), null, null, null);
      Bold lObj1 = createBold(lObj0);
      Underline lObj2 = createUnderline(lObj1, UnderlineType.WORDS);
      createText(lObj2, this.getPrettyName(feature));
      Object _eGet = eObject.eGet(feature);
      List<?> values = ((List<?>) _eGet);
      for (final Object value : values) {
        if ((feature instanceof EAttribute)) {
          String sValue = ("" + value);
          StyleApplication lObj3 = createStyleApplication(parent, false, getBindedStyle("BulletList"), null, null, null);
          createText(lObj3, sValue);
        } else {
          if ((value instanceof EObject)) {
            StyleApplication lObj4 = createStyleApplication(parent, false, getBindedStyle("BulletList"), null, null, null);
            boolean _hasBookmark = this.hasBookmark(value);
            if (_hasBookmark) {
              Hyperlink lObj5 = createHyperlink(lObj4, getBookmarkString(this.validBookmark(value)), this.getHyperlinkComment(((EObject)value)), null);
              iconAndText(lObj5, ((EObject)value));
            } else {
              iconAndText(lObj4, ((EObject)value));
            }
          }
        }
      }
    } else {
      StyleApplication lObj6 = createStyleApplication(parent, false, getBindedStyle("BodyText"), null, null, null);
      {
        Bold lObj7 = createBold(lObj6);
        Underline lObj8 = createUnderline(lObj7, UnderlineType.WORDS);
        createText(lObj8, this.getPrettyName(feature));
        createText(lObj6, " : ");
        Object value_1 = eObject.eGet(feature);
        if ((feature instanceof EAttribute)) {
          String sValue_1 = ("" + value_1);
          createText(lObj6, sValue_1);
        } else {
          if ((value_1 instanceof EObject)) {
            boolean _hasBookmark_1 = this.hasBookmark(value_1);
            if (_hasBookmark_1) {
              Hyperlink lObj9 = createHyperlink(lObj6, getBookmarkString(this.validBookmark(value_1)), this.getHyperlinkComment(((EObject)value_1)), null);
              iconAndText(lObj9, ((EObject)value_1));
            } else {
              iconAndText(lObj6, ((EObject)value_1));
            }
          }
        }
      }
    }
  }
  
  /**
   * Based on the enum in DocKind.java, you can introduce an option (e.g. set in the Generation Launching wizard for the EMF Full Model generator)
   * This template will automatically apply the right formatting: in table or in "form" (bullet-points)
   */
  public void genericDoc(final ContainerElement parent, final EObject element, final DocKind kind, final int level, final boolean recursive) throws PxDocGenerationException {
    if (kind != null) {
      switch (kind) {
        case form:
          docWithText(parent, element, level, recursive);
          break;
        case table:
          docWithTables(parent, element, level, recursive);
          break;
        default:
          break;
      }
    }
  }
  
  /**
   * Includes the documentation of an element, with the descripion of attributes in a "bullet point" style
   */
  public void docWithText(final ContainerElement parent, final EObject element, final int level, final boolean recursive) throws PxDocGenerationException {
    boolean _isExcluded = this.isExcluded(element);
    if (_isExcluded) {
      return;
    }
    String doc = this.getDescription(element);
    List<Diagram> diagrams = this.queryDiagrams(element).notEmpty().execute();
    List<EStructuralFeature> attributes = this.getProperties(element);
    List<EObject> childs = this.getChildren(element);
    boolean empty = (((diagrams.isEmpty() && attributes.isEmpty()) && childs.isEmpty()) && this.isEmptyDocumentation(doc));
    if ((!empty)) {
      if (((level > 0) && (level < 10))) {
        HeadingN lObj0 = createHeadingN(parent, level, true);
        boolean _hasBookmark = this.hasBookmark(element);
        if (_hasBookmark) {
          String lObj1 = this.validBookmark(element);
          Bookmark lObj2 = createBookmark(lObj0, getBookmarkString(lObj1));
          iconAndText(lObj2, element);
        } else {
          iconAndText(lObj0, element);
        }
      }
      boolean _suggestImprovements = this.getSuggestImprovements();
      if (_suggestImprovements) {
        _CommonServices.documentation(parent, element);
      } else {
        _CommonServices.optionalDocumentation(parent, element);
      }
      boolean _isEmpty = diagrams.isEmpty();
      boolean _not = (!_isEmpty);
      if (_not) {
        Iterator<org.pragmaticmodeling.pxdoc.diagrams.Diagram> lObj3 = getIterator(diagrams);
        while (lObj3.hasNext()) {
          _DiagramServices.includeDiagram(parent, lObj3.next());
          
        }
      }
      Iterator<org.eclipse.emf.ecore.EStructuralFeature> lObj4 = getIterator(attributes);
      while (lObj4.hasNext()) {
        propertyFormDescription(parent, lObj4.next(), element);
        
      }
      if (recursive) {
        Iterator<org.eclipse.emf.ecore.EObject> lObj5 = getIterator(childs);
        while (lObj5.hasNext()) {
          docWithText(parent, lObj5.next(), (level + 1), recursive);
          
        }
      }
    }
  }
  
  /**
   * Include the element documentation with a description of its attributes in a table
   */
  public void docWithTables(final ContainerElement parent, final EObject element, final int level, final boolean recursive) throws PxDocGenerationException {
    boolean _isExcluded = this.isExcluded(element);
    if (_isExcluded) {
      return;
    }
    String doc = this.getDescription(element);
    List<Diagram> diagrams = this.queryDiagrams(element).notEmpty().execute();
    List<EStructuralFeature> attributes = this.getProperties(element);
    List<EObject> childs = this.getChildren(element);
    boolean empty = (((diagrams.isEmpty() && attributes.isEmpty()) && childs.isEmpty()) && this.isEmptyDocumentation(doc));
    if ((!empty)) {
      if (((level > 0) && (level < 10))) {
        HeadingN lObj0 = createHeadingN(parent, level, true);
        boolean _hasBookmark = this.hasBookmark(element);
        if (_hasBookmark) {
          String lObj1 = this.validBookmark(element);
          Bookmark lObj2 = createBookmark(lObj0, getBookmarkString(lObj1));
          iconAndText(lObj2, element);
        } else {
          iconAndText(lObj0, element);
        }
      }
      boolean _suggestImprovements = this.getSuggestImprovements();
      if (_suggestImprovements) {
        _CommonServices.documentation(parent, element);
      } else {
        _CommonServices.optionalDocumentation(parent, element);
      }
      _DiagramServices.diagrams(parent, element);
      boolean _isEmpty = attributes.isEmpty();
      boolean _not = (!_isEmpty);
      if (_not) {
        Table lObj3 = createTable(parent, null, null, null, null, ShadingPattern.NO_VALUE);
        Measure cellWidth4 = createMeasure((float)35, UnitKind.PC);
        createCellDef(lObj3, null, cellWidth4, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
        Measure cellWidth5 = createMeasure((float)65, UnitKind.PC);
        createCellDef(lObj3, null, cellWidth5, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
        {
          Row lObj6 = createRow(lObj3, null, true, "182,182,228", ShadingPattern.NO_VALUE, false, null);
          {
            Cell lObj7 = createCell(lObj6);
            createText(lObj7, "Property");
            Cell lObj8 = createCell(lObj6);
            createText(lObj8, "Value");
          }
          Iterator<org.eclipse.emf.ecore.EStructuralFeature> lObj9 = getIterator(attributes);
          while (lObj9.hasNext()) {
            nodeAttributeRow(lObj3, lObj9.next(), element);
            
          }
        }
      }
      if (recursive) {
        Iterator<org.eclipse.emf.ecore.EObject> lObj10 = getIterator(this.getChildren(element));
        while (lObj10.hasNext()) {
          docWithTables(parent, lObj10.next(), (level + 1), recursive);
          
        }
      }
    }
  }
  
  /**
   * Icon and provided text.
   */
  public void iconAndText(final ContainerElement parent, final EObject element, final String text) throws PxDocGenerationException {
    createImage(parent, null, this.getImage(element), null, null, null, null);
    createText(parent, " ");
    createText(parent, text);
  }
  
  /**
   * Icon and name of a NamedElement.
   */
  public void iconAndText(final ContainerElement parent, final EObject element) throws PxDocGenerationException {
    createImage(parent, null, this.getImage(element), null, null, null, null);
    createText(parent, " ");
    createText(parent, this.emfLabelProvider.getText(element));
  }
}
