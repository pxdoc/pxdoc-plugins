package org.pragmaticmodeling.pxdoc.common.lib;

import fr.pragmaticmodeling.pxdoc.Color;
import fr.pragmaticmodeling.pxdoc.ContainerElement;
import fr.pragmaticmodeling.pxdoc.Italic;
import fr.pragmaticmodeling.pxdoc.StyleApplication;
import fr.pragmaticmodeling.pxdoc.runtime.AbstractPxDocModule;
import fr.pragmaticmodeling.pxdoc.runtime.IPxDocGenerator;
import fr.pragmaticmodeling.pxdoc.runtime.IPxDocRendererExtension;
import fr.pragmaticmodeling.pxdoc.runtime.PxDocGenerationException;
import java.io.File;
import java.util.List;
import org.apache.log4j.Logger;
import org.pragmaticmodeling.pxdoc.common.lib.DefaultDescriptionProvider;
import org.pragmaticmodeling.pxdoc.common.lib.IDescriptionProvider;

@SuppressWarnings("all")
public class CommonServices extends AbstractPxDocModule {
  private Logger logger = Logger.getLogger(getClass());
  
  public boolean suggestImprovements;
  
  private IDescriptionProvider _descriptionProvider;
  
  private IDescriptionProvider _defaultDescriptionProvider = new DefaultDescriptionProvider(this.generator);
  
  private List<String> _stylesToBind;
  
  public CommonServices(final IPxDocGenerator generator) {
    super(generator);
  }
  
  /**
   * Add a suffix to a filename - May be useful to manage manual parts (=subdocuments)
   */
  public String suffixedFile(final String doc, final String suffix) {
    File file = new File(doc);
    String _substring = file.getName().substring(0, file.getName().lastIndexOf("."));
    String _plus = (_substring + suffix);
    return (_plus + ".docx");
  }
  
  /**
   * Get the HasBoormark value from the Description Provider
   */
  public boolean hasBookmark(final Object object) {
    return this.getDescriptionProvider().hasBookmark(object);
  }
  
  /**
   * Get the suggestImprovement value
   */
  public boolean getSuggestImprovements() {
    return this.suggestImprovements;
  }
  
  /**
   * Set the suggestImprovement value
   */
  public void setSuggestImprovements(final boolean value) {
    this.suggestImprovements = value;
  }
  
  /**
   * Returns true if the string is null or empty after trim.
   */
  public boolean isEmptyDocumentation(final String text) {
    return ((text == null) || (text.trim().length() == 0));
  }
  
  /**
   * Valid Word bookmark provided by the Default Description Provider
   */
  public String validBookmark(final Object object) {
    return this.getDescriptionProvider().getValidBookmark(object);
  }
  
  /**
   * Get the language renderer extension required for the description from the Description Provider
   */
  private IPxDocRendererExtension getDescriptionLanguageExtension(final Object element) {
    return this.getDescriptionProvider().getDescriptionLanguageExtension(element);
  }
  
  /**
   * Get the language in which the item description is written, from the Description Provider
   */
  private String getDescriptionLanguage(final Object element) {
    return this.getDescriptionProvider().getDescriptionLanguage(element);
  }
  
  /**
   * Get the description from the set DescriptionProvider
   */
  public String getDescription(final Object element) {
    return this.getDescriptionProvider().getDescription(element);
  }
  
  /**
   * Get the Description Provider: if _descriptionProvider has not been set, the DefaultDescriptionProvider is used
   */
  public IDescriptionProvider getDescriptionProvider() {
    if ((this._descriptionProvider == null)) {
      return this._defaultDescriptionProvider;
    } else {
      return this._descriptionProvider;
    }
  }
  
  /**
   * Set the Description Provider
   */
  public void setDescriptionProvider(final IDescriptionProvider descProvider) {
    this._descriptionProvider = descProvider;
  }
  
  public void error(final String message) {
    logger.error(message);
  }
  
  public void info(final String message) {
    logger.info(message);
  }
  
  /**
   * May be interesting in some cases: you want either to highlight a missing description or to do nothing
   * according to SuggestImprovement boolean
   */
  public void smartDocumentation(final ContainerElement parent, final Object element) throws PxDocGenerationException {
    if (this.suggestImprovements) {
      documentation(parent, element);
    } else {
      optionalDocumentation(parent, element);
    }
  }
  
  /**
   * Same as the template 'documentation', but if no description is available
   * nothing is done
   */
  public void optionalDocumentation(final ContainerElement parent, final Object element) throws PxDocGenerationException {
    String doc = this.getDescription(element);
    boolean _isEmptyDocumentation = this.isEmptyDocumentation(doc);
    boolean _not = (!_isEmptyDocumentation);
    if (_not) {
      StyleApplication lObj0 = createStyleApplication(parent, false, getBindedStyle("BodyText"), null, null, null);
      {
        String lang = this.getDescriptionLanguage(element);
        IPxDocRendererExtension ext = this.getDescriptionLanguageExtension(element);
        if ((lang != null)) {
          addAllToParent(getLanguageRenderer(lang).render(lObj0, doc, ext), lObj0);
          
        } else {
          createText(lObj0, doc);
        }
      }
    }
  }
  
  /**
   * Include element description, considering that a description should be present: if it is not the case
   *   - if SuggestImprovement is True: highlight that description is missing
   *   - if False: only write "No description" (not highlighted)
   */
  public void documentation(final ContainerElement parent, final Object element) throws PxDocGenerationException {
    String doc = this.getDescription(element);
    boolean _isEmptyDocumentation = this.isEmptyDocumentation(doc);
    boolean _not = (!_isEmptyDocumentation);
    if (_not) {
      String lang = this.getDescriptionLanguage(element);
      IPxDocRendererExtension ext = this.getDescriptionLanguageExtension(element);
      if ((lang != null)) {
        addAllToParent(getLanguageRenderer(lang).render(parent, doc, ext), parent);
        
      } else {
        createText(parent, doc);
      }
    } else {
      if (this.suggestImprovements) {
        StyleApplication lObj0 = createStyleApplication(parent, false, getBindedStyle("Emphasis"), null, null, null);
        Color lObj1 = createColor(lObj0, "255,0,0");
        createText(lObj1, "A description shall be provided.");
      } else {
        StyleApplication lObj2 = createStyleApplication(parent, false, getBindedStyle("BodyText"), null, null, null);
        Italic lObj3 = createItalic(lObj2);
        createText(lObj3, "No description.");
      }
    }
  }
}
