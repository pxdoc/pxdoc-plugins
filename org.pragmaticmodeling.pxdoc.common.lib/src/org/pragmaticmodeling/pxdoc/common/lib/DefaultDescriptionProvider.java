package org.pragmaticmodeling.pxdoc.common.lib;

import java.util.HashMap;
import java.util.Map;

import org.pragmaticmodeling.pxdoc.diagrams.Diagram;

import fr.pragmaticmodeling.pxdoc.runtime.IPxDocGenerator;
import fr.pragmaticmodeling.pxdoc.runtime.IPxDocRendererExtension;

public class DefaultDescriptionProvider implements IDescriptionProvider {

	private Map<String, IPxDocRendererExtension> map = new HashMap<String, IPxDocRendererExtension>();
	private IPxDocGenerator generator;
	private Map<Object, String> bookmarks = new HashMap<Object, String>();
	private int bookmarkIndex = 0;

	public DefaultDescriptionProvider(IPxDocGenerator generator) {
		this.generator = generator;
	}

	@Override
	public String getDescription(Object object) {
		if (object instanceof Diagram) {
			return ((Diagram) object).getDocumentation();
		}
		return "";
	}

	@Override
	public String getDescriptionLanguage(Object object) {
		if (isHtml(getDescription(object)))
			return "html";
		return null;
	}

	private boolean isHtml(String description) {
		return description.trim().startsWith("<") && description.trim().endsWith(">");
	}

	@Override
	public IPxDocRendererExtension getDescriptionLanguageExtension(Object object) {
		String language = getDescriptionLanguage(object);
		if (language != null) {
			return map.get(language);
		}
		return null;
	}

	@Override
	public void installLanguageExtension(String langId, IPxDocRendererExtension ext) {
		if (langId != null && ext != null) {
			map.put(langId, ext);
		}
	}

	public IPxDocGenerator getGenerator() {
		return generator;
	}

	protected Map<String, IPxDocRendererExtension> getLanguageExtensions() {
		return map;
	}

	@Override
	public String getValidBookmark(Object object) {
		if (!bookmarks.containsKey(object)) {
			bookmarks.put(object, generateBookmark(object));
		}
		return bookmarks.get(object);
	}

	private String generateBookmark(Object object) {
		return "bookmark" + bookmarkIndex++;
	}

	@Override
	public boolean hasBookmark(Object object) {
		return true;
	}
}
