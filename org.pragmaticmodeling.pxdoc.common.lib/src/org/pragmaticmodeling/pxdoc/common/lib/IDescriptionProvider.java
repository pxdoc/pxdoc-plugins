package org.pragmaticmodeling.pxdoc.common.lib;

import fr.pragmaticmodeling.pxdoc.runtime.IPxDocRendererExtension;

public interface IDescriptionProvider {

	/**
	 * Specifies how to get the description of an object (which attribute, in which linked object...)
	 * @param object
	 * @return
	 */
	public String getDescription(Object object);
	
	/**
	 * Specifies how to get the language used in the description of an object (HTML, wiki,...)
	 * @param object
	 * @return
	 */
	public String getDescriptionLanguage(Object object);
	
	/**
	 * Specifies which extension of a pxDoc language renderer shall be used for an object
	 * @param object
	 * @return
	 */
	public IPxDocRendererExtension getDescriptionLanguageExtension(Object object);
	
	/**
	 * Install a pxDoc language extension required for its description
	 * @param langId
	 * @param ext
	 */
	public void installLanguageExtension(String langId, IPxDocRendererExtension ext);
	
	/**
	 * Build a Word-compliant bookmark for an object
	 * @param object
	 * @return
	 */
	public String getValidBookmark(Object object);
	
	/**
	 * Specifies if the object shall have a bookmark (to be referenced later with an hyperlink) or not
	 * @param object
	 * @return
	 */
	public boolean hasBookmark(Object object);
}
