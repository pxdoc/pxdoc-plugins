package org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.wizard;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jface.dialogs.DialogSettings;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.plugin.AbstractUIPlugin;

import fr.pragmaticmodeling.pxdoc.runtime.IModelProvider;
import fr.pragmaticmodeling.pxdoc.runtime.IPxDocGenerator;
import fr.pragmaticmodeling.pxdoc.runtime.IStylesheetsRegistry;
import fr.pragmaticmodeling.pxdoc.runtime.launcher.Model;
import fr.pragmaticmodeling.pxdoc.runtime.launcher.PxDocLauncher;
import fr.pragmaticmodeling.pxdoc.runtime.launcher.PxDoclauncherFactory;
import fr.pragmaticmodeling.pxdoc.runtime.launcher.PxDoclauncherPackage;
import org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.IPxDocWizardPage;
import org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.SelectionMode;

/**
 * Wizard class
 */
public abstract class AbstractPxDocWizard extends Wizard implements INewWizard, IPxDocWizard {

	private static Logger logger = Logger.getLogger(AbstractPxDocWizard.class);

	/**
	 * Selection
	 */
	private Object selection;

	private IStylesheetsRegistry stylesheetsRegistry;
	
	/**
	 * Model provider
	 */
	private IModelProvider modelProvider;

	/**
	 * Launcher being built
	 */
	//private PxDocLauncher launcher;

	/**
	 * Selection mode (file or directory)
	 */
	private SelectionMode selectionMode = SelectionMode.file;

	@Override
	public SelectionMode getSelectionMode() {
		return selectionMode;
	}

	/**
	 * Les settings du wizard
	 */
	private DialogSettings wizardSettings = null;

	/**
	 * Returns the model provider
	 * 
	 * @return the model provider
	 */
	@Override
	public IModelProvider getModelProvider() {
		return modelProvider;
	}

	/**
	 * Sets the model provider
	 * 
	 * @param modelProvider
	 *            an IModelProvider
	 */
	@Override
	public void setModelProvider(IModelProvider modelProvider) {
		this.modelProvider = modelProvider;
	}

	/**
	 * Get accessor for the launcher instance
	 * 
	 * @return the launcher instance
	 */
	@Override
	public PxDocLauncher getLauncher() {
		return getPxDocGenerator().getLauncher();
	}

	public static final String SETTINGS_ID = "PxDocWizard_Settings";

	/**
	 * The page to select the generation directory
	 */
	private TargetDirectoryPage basicPage = null;
	private AbstractUIPlugin activator = null;

	private Object model;

	private IPxDocGenerator generator;

	public AbstractPxDocWizard() {
		this(SelectionMode.file);
	}

	public AbstractPxDocWizard(SelectionMode directory) {
		super();
		selectionMode = directory;
		super.setWindowTitle(Messages.pxDocWizardTitle);
	}

	@Override
	public IPxDocGenerator getPxDocGenerator() {
		return generator;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.wizard.Wizard#addPages()
	 */
	@Override
	public void addPages() {
		this.basicPage = new TargetDirectoryPage(getSelectionMode());
		addPage(this.basicPage);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.wizard.Wizard#canFinish()
	 */
	@Override
	public boolean canFinish() {
		for (IWizardPage lPage : getPages()) {
			if (!lPage.isPageComplete())
				return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.wizard.Wizard#performFinish()
	 */
	@Override
	public boolean performFinish() {
		PxDoclauncherFactory factory = PxDoclauncherPackage.eINSTANCE.getPxDoclauncherFactory();
		Model xModel = factory.createModel();
		if (getModel() != null) {
			xModel.setInstance(getModel());
		} else {
			xModel.setInstance(getModelProvider().adaptSelection(selection));
		}
		getLauncher().setModel(xModel);
		for (IWizardPage lPage : getPages()) {
			if (lPage instanceof IPxDocWizardPage) {
				((IPxDocWizardPage) lPage).fillLauncher(getLauncher());
			}
		}

		// save settings
		saveSettings();
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IWorkbenchWizard#init(org.eclipse.ui.IWorkbench,
	 * org.eclipse.jface.viewers.IStructuredSelection)
	 */
	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
	}

	/**
	 * Permet de d�clencher une s�quence personnalis�e d'initialisation APRES
	 * construction des pages du wizard.
	 */
	public final void customInitialization() {
		loadSettings();
	}

	/**
	 * @return the settings file path
	 */
	protected IPath getSettingsPath(String identifier) {
		int pathLength = activator.getStateLocation().addTrailingSeparator().append(".conf").addTrailingSeparator().append("_settings.xml").toPortableString().length();
		if (identifier.length()>256-pathLength) {
			identifier = identifier.substring(0, 256-pathLength);
		}
		return activator.getStateLocation().addTrailingSeparator().append(".conf").addTrailingSeparator()
				.append(identifier + "_settings.xml");
	}

	private final void loadSettings() {
		wizardSettings = new DialogSettings(SETTINGS_ID);
		IPath lSettingsPath = getSettingsPath(getModelIdentifier(getModel()));
		if (lSettingsPath.toFile().exists()) {
			try {
				wizardSettings.load(lSettingsPath.toString());
			} catch (IOException e) {
				logger.error(e.getMessage());
			}
		}
		for (IWizardPage lPage : getPages()) {
			if (lPage instanceof IPxDocWizardPage) {
				IPxDocWizardPage page = (IPxDocWizardPage) lPage;
				page.loadDialogSettings(wizardSettings.getSection(page.getSectionId()));
			}
		}
	}

	/**
	 * Enregistrement des settings du wizard pour persistence et utilisation
	 * lors de la prochaine utilisation du wizard
	 */
	private final void saveSettings() {
		wizardSettings = new DialogSettings(SETTINGS_ID);

		for (IWizardPage lPage : getPages()) {
			if (lPage instanceof IPxDocWizardPage) {
				IDialogSettings lPageSettings = ((IPxDocWizardPage) lPage).buildDialogSettings();
				if (lPageSettings != null)
					wizardSettings.addSection(lPageSettings);
			}
		}
		try {
			IPath lSettingsPath = getSettingsPath(getModelIdentifier(getModel()));
			if (lSettingsPath.toFile().exists()) {
				lSettingsPath.toFile().delete();
			}
			lSettingsPath.toFile().getParentFile().mkdirs();
			lSettingsPath.toFile().createNewFile();
			wizardSettings.save(lSettingsPath.toString());
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
	}

	public Object getModel() {
		if (model == null) {
			model = getModelProvider().adaptSelection(selection);
		}
		return model;
	}

	/**
	 * Return a unique ID for an object in order to persist the generator settings for this object
	 */
	public String getModelIdentifier(Object model) {
		if (model != null) {
			String identifier = getModel().toString().replaceAll("[\\\\/:*?\"<>|]", "");
			int first = identifier.indexOf("@");
			if (first >-1) {
				int end = identifier.indexOf(" ", first);
				if (end>-1) {
					identifier = identifier.substring(0, first) + identifier.substring(end);
					identifier = identifier.replaceAll("\\W+", "");
				}
			}
			return identifier;
		}
		return "Object";
	}
	
	public Object getSelection() {
		return selection;
	}

	public void setSelection(Object selection) {
		this.selection = selection;
	}
	
	@Override
	public IStylesheetsRegistry getStylesheetsRegistry() {
		return stylesheetsRegistry;
	}
	
	@Override
	public void setStylesheetsRegistry(IStylesheetsRegistry stylesheetsRegistry) {
		this.stylesheetsRegistry = stylesheetsRegistry;
	}
	
	@Override
	public void setPxDocGenerator(IPxDocGenerator pxDocGenerator) {
		this.generator = pxDocGenerator;
	}
	
	@Override
	public void setUiActivator(AbstractUIPlugin uiActivator) {
		this.activator = uiActivator;
	}
	
	@Override
	public boolean generateDocuments() {
		return true;
	}
	

}
