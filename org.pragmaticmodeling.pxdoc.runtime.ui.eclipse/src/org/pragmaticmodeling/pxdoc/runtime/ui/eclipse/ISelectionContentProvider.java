package org.pragmaticmodeling.pxdoc.runtime.ui.eclipse;

import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.viewers.ITreeContentProvider;

public interface ISelectionContentProvider extends ITreeContentProvider {

	EObject getRootEObject();
	List<Object> getAllElements(Object element);
}
