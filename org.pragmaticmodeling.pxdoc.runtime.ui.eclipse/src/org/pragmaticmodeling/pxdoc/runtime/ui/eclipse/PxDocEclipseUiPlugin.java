package org.pragmaticmodeling.pxdoc.runtime.ui.eclipse;

import java.io.IOException;
import java.net.URL;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.log4j.PropertyConfigurator;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The main plugin class to be used in the desktop.
 */
public class PxDocEclipseUiPlugin extends AbstractUIPlugin {
	//The shared instance.
	private static PxDocEclipseUiPlugin plugin;

	//Resource bundle.
	private ResourceBundle resourceBundle;

	// The plug-in ID
    public static final String PLUGIN_ID = "org.pragmaticmodeling.pxdoc.runtime.ui.eclipse";

	/**
	 * The constructor.
	 */
	public PxDocEclipseUiPlugin() {
		plugin = this;
		//FIXME la classe messages n'existe pas...
		try {
			resourceBundle = ResourceBundle.getBundle("messages");
		}
		catch (MissingResourceException x) {
			resourceBundle = null;
		}
	}

	/**
	 * This method is called upon plug-in activation
	 */
	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		configureLog(getBundle().getEntry("/logger.properties"));
	}
	
	/**
	 * This method is called when the plug-in is stopped
	 */
	@Override
	public void stop(BundleContext context) throws Exception {
		super.stop(context);
		plugin = null;
		resourceBundle = null;
	}

	/**
	 * Returns the shared instance.
	 */
	public static PxDocEclipseUiPlugin getDefault() {
		return plugin;
	}
	
	/**
	 * Convenient method to configure log4j to use the config file at url.
	 * @param url
	 */
	public static void configureLog(URL url) {
		try {
			URL resolved = FileLocator.toFileURL(url);
			if(resolved != null)
				PropertyConfigurator.configure(resolved);
			else
				plugin.error("configureLog called with null url", null);
		} catch (IOException e) {
			plugin.error("configureLog called with wrong url " + url, e);
		}
	}
	
	public void error(String message, Throwable exception) {
		getLog().log(new Status(IStatus.ERROR, PLUGIN_ID, IStatus.OK, message, exception));
	}
	
	/**
	 * Returns the string from the plugin's resource bundle, or 'key' if not
	 * found.
	 */
	public static String getResourceString(String key) {
		ResourceBundle bundle = PxDocEclipseUiPlugin.getDefault().getResourceBundle();
		try {
			return (bundle != null) ? bundle.getString(key) : key;
		} catch (MissingResourceException e) {
			return key;
		}
	}
	
	/**
	 * Returns the plugin's resource bundle,
	 */
	public ResourceBundle getResourceBundle() {
		return resourceBundle;
	}
}
