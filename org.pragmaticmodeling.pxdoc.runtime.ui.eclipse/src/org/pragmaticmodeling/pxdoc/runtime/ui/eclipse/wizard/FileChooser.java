package org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.wizard;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.CommonPlugin;
import org.eclipse.emf.common.util.URI;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.PxDocEclipseUiPlugin;
import org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.SelectionMode;

public class FileChooser extends Composite {

	private String fileFilterPath = null;
	private String filePath = null;
	private Label filePathLabel = null;
	private final Color white = new Color(null, 255, 255, 255);
	private Button buttonSelectFile = null;
	private List<ModifyListener> modifyListeners = null;
	private SelectionMode selectionMode;
	
	/**
	 * Valid extensions
	 * 
	 */
	public enum FileExtension {
		docx, pptx;

		/**
		 * 
		 */
		public String getExtension() {
			return "." + super.toString();
		}

		/**
		 * 
		 */
		public String getFilter() {
			return "*." + super.toString();
		}

		/**
		 * 
		 */
		public String getDescription() {
			return super.toString() + " " + PxDocEclipseUiPlugin.getResourceString("pxDoc.filechooser.label");
		}
	}




	public FileChooser(final Composite parent, int style, SelectionMode selectionMode) {
		super(parent, style);
		this.selectionMode = selectionMode;
		modifyListeners = new ArrayList<ModifyListener>();
		addDisposeListener(new DisposeListener() {
			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				white.dispose();
			}
		});
		// Layout
		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 2;
		gridLayout.marginHeight = 0;
		setLayout(gridLayout);

		// Add a filechooser button
		filePathLabel = new Label(this, SWT.BORDER | SWT.WRAP | SWT.DRAW_MNEMONIC);
		filePathLabel.setBackground(white);
		filePathLabel.setText("");
		filePathLabel.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		buttonSelectFile = new Button(this, SWT.PUSH);
		buttonSelectFile.setText("...");
		buttonSelectFile.addListener(SWT.Selection, new Listener() {
			
			@Override
			public void handleEvent(Event event) {
				if (getSelectionMode().equals(SelectionMode.file)) {
					handleChooseFileEvent(event, parent.getShell());
				} else {
					handleChooseDirectoryEvent(event, parent.getShell());
				}
			}
			
		});

	}

	/**
	 * Returns the selection mode
	 * 
	 * @return the selection mode
	 */
	public SelectionMode getSelectionMode() {
		return selectionMode;
	}
	
	public void addModifyListener(ModifyListener pListener) {
		modifyListeners.add(pListener);
	}

	public void removeModifyListener(ModifyListener pListener) {
		modifyListeners.remove(pListener);
	}

	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		filePathLabel.setEnabled(enabled);
		buttonSelectFile.setEnabled(enabled);
	}

	public final String getFilePath() {
		return filePath;
	}

	public File getFile() {
		File lFile = null;
		if (getFilePath().startsWith("platform:/resource")) {
			URI uri = URI.createURI(getFilePath());
			URI resolved = CommonPlugin.resolve(uri);
			lFile = new File(resolved.toFileString());
		} else {
			lFile = new File(getFilePath());
		}
		return lFile;
	}

	public void setFilePath(String file) {
		this.filePath = file;
		filePathLabel.setText(file);
		layout(true);
		Event e = new Event();
		e.widget = filePathLabel;
		notifyListeners(e);
	}

	protected void handleChooseFileEvent(Event e, Shell shell) {
		FileDialog fileDialog = new FileDialog(shell, SWT.SINGLE);

		fileDialog.setFilterPath(fileFilterPath);

		fileDialog.setFilterExtensions(new String[] { FileExtension.docx.getFilter(), FileExtension.pptx.getFilter() });
		fileDialog.setFilterNames(new String[] { FileExtension.docx.getDescription(), FileExtension.pptx.getDescription() });

		filePath = fileDialog.open();

		if (filePath != null) {

			File tmp = new File(filePath);

			if (tmp.exists()) {
				MessageBox box = new MessageBox(shell, SWT.OK | SWT.CANCEL | SWT.ICON_WARNING);
				box.setText(PxDocEclipseUiPlugin.getResourceString("pxDocDialogWarningTitle"));
				box.setMessage(PxDocEclipseUiPlugin.getResourceString("pxDoc.filechooser.filealreadyexists.warning"));
				int res = box.open();

				if (res == SWT.CANCEL) {
					return;
				}
			}
			fileFilterPath = fileDialog.getFilterPath();
			filePathLabel.setText(filePath);
			filePathLabel.setToolTipText(filePath);

			notifyListeners(e);

		}
	}

	protected void handleChooseDirectoryEvent(Event e, Shell shell) {
		DirectoryDialog fileDialog = new DirectoryDialog(shell, SWT.SINGLE);

		fileDialog.setFilterPath(fileFilterPath);

		filePath = fileDialog.open();

		if (filePath != null) {
			fileFilterPath = fileDialog.getFilterPath();
			filePathLabel.setText(filePath);
			filePathLabel.setToolTipText(filePath);
			notifyListeners(e);
		}
	}

	private void notifyListeners(Event e) {
		ModifyEvent lEvent = new ModifyEvent(e);
		for (ModifyListener lListener : modifyListeners) {
			lListener.modifyText(lEvent);
		}
	}

}
