package org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.commands;

import org.eclipse.core.expressions.PropertyTester;

public class DefaultPropertyTester extends PropertyTester {

	private Class<?> clazz;
	
	public DefaultPropertyTester(Class<?> clazz) {
		super();
		this.clazz = clazz;
	}
	
	public DefaultPropertyTester() {
		super();
	}

	@Override
	public boolean test(Object receiver, String property, Object[] args, Object expectedValue) {
		if (clazz != null) {
			return clazz.isAssignableFrom(receiver.getClass());
		}
		return true;
	}
	
	public Class<?> getClass_() {
		return clazz;
	}

}
