package org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.wizard;


import org.eclipse.jface.wizard.IWizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Shell;

public class PxDocWizardDialog extends WizardDialog {

	public PxDocWizardDialog(Shell parentShell, IWizard newWizard) {
		super(parentShell, newWizard);
		setPageSize(450, 450);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#create()
	 */
	@Override
	public void create() {
		super.create();
		AbstractPxDocWizard wiz = (AbstractPxDocWizard)getWizard();
		wiz.customInitialization();
	}


}