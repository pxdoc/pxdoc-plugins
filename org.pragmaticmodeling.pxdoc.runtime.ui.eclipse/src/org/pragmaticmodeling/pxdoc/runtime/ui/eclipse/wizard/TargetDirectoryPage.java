package org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.wizard;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.DialogSettings;
import org.eclipse.jface.dialogs.IDialogPage;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.preference.PreferenceDialog;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.PreferencesUtil;
import org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.IPxDocWizardPage;
import org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.PxDocEclipseUiPlugin;
import org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.SelectionMode;
import org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.wizard.FileChooser.FileExtension;

import fr.pragmaticmodeling.common.ui.ObjectsCombo;
import fr.pragmaticmodeling.pxdoc.IPxdocStylesheet;
import fr.pragmaticmodeling.pxdoc.runtime.IStylesheetsRegistry;
import fr.pragmaticmodeling.pxdoc.runtime.StylesheetDescriptor;
import fr.pragmaticmodeling.pxdoc.runtime.launcher.PxDocLauncher;
import fr.pragmaticmodeling.pxdoc.runtime.util.PxDocLauncherHelper;
import fr.pragmaticmodeling.pxdoc.runtime.util.PxDocUtils;
import fr.pragmaticmodeling.pxdoc.runtime.util.Util;

/**
 * First wizard page to set target generation directory and file.
 */
public class TargetDirectoryPage extends WizardPage implements Observer, IPxDocWizardPage {

	public static final String SETTINGS_ID = "TargetDirectoryPage_Settings";

	protected Point lastMouseDown;
	private IPxdocStylesheet stylesheet;
	private IDialogSettings wizardSettings;
	private IStatus status;

	private static final String FILENAME = "FILENAME";
	private static final String DIRECTORY = "DIRECTORY";

	/**
	 * The selection mode (file or directory)
	 */
	private SelectionMode selectionMode;

	private List<StylesheetDescriptor> allStylesheets;

	protected File stylesheetFile;

	private List<String> allStyles;

	private List<Combo> combos;

	private Map<String, String> styleBindings;

	private boolean requiresStylesheetBinding;

	private ObjectsCombo stylesheetsCombo;

	private Button protectButton;
	private boolean loaded;

	private Text dirText;

	private Text fileText;

	private Button checkButton;

	class StylesheetsLabelProvider extends LabelProvider {

		@Override
		public String getText(Object element) {
			if (element instanceof File) {
				return ((File) element).getName();
			} else if (element instanceof StylesheetDescriptor) {
				return ((StylesheetDescriptor) element).toString();
			}
			return super.getText(element);
		}

	}

	/**
	 * Constructor for TargetDirectoryPage.
	 */
	public TargetDirectoryPage(SelectionMode selectionMode) {
		super("Target File Or Directory Page");
		this.selectionMode = selectionMode;
		setTitle(PxDocEclipseUiPlugin.getResourceString("pxDoc.wizard.destinationpage.title"));
		setDescription(PxDocEclipseUiPlugin.getResourceString("pxDoc.wizard.destinationpage.description"));
		allStyles = new ArrayList<String>();
		combos = new ArrayList<Combo>();
	}

	/**
	 * @see IDialogPage#createControl(Composite)
	 */
	@Override
	public void createControl(final Composite parent) {
		Composite composite = new Composite(parent, SWT.NULL);
		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 4;
		composite.setLayout(gridLayout);

		ModifyListener modifyListener = new ModifyListener() {
			
			@Override
			public void modifyText(ModifyEvent e) {
				validatePageAndUpdateWizard();
			}
			
		};

		Label dirLabel = new Label(composite, SWT.WRAP);
		dirLabel.setText("Target Folder : ");
		dirLabel.setToolTipText("Select a target directory for the documentation generation.");
		dirText = new Text(composite, SWT.BORDER);
		dirText.addModifyListener(modifyListener);
		dirText.setText(System.getProperty("user.home"));
		GridDataFactory.fillDefaults().align(SWT.FILL, SWT.BEGINNING).grab(true, false).applyTo(dirText);
		Button dirButton = new Button(composite, SWT.PUSH);
		dirButton.setText("...");
		dirButton.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				DirectoryDialog fileDialog = new DirectoryDialog(parent.getShell(), SWT.SINGLE);
				String dirPath = fileDialog.open();
				if (dirPath != null) {
					dirText.setText(dirPath);
					validatePageAndUpdateWizard();
				}
			}
		});
		new Label(composite, SWT.WRAP);
		if (selectionMode == SelectionMode.file) {
			Label fileLabel = new Label(composite, SWT.WRAP);
			fileLabel.setText("Target Document : ");
			fileLabel.setToolTipText("Set a file name for your generated document.");
			fileText = new Text(composite, SWT.BORDER);
			fileText.addModifyListener(modifyListener);
			String initialName = getPxDocWizard().getPxDocGenerator().getName();
			initialName = (initialName == null ? "New Document" : initialName);
			fileText.setText(initialName + ".docx");
			GridDataFactory.fillDefaults().align(SWT.FILL, SWT.BEGINNING).grab(true, false).applyTo(fileText);
			Button fileButton = new Button(composite, SWT.PUSH);
			fileButton.setText("...");
			fileButton.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					FileDialog fileDialog = new FileDialog(parent.getShell(), SWT.SINGLE);
					fileDialog.setFilterExtensions(new String[]{"*.docx"});
					File initialDir = new File(dirText.getText());
					if (initialDir.exists()) {
						fileDialog.setFilterPath(initialDir.getAbsolutePath());
					}
					String filePath = fileDialog.open();
					if (filePath != null) {
						File file = new File(filePath);
						File directory = file.getParentFile();
						if (!directory.equals(new File(dirText.getText()))) {
							dirText.setText(directory.getAbsolutePath());
						}
						fileText.setText(file.getName());
						validatePageAndUpdateWizard();
					}
				}
			});
			checkButton = new Button(composite, SWT.PUSH);
			checkButton.setText("Check");
			checkButton.setEnabled(false);
			checkButton.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					validatePageAndUpdateWizard();
				}

			});
		}
		// document protection
		Label protectLabel = new Label(composite, SWT.WRAP);
		protectLabel.setText("Protect Document");
		switch (selectionMode) {
		case directory:
			protectLabel.setText("Protect Documents");
			break;
		case file:
			protectLabel.setText("Protect Document");
			break;
		}
		protectButton = new Button(composite, SWT.CHECK);
		protectButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		protectButton.setEnabled(true);

		requiresStylesheetBinding = generateDocuments()
				&& getPxDocWizard().getPxDocGenerator().requiresStylesheetBinding();
		if (requiresStylesheetBinding) {
			allStylesheets = new ArrayList<StylesheetDescriptor>();
			allStylesheets.addAll(getStylesheetsRegistry().getGlobalStylesheets());
			styleBindings = new HashMap<String, String>();
			// Stylesheet
			Group groupStylesheet = new Group(composite, SWT.NONE);
			groupStylesheet.setText("Stylesheet");
			groupStylesheet.setToolTipText("A stylesheet your document generator will conform to.");
			groupStylesheet.setLayoutData(new GridData(GridData.FILL, GridData.BEGINNING, true, false, 3, 1));
			groupStylesheet.setLayout(new GridLayout(3, false));

			// Label
			Label stylesheetLabel = new Label(groupStylesheet, SWT.WRAP);
			stylesheetLabel.setToolTipText(
					"Select a stylesheet file. If the selection is empty, use the 'Dirs...' button to set the directories where stylesheets can be found.");
			stylesheetLabel.setText("Stylesheet File: ");

			// Stylesheet
			if (getPxDocWizard().getPxDocGenerator().getStylesheet() == null) {
				Composite fileSelectionArea = new Composite(groupStylesheet, SWT.NONE);
				GridData fileSelectionData = new GridData(GridData.FILL, GridData.BEGINNING, true, false, 2, 1);
				fileSelectionArea.setLayoutData(fileSelectionData);
				GridLayout fileSelectionLayout = new GridLayout();
				fileSelectionLayout.numColumns = 3;
				fileSelectionLayout.makeColumnsEqualWidth = false;
				fileSelectionLayout.marginWidth = 0;
				fileSelectionLayout.marginHeight = 0;
				fileSelectionArea.setLayout(fileSelectionLayout);

				stylesheetsCombo = new ObjectsCombo(fileSelectionArea, SWT.NONE, new StylesheetsLabelProvider());
				stylesheetsCombo.setToolTipText(
						"Select a stylesheet file. If the selection is empty, use the 'Dirs...' button to set the directories where stylesheets can be found.");
				stylesheetsCombo.setInput(allStylesheets);
				stylesheetsCombo.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
				stylesheetsCombo.addSelectionListener(new SelectionAdapter() {

					@Override
					public void widgetSelected(SelectionEvent e) {
						File file = ((StylesheetDescriptor) stylesheetsCombo.getSelection()).getFile();
						if (file != null && file.equals(stylesheetFile)) {
							return;
						}
						stylesheetFile = ((StylesheetDescriptor) stylesheetsCombo.getSelection()).getFile();
						stylesheet = PxDocUtils.getStylesheet(stylesheetFile);
						if (stylesheet != null) {
							getPxDocWizard().getPxDocGenerator().setStylesheet(stylesheet);
						}
						allStyles = stylesheet.getParagraphStyles();
						fillCombos();
						styleBindings.clear();
						for (String unbindedStyle : getPxDocWizard().getPxDocGenerator().getStylesToBind()) {
							styleBindings.put(unbindedStyle, null);
						}
						validatePageAndUpdateWizard();
					}
				});
				Button browsePreferencesBtn = new Button(fileSelectionArea, SWT.PUSH);
				browsePreferencesBtn.setText("Dirs...");
				browsePreferencesBtn.setToolTipText("Preferences page to set stylesheet directories.");
				browsePreferencesBtn.addSelectionListener(new SelectionAdapter() {

					@Override
					public void widgetSelected(SelectionEvent e) {
						PreferenceDialog pref = PreferencesUtil.createPreferenceDialogOn(getShell(),
								"fr.pragmaticmodeling.pxdoc.dsl.PxDoc.stylesheets", null, null);
						if (pref != null)
							pref.open();
						List<StylesheetDescriptor> stylesheets = getStylesheetsRegistry().getGlobalStylesheets(true);
						if (stylesheets.size() != allStylesheets.size()) {
							allStylesheets = new ArrayList<StylesheetDescriptor>();
							allStylesheets.addAll(stylesheets);
							stylesheetsCombo.setInput(allStylesheets);
							validatePageAndUpdateWizard();
						}
					}

				});
			}
			for (String unbindedStyle : getPxDocWizard().getPxDocGenerator().getStylesToBind()) {
				styleBindings.put(unbindedStyle, null);
				Label label = new Label(groupStylesheet, SWT.WRAP);
				label.setText(unbindedStyle);
				Combo combo = new Combo(groupStylesheet, SWT.BORDER);
				combo.setData(unbindedStyle);
				combo.addSelectionListener(new SelectionAdapter() {
					@Override
					public void widgetSelected(SelectionEvent e) {
						styleBindings.put((String) ((Combo) e.widget).getData(), ((Combo) e.widget).getText());
						validatePageAndUpdateWizard();
					}
				});
				for (String s : allStyles) {
					combo.add(s);
				}
				combo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
				combos.add(combo);
			}

			fillCombos();
		}
		// set the composite as the control for this page
		setControl(composite);

	}

	private boolean generateDocuments() {
		return getPxDocWizard().generateDocuments();
	}

	private void fillCombos() {
		for (Combo c : combos) {
			c.clearSelection();
			c.removeAll();
			c.setEnabled(!allStyles.isEmpty());
			for (String s : allStyles) {
				c.add(s);
			}
		}
	}

	@Override
	public boolean isPageComplete() {
		return status != null && status.isOK();
	}

	/**
	 * D�s que le fichier destrination est choisit, on peut aller vers la
	 * prochaine page
	 * 
	 * @see IWizardPage#canFlipToNextPage()
	 */
	@Override
	public boolean canFlipToNextPage() {
		return (getNextPage() != null && isPageComplete());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.wizard.WizardPage#getDialogSettings()
	 */
	@Override
	public IDialogSettings buildDialogSettings() {
		IDialogSettings settings = wizardSettings == null ? new DialogSettings(SETTINGS_ID) : wizardSettings;
		if (fileText != null) {
			settings.put(FILENAME, fileText.getText());
		}
		settings.put(DIRECTORY, dirText.getText());
		if (requiresStylesheetBinding) {
			settings.put("stylesheet", stylesheetFile.getName());
			IDialogSettings bindingsSection = settings.addNewSection("styleBindings");
			String keys = "";
			for (String key : styleBindings.keySet()) {
				keys += key + ",";
				bindingsSection.put(key, styleBindings.get(key));
			}
			if (keys.length() > 0) {
				keys = keys.substring(0, keys.length() - 1);
			}
			bindingsSection.put("_keys", keys);
		}
		settings.put("isProtected", isProtected());
		return settings;
	}

	@Override
	public void loadDialogSettings(IDialogSettings settings) {
		if (settings != null) {
			wizardSettings = settings;
			if (fileText != null) {
				String filename = settings.get(FILENAME);
				fileText.setText(filename != null ? filename : "");
			}
			String directory = settings.get(DIRECTORY);
			dirText.setText(directory != null ? directory : System.getProperty("user.home"));
			if (requiresStylesheetBinding) {
				String stylesheet = settings.get("stylesheet");
				if (stylesheet != null) {
					selectStylesheet(stylesheet);
				}
				IDialogSettings bindingsSection = settings.getSection("styleBindings");
				if (bindingsSection != null) {
					String[] keys = bindingsSection.get("_keys").split(",");
					for (String key : keys) {
						String style = bindingsSection.get(key);
						setComboSelection(key, style);
					}
				}
			}
			protectButton.setSelection(settings.getBoolean("isProtected"));
		}
		loaded = true;
		validatePageAndUpdateWizard();
	}

	private void selectStylesheet(String stylesheet2) {
		if (stylesheetsCombo == null)
			return;
		File selectedStylesheet = null;
		for (StylesheetDescriptor descriptor : allStylesheets) {
			File f = descriptor.getFile();
			if (f.getName().equals(stylesheet2)) {
				selectedStylesheet = f;
				break;
			}
		}
		if (selectedStylesheet != null) {
			stylesheetsCombo.setSelection(selectedStylesheet);
			stylesheetFile = (File) stylesheetsCombo.getSelection();
			stylesheet = PxDocUtils.getStylesheet(stylesheetFile);
			if (stylesheet != null) {
				getPxDocWizard().getPxDocGenerator().setStylesheet(stylesheet);
			}
			allStyles = stylesheet.getParagraphStyles();
			fillCombos();
		}
	}

	private void setComboSelection(String key, String style) {
		for (Combo c : combos) {
			if (key.equals(c.getData())) {
				if (allStyles.contains(style)) {
					c.setText(style);
					styleBindings.put(key, style);
				}
			}
		}
	}

	private void validatePageAndUpdateWizard() {
		validatePage();
		getContainer().updateMessage();
		getContainer().updateButtons();
	}

	private void validatePage() {
		status = null;
		setErrorMessage(null);
		setMessage(null);
		if (!loaded)
			return;
		String generationDirectory = dirText.getText();
		String filename = null;
		if (isEmpty(generationDirectory)) {
			setErrorMessage(PxDocEclipseUiPlugin.getResourceString("targetDirectoryPage.error.validTargetFolder"));
			return;
		}
		File genDirFile = new File(generationDirectory);
		if (!genDirFile.exists()) {
			setErrorMessage(PxDocEclipseUiPlugin.getResourceString("targetDirectoryPage.error.validTargetFolder"));
			return;
		}
		if (fileText != null) {
			filename = fileText.getText();
			if (isEmpty(filename)) {
				 setErrorMessage(PxDocEclipseUiPlugin.getResourceString("targetDirectoryPage.error.validTargetFile"));
				return;
			}
			if (!filename.endsWith(FileExtension.docx.getExtension())
					&& !filename.endsWith(FileExtension.pptx.getExtension())) {
				setErrorMessage(
						PxDocEclipseUiPlugin.getResourceString("targetDirectoryPage.error.validTargetFileExtension"));
				return;
			}
			// Check that the file is writable
			if (!checkFileIsWritable(getTargetFile())) {
				return;
			}
		}
		if (requiresStylesheetBinding) {
			if (stylesheetFile == null) {
				setErrorMessage("Select a stylesheet your generator will conform to.");
				return;
			}
			if (allStylesheets.isEmpty()) {
				setErrorMessage(
						"No stylesheet can be found.\nUse the 'Dirs...' button to access the Preferences page where you can set the directories where stylesheets can be found.");
				return;
			}
			if (!getPxDocWizard().getPxDocGenerator().getStylesToBind().isEmpty()) {
				if (styleBindings.values().contains(null) || styleBindings.isEmpty()) {
					setErrorMessage("All parameterized styles have to be binded.");
					return;
				}
			}
		}
		if (getErrorMessage() == null && status == null) {
			status = Status.OK_STATUS;
		}
	}

	@Override
	public void setErrorMessage(String newMessage) {
		if (newMessage != null)
			status = new Status(IStatus.ERROR, "org.pragmaricmodeling.pxdoc.runtime.ui.eclipse", newMessage);
		super.setErrorMessage(newMessage);
	}

	public String getTargetFile(String generationDirectory, String filename) {
		if (generationDirectory != null) {
			if (filename != null) {
				File file = new File(generationDirectory, filename);
				return file.getAbsolutePath();
			} else {
				return generationDirectory;
			}
		}
		return null;
	}

	private boolean isEmpty(String pString) {
		return (pString == null || pString.trim().equals(""));
	}

	protected IPxDocWizard getPxDocWizard() {
		return (IPxDocWizard) getWizard();
	}

	public final void setEnabled(boolean enabled) {
	}

	private boolean checkFileIsWritable(File file) {
		if (!Util.canWrite(file)) {
			setErrorMessage(PxDocEclipseUiPlugin.getResourceString("targetDirectoryPage.error.fileNotWritable"));
			checkButton.setEnabled(true);
			return false;
		} else {
			checkButton.setEnabled(false);
			return true;
		}
	}

	public File getTargetFile() {
		return new File(dirText.getText(), fileText.getText());
	}

	@Override
	public String getSectionId() {
		return SETTINGS_ID;
	}

	@Override
	public void fillLauncher(PxDocLauncher pLauncher) {
		if (fileText != null) {
			File destinationFile = getTargetFile();
			PxDocLauncherHelper.addProperty(pLauncher, "filename", destinationFile.getAbsoluteFile());
			pLauncher.setFilename(fileText.getText());
		}
		String generationDirectory = dirText.getText();
		pLauncher.setGenerationDirectory(generationDirectory);
		if (requiresStylesheetBinding) {
			pLauncher.setStylesheet(stylesheetFile.getName());
			for (String key : styleBindings.keySet()) {
				PxDocLauncherHelper.addStyleBinding(getPxDocWizard().getPxDocGenerator(), key, styleBindings.get(key));
			}
		}
		pLauncher.setProtectGenerated(isProtected());
	}

	private boolean isProtected() {
		return protectButton.getSelection();
	}

	@Override
	public void update(Observable o, Object arg) {

	}

	IStylesheetsRegistry getStylesheetsRegistry() {
		return getPxDocWizard().getStylesheetsRegistry();
	}

}
