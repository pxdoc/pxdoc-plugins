package org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.wizard;

import java.util.Observable;
import java.util.Observer;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.widgets.Composite;

import org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.IPxDocWizardPage;

public abstract class AbstractPxDocWizardPage extends WizardPage implements IPxDocWizardPage, Observer {

    /**
     * Gestion des erreurs possible sur la page
     */
    private IStatus currentStatus = null;
    
	protected AbstractPxDocWizardPage(String pageName) {
		super(pageName);
	}

    @Override
	public boolean isPageComplete() {
        if (getCurrentStatus() != null && getCurrentStatus().isOK())
            return true;
        return false;
	}

	/**
     * D�s que le fichier destrination est choisit, on peut aller vers la prochaine page
     * 
     * @see IWizardPage#canFlipToNextPage()
     */
    @Override
    public boolean canFlipToNextPage() {
    	return (getNextPage() != null && isPageComplete());
    }

	public IStatus getCurrentStatus() {
		return currentStatus;
	}

	public void setCurrentStatus(IStatus currentStatus) {
		this.currentStatus = currentStatus;
	}

	@Override
	public abstract void loadDialogSettings(IDialogSettings pSettings);

	@Override
	public abstract String getSectionId();

	@Override
	public void createControl(Composite parent) {
		
	}

	@Override
	public abstract IDialogSettings buildDialogSettings();
	
    protected IPxDocWizard getPxDocWizard() {
        return (IPxDocWizard)getWizard();
    }

	@Override
	public void update(Observable observable, Object data) {
		setCurrentStatus(checkValidity());
		getWizard().getContainer().updateButtons();
		WizardHelper.applyToStatusLine(this, getCurrentStatus());
	}
    
    protected abstract IStatus checkValidity();
    
}
