package org.pragmaticmodeling.pxdoc.runtime.ui.eclipse;

import java.io.IOException;
import java.net.URL;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.log4j.PropertyConfigurator;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

public abstract class AbstractPxUiPlugin extends AbstractUIPlugin {

	// The plug-in ID
	// public static final String PLUGIN_ID =
	// "�e.parentPackageDeclaration.name�";

	// The shared instance
	private static AbstractPxUiPlugin plugin;

	// Resource bundle.
	private ResourceBundle resourceBundle = null;

//	@Inject
//	private IStylesheetsRegistry stylesheetsRegistry;

	/**
	 * The constructor
	 */
	public AbstractPxUiPlugin() {
		plugin = this;

		try {
			resourceBundle = ResourceBundle.getBundle("messages");
		} catch (MissingResourceException x) {
			resourceBundle = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.
	 * BundleContext)
	 */
	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		URL loggerProperties = getBundle().getEntry("/logger.properties");
		if (loggerProperties != null)
			configureLog(loggerProperties);
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	@Override
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static AbstractPxUiPlugin getDefault() {
		return plugin;
	}

	/**
	 * Returns an image descriptor for the image file at the given plug-in
	 * relative path
	 *
	 * @param path
	 *            the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(AbstractPxUiPlugin.getDefault().getPluginId(), path);
	}

	/**
	 * Returns the string from the plugin's resource bundle, or 'key' if not
	 * found.
	 */
	public static String getResourceString(String key) {
		ResourceBundle bundle = AbstractPxUiPlugin.getDefault().getResourceBundle();
		try {
			return (bundle != null) ? bundle.getString(key) : key;
		} catch (MissingResourceException e) {
			return key;
		}
	}

	/**
	 * Returns the plugin's resource bundle,
	 */
	public ResourceBundle getResourceBundle() {
		return resourceBundle;
	}

	/**
	 * Convenient method to configure log4j to use the config file at url.
	 * 
	 * @param url
	 */
	public static void configureLog(URL url) {
		try {
			URL resolved = FileLocator.toFileURL(url);
			// System.out.println("url " + url);
			// System.out.println("\tresolved to " + resolved);
			if (resolved != null)
				PropertyConfigurator.configure(resolved);
			else
				plugin.error("configureLog called with null url", null);
		} catch (IOException e) {
			plugin.error("configureLog called with wrong url " + url, e);
		}

	}

	public void error(String message, Throwable exception) {
		getLog().log(new Status(IStatus.ERROR, AbstractPxUiPlugin.getDefault().getPluginId(), IStatus.OK, message, exception));
	}
	
	protected abstract String getPluginId();
	
	public abstract String getPxDocGeneratorPluginId();
	
//	public IStylesheetsRegistry getStylesheetsRegistry() {
//		if (stylesheetsRegistry == null) {
//			stylesheetsRegistry =  new StylesheetRegistryImpl();
//		}
//		return stylesheetsRegistry;
//	}
}
