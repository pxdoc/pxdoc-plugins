package org.pragmaticmodeling.pxdoc.runtime.ui.eclipse;

import org.osgi.framework.BundleContext;

import com.google.inject.Injector;

import fr.pragmaticmodeling.pxdoc.runtime.eclipse.IPxGuiceAwarePlugin;

public abstract class AbstractPxGuiceAwareUiPlugin extends AbstractPxUiPlugin implements IPxGuiceAwarePlugin {

	private Injector injector;
	
	public AbstractPxGuiceAwareUiPlugin() {
		super();
	}
	
	@Override
	public Injector getInjector() {
        return injector;
    }
	
	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
	}
	
	@Override
	public void stop(BundleContext context) throws Exception {
		this.injector = null;
		super.stop(context);
	}

	public void setInjector(Injector injector) {
		this.injector = injector;
	}

}
