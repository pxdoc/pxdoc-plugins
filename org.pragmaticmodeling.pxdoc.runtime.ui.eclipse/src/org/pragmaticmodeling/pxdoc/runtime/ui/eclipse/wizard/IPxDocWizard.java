package org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.wizard;

import org.eclipse.jface.wizard.IWizard;
import org.eclipse.ui.plugin.AbstractUIPlugin;

import fr.pragmaticmodeling.pxdoc.runtime.IModelProvider;
import fr.pragmaticmodeling.pxdoc.runtime.IPxDocGenerator;
import fr.pragmaticmodeling.pxdoc.runtime.IStylesheetsRegistry;
import fr.pragmaticmodeling.pxdoc.runtime.launcher.PxDocLauncher;
import org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.SelectionMode;


/**
 * Contract for pxDocGeneration wizards
 */
public interface IPxDocWizard extends IWizard {
	
	public IPxDocGenerator getPxDocGenerator();

	/**
     * Returns the model provider
     * 
     * @return the model provider
     */
    public IModelProvider getModelProvider();

    /**
     * Sets the model provider
     * 
     * @param modelProvider an IModelProvider
     */
    public void setModelProvider(IModelProvider modelProvider);

    /**
     * Get accessor for the launcher instance
     * 
     * @return the launcher instance
     */
    public PxDocLauncher getLauncher();

    /**
     * Set accessor for the launcher instance
     * 
     * @param launcher a launcher
     */
    //public void setLauncher(PxDocLauncher launcher);
    
    /**
     * Returns the generation directory chosen by the user
     * @return a generation directory
     */
    //public String getGenerationDirectory();
    
    /**
     * Returns the selection mode
     * @return the selection mode (file or directory)
     */
    public SelectionMode getSelectionMode();

	public void setStylesheetsRegistry(IStylesheetsRegistry stylesheetsRegistry);
	public IStylesheetsRegistry getStylesheetsRegistry();

	public void setSelection(Object selection);

	public void setPxDocGenerator(IPxDocGenerator pxDocGenerator);

	public void setUiActivator(AbstractUIPlugin uiActivator);
    
	public Object getModel();

	public boolean generateDocuments();
}
