package org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.wizard;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTreeViewer;
import org.eclipse.jface.viewers.ContentViewer;
import org.eclipse.jface.viewers.IBaseLabelProvider;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;

import org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.ISelectionContentProvider;



/**
 * @author bernardr
 * 
 */
public abstract class SelectionGroup extends Composite {

	/**
	 * Identifiant des settings de cette page.
	 */
	public static final String SETTINGS_ID_GENERATEALL_ACTIVE = "SETTINGS_ID_GENERATEALL_ACTIVE";

	public static final String SETTINGS_ID_IDSLIST = "SETTINGS_ID_IDSLIST";

	/**
	 * Le groupe.
	 */
	private Group group = null;

	/**
	 * content provider.
	 */
	private ISelectionContentProvider contentProvider = null;

	/**
	 * checkbox tree viewer.
	 */
	private CheckboxTreeViewer viewer = null;

	/**
	 * Select all.
	 */
	private Button buttonSelectAll = null;

	/**
	 * De-select all.
	 */
	private Button buttonDeselectAll = null;

	private boolean singleSelection;
	
	private List<ISelectionChangedListener> selectionChangedListeners = null;

	/**
	 * Constructor.
	 * 
	 * @param parent
	 *            the parent composite
	 * @param label
	 *            a label for the group
	 * @param pSingleSelection
	 *            true if multiple selection is allowed, otherwise false for
	 *            single selection
	 */
	public SelectionGroup(Composite parent, String label, boolean pSingleSelection) {
		super(parent, SWT.NULL);
		selectionChangedListeners = new ArrayList<ISelectionChangedListener>();
		this.singleSelection = pSingleSelection;
		//setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_DARK_BLUE));
		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 1;
		setLayout(gridLayout);

		group = new Group(this, SWT.SHADOW_ETCHED_IN);

		// gridLayout.marginHeight = 0;
		// gridLayout.verticalSpacing = 0;

		GridLayout groupLayout = new GridLayout();
		groupLayout.numColumns = 2;
		group.setLayout(groupLayout);
		group.setText(label);
		group.setLayoutData(getGridData(GridData.FILL, GridData.FILL, true,
				true, 1));

		contentProvider = getContentProvider();
		//contentProvider.setMatcher(getMatcher());

		if (!singleSelection) {
			buttonSelectAll = getButtonSelectAll();
			buttonDeselectAll = getButtonDeselectAll();
		}
		viewer = getViewer();
		viewer.setContentProvider(contentProvider);
		viewer.setComparator(new StringsViewerComparator());

		addControlListener(new ControlAdapter() {

			@Override
			public void controlResized(ControlEvent e) {
				SelectionGroup.this.controlResized(e);
			}

		});
	}
	
	protected abstract ISelectionContentProvider getContentProvider();
	
	public void addSelectionChangedListener(ISelectionChangedListener pListener) {
		selectionChangedListeners.add(pListener);
	}
	
	public void removeSelectionChangedListener(ISelectionChangedListener pListener) {
		selectionChangedListeners.remove(pListener);
	}
	
    void controlResized(ControlEvent e) {
    	layout(true);
    }

	/**
	 * Initialise les data du viewer contenu dans ce groupe.
	 * 
	 * @param data
	 *            the input of the viewer
	 */
	public final void setInput(Object data) {
		if (viewer != null) {
			viewer.setInput(data);
		}
	}

	/**
	 * Retourne l'input si elle existe.
	 * 
	 * @return the input of the viewer
	 */
	public final Object getInput() {
		if (viewer != null) {
			return viewer.getInput();
		}
		return null;
	}

	/**
	 * Retourne la liste des identifiants s�lectionn�s pour le groupe ou null si
	 * rien n'est s�lectionn�.
	 * 
	 * @return a list of selected ids
	 */
	public final List<String> getSelectedIDs() {
		List<String> ids = new ArrayList<String>();
		Object[] elements = viewer.getCheckedElements();
		for (Object o : elements) {
			if (o instanceof EObject) {
				ids.add(getXMIId((EObject) o));
			}
		}
		if (ids.size() > 0) {
			// On insere d'abord l'id du noeud racine non affich�
			if (contentProvider.getRootEObject() != null) {
				ids.add(0, getXMIId(contentProvider.getRootEObject()));
			}
		}
		return ids;
	}

	/**
	 * Returns the objects selected.
	 * 
	 * @return a list of the selected EObjects
	 */
	public final List<EObject> getSelectedElements() {
		List<EObject> eobjects = new ArrayList<EObject>();

		Object[] elements = viewer.getCheckedElements();
		for (Object o : elements) {
			if (o instanceof EObject) {
				eobjects.add(((EObject) o));
			}
		}

		return eobjects;
	}

	/**
	 * Renvoi un Matcher qui d�cide si un objet sera conserv� ou non.
	 * 
	 * @return the matcher used to filter the content of the tree viewer
	 */
	/*protected Matcher getMatcher() {
		return null;
	}*/

	/**
	 * Set this viewer enabled or not.
	 * 
	 * @param value
	 *            the enabled mode
	 */
	@Override
	public void setEnabled(boolean value) {
		group.setEnabled(value);
		Color color = null;
		if (value) {
			color = viewer.getControl().getDisplay()
					.getSystemColor(SWT.COLOR_WHITE);
		} else {
			color = viewer.getControl().getDisplay()
					.getSystemColor(SWT.COLOR_GRAY);
		}
		viewer.getControl().setBackground(color);
		if (!singleSelection) {
			getButtonSelectAll().setEnabled(value);
			getButtonDeselectAll().setEnabled(value);
		}
	}

	/**
	 * Cree et initialise le bouton permettant de selectionner tout l'arbre.
	 * 
	 * @return the select all button instance
	 */
	protected final Button getButtonSelectAll() {
		if (buttonSelectAll == null) {
			buttonSelectAll = new Button(group, SWT.PUSH);
			buttonSelectAll.setText(Messages.selectAll);
			buttonSelectAll.setLayoutData(getGridData(
					GridData.VERTICAL_ALIGN_BEGINNING,
					GridData.HORIZONTAL_ALIGN_BEGINNING, false, false, 1));
			buttonSelectAll.addSelectionListener(new SelectionAdapter()  {
				

				@Override
				public void widgetSelected(SelectionEvent arg0) {
					handleSelection(arg0);
				}

				@SuppressWarnings("deprecation")
				private void handleSelection(SelectionEvent e) {
					viewer.expandAll();
					viewer.setAllChecked(true);
					updateStates();
					notifyListeners();
				}
			});
		}
		return buttonSelectAll;
	}
	
	private void notifyListeners() {
		SelectionChangedEvent lEvent =  new SelectionChangedEvent(getViewer(), new StructuredSelection(getSelectedElements()));
		for (ISelectionChangedListener lListener : selectionChangedListeners) {
			lListener.selectionChanged(lEvent);
		}
	}
	
	/**
	 * Cree et initialise le bouton permettant de d�selectionner tout l'arbre.
	 * 
	 * @return the 'deselect all' button instance
	 */
	protected final Button getButtonDeselectAll() {
		if (buttonDeselectAll == null) {
			buttonDeselectAll = new Button(group, SWT.PUSH);
			buttonDeselectAll.setText(Messages.deselectAll);
			buttonDeselectAll.setLayoutData(getGridData(
					GridData.VERTICAL_ALIGN_BEGINNING,
					GridData.HORIZONTAL_ALIGN_BEGINNING, false, false, 1));
			buttonDeselectAll.addSelectionListener(new SelectionAdapter() {
				
				@Override
				public void widgetSelected(SelectionEvent arg0) {
					handleSelection(arg0);
				}

				@SuppressWarnings("deprecation")
				private void handleSelection(SelectionEvent e) {
					viewer.expandAll();
					viewer.setAllChecked(false);
					updateStates();
					notifyListeners();
				}
			});
		}
		return buttonDeselectAll;
	}

	/**
	 * Cree et initialise le viewer arbre.
	 * 
	 * @return the treeviewer widget instance
	 */
	protected final CheckboxTreeViewer getViewer() {

		final CheckboxTreeViewer v = new CheckboxTreeViewer(group);
		v.getTree().setLayoutData(new GridData(GridData.FILL_BOTH));
		v.getTree().setLayoutData(
				getGridData(GridData.FILL, GridData.FILL, true, true, 2));
		v.setLabelProvider(getLabelProvider());

		v.addCheckStateListener(new ICheckStateListener() {

			@Override
			public void checkStateChanged(CheckStateChangedEvent event) {
				if (!singleSelection) {
					v.setSubtreeChecked(event.getElement(), event.getChecked());
				}
				oneCheckStateChanged(event.getChecked(), event.getElement(), v,
						contentProvider);
				notifyListeners();
			}
		});

		return v;
	}

	protected abstract ILabelProvider getLabelProvider();
	
	/**
	 * Test if something has been selected.
	 * 
	 * @return true if at least one element is selected.
	 */
	public final boolean somethingIsSelected() {
		// Quand une case est activee ou desactivee, on notifie les abonnes
		// de la liste des elements du model s�lectionn�s
		boolean somethingIsSelected = false;
		if (viewer.getCheckedElements().length > 0) {
			somethingIsSelected = true;
		}
		return somethingIsSelected;
	}

	/**
	 * Build dialog settings.
	 * 
	 * @param s
	 *            the dialog settings to be built
	 */
	public void fillDialogSettings(IDialogSettings s) {
		if (s != null) {
			Object[] ids = getCheckedElements();
			if (ids != null && ids.length > 0) {
				String[] tmp = new String[ids.length];
				for (int i = 0; i < ids.length; i++) {
					tmp[i] = ids[i].toString();
				}
				s.put(SETTINGS_ID_IDSLIST, tmp);
			}
		}
	}

	/**
	 * Load dialog settings.
	 * 
	 * @param s
	 *            the dialog settings to be loaded
	 */
	@SuppressWarnings("unchecked")
	public void loadDialogSettings(IDialogSettings s) {

		if (s != null) {
			// Gets the model
			if (s.getArray(SETTINGS_ID_IDSLIST) != null) {
				List<String> allSelectedIds = Arrays.asList(s
						.getArray(SETTINGS_ID_IDSLIST));
				ISelectionContentProvider ptcp = (ISelectionContentProvider) viewer
						.getContentProvider();
				List<Object> allElements = ptcp.getAllElements(viewer.getInput());
				@SuppressWarnings("rawtypes")
				List selectedElementsList = new ArrayList();

				for (Object value : allElements) {
					if (value instanceof EObject) {
						EObject eobject = (EObject)value;
						if (allSelectedIds.contains(getXMIId(eobject))) {
							selectedElementsList.add(eobject);
						}
					}
				}

				Object[] selectedElements = selectedElementsList.toArray();
				for (Object element : selectedElements) {
					viewer.expandToLevel(element, 0);
				}
				viewer.setCheckedElements(selectedElements);
			}
			updateStates();
		}
	}

	/**
	 * Setter for the selected elements.
	 * 
	 * @param pSelectedElements
	 *            the elements to be selected
	 */
	public void setSelectedElements(
			@SuppressWarnings("rawtypes") List pSelectedElements) {
		for (Object lObject : pSelectedElements) {
			oneCheckStateChanged(true, lObject, viewer, contentProvider);
		}
	}

	/**
	 * Mise a jour des elements graphiques.
	 */
	private void updateStates() {
	}

	/**
	 * Retourne la liste des identifiantss check�s ou null si rien n'est
	 * s�lectionn�.
	 * 
	 * @return the checked elements
	 */
	private Object[] getCheckedElements() {
		List<Object> ids = new ArrayList<Object>();
		Object[] elements = viewer.getCheckedElements();
		for (Object o : elements) {
			if (o instanceof EObject) {
				ids.add(getXMIId((EObject) o));
			}
		}

		if (ids.size() > 0) {
			// On insere d'abord l'id du noeud racine non affich�
			if (contentProvider.getRootEObject() != null) {
				ids.add(0, getXMIId(contentProvider.getRootEObject()));
			}
			return ids.toArray();
		}

		return null;
	}

	/**
	 * Permet de positionner un comportement specifique si necessaire lors du
	 * changement d'�tat d'une case � cocher. Le comportement par d�faut est de
	 * ne rien faire.
	 * 
	 * @param pState
	 *            true if the element is checked
	 * @param pElement
	 *            the element
	 * @param pViewer
	 *            the viewer
	 * @param pContentProvider
	 *            the content provider
	 */
	@SuppressWarnings("deprecation")
	protected void oneCheckStateChanged(boolean pState, Object pElement,
			CheckboxTreeViewer pViewer,
			ISelectionContentProvider pContentProvider) {
		// if (state == true) {
		if (singleSelection) {
			pViewer.setAllChecked(false);
		}
		pViewer.setChecked(pElement, pState);

		// On recherche la structure parent
		Object parentElement = pContentProvider.getParent(pElement);
		while (parentElement != null) {
			if (allChildrenAreChecked(parentElement)) {
				pViewer.setGrayChecked(parentElement, !pState);
				pViewer.setChecked(parentElement, pState);
			} else if (noChildrenAreChecked(parentElement)) {
				pViewer.setChecked(parentElement, false);
				pViewer.setGrayChecked(parentElement, false);
			} else {
				pViewer.setChecked(parentElement, !pState);
				pViewer.setGrayChecked(parentElement, true);
			}
			parentElement = pContentProvider.getParent(parentElement);
		}

		if (!singleSelection) {
			pViewer.setSubtreeChecked(pElement, pState);
		}
	}

	/**
	 * Returns true if all sub-elements of the given element are checked.
	 * 
	 * @param element
	 *            the element for which we want to know if all of its children
	 *            are selected
	 * @return true if all the children of the given element are selected
	 */
	private boolean allChildrenAreChecked(Object element) {
		for (Object lObject : contentProvider.getChildren(element)) {
			if (!viewer.getChecked(lObject)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Returns true if no children of the given element are selected.
	 * 
	 * @param element
	 *            the element for which we want to know if all of its children
	 *            are Deselected.
	 * @return true if no children of the given element are selected
	 */
	private boolean noChildrenAreChecked(Object element) {
		for (Object lObject : contentProvider.getChildren(element)) {
			if (viewer.getChecked(lObject)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * A comparator that allow to sort elements of the tree.
	 */
	private static class StringsViewerComparator extends ViewerComparator {

		/**
		 * Constructor.
		 */
		StringsViewerComparator() {
			super();
		}

		@Override
		public int compare(Viewer viewer, Object e1, Object e2) {
			int cat1 = category(e1);
			int cat2 = category(e2);

			if (cat1 != cat2) {
				return cat1 - cat2;
			}

			String name1;
			String name2;

			if (viewer == null || !(viewer instanceof ContentViewer)) {
				name1 = e1.toString();
				name2 = e2.toString();
			} else {
				IBaseLabelProvider prov = ((ContentViewer) viewer)
						.getLabelProvider();
				if (prov instanceof ILabelProvider) {
					ILabelProvider lprov = (ILabelProvider) prov;
					name1 = lprov.getText(e1);
					name2 = lprov.getText(e2);
				} else {
					name1 = e1.toString();
					name2 = e2.toString();
				}
			}
			if (name1 == null) {
				name1 = "";//$NON-NLS-1$
			}
			if (name2 == null) {
				name2 = "";//$NON-NLS-1$
			}

			// to lower case
			name1 = name1.toLowerCase();
			name2 = name2.toLowerCase();

			// use the comparator to compare the strings
			return getComparator().compare(name1, name2);
		}

	}

	/**
	 * Helper method to initialize grid data.
	 * 
	 * @param verticalAlignment
	 *            vertival alignment
	 * @param horizontalAlignment
	 *            horizontal alignment
	 * @param grabExcessVerticalSpace
	 *            grab excess vertcal space
	 * @param grabExcessHorizontalSpace
	 *            grab excess horizontal space
	 * @param horizontalSpan
	 *            horizontal span
	 * @return a GridData instance
	 */
	private static GridData getGridData(int verticalAlignment,
			int horizontalAlignment, boolean grabExcessVerticalSpace,
			boolean grabExcessHorizontalSpace, int horizontalSpan) {

		GridData gridData = new GridData();

		gridData.verticalAlignment = verticalAlignment;
		gridData.grabExcessVerticalSpace = grabExcessVerticalSpace;
		gridData.horizontalAlignment = horizontalAlignment;
		gridData.grabExcessHorizontalSpace = grabExcessHorizontalSpace;
		gridData.horizontalSpan = horizontalSpan;

		return gridData;
	}

	/**
	 * Retourne l'id XMI de l'objet EMF.
	 * 
	 * @param e
	 *            the emf element
	 * @return a xmi uuid
	 */
	private static String getXMIId(EObject e) {
		if (e != null && e.eResource() != null) {
			return e.eResource().getURIFragment(e);
		}
		return null;
	}

	/**
	 * 
	 */
	/*
	 * public final void setLayoutData(GridData layoutData) {
	 * this.group.setLayoutData(layoutData); }
	 */
}
