package org.pragmaticmodeling.pxdoc.runtime.ui.eclipse;

import org.eclipse.jface.dialogs.IDialogSettings;

import fr.pragmaticmodeling.pxdoc.runtime.launcher.PxDocLauncher;

public interface IPxDocWizardPage {

	IDialogSettings buildDialogSettings();
	void loadDialogSettings(IDialogSettings pSettings);
	String getSectionId();
	void fillLauncher(PxDocLauncher pLauncher);
}
