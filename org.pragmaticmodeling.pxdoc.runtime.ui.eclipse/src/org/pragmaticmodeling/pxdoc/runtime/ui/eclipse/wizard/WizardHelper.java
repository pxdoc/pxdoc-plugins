package org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.wizard;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.wizard.WizardPage;

public class WizardHelper {
	/**
	 * Applies the status to the status line of a dialog page.
	 */
	public final static void applyToStatusLine(WizardPage page, IStatus status) {
		String message= status.getMessage();
		if (message.length() == 0) message= null;
		switch (status.getSeverity()) {
			case IStatus.OK:
				page.setErrorMessage(null);
				page.setMessage(message);
				break;
			case IStatus.WARNING:
				page.setErrorMessage(null);
				page.setMessage(message, WizardPage.WARNING);
				break;				
			case IStatus.INFO:
				page.setErrorMessage(null);
				page.setMessage(message, WizardPage.INFORMATION);
				break;			
			default:
				page.setErrorMessage(message);
				page.setMessage(null);
				break;		
		}
	}
}
