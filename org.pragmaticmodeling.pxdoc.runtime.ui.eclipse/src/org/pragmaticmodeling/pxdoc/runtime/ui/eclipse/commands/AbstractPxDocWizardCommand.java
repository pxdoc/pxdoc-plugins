package org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.commands;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.expressions.IEvaluationContext;
import org.eclipse.core.runtime.jobs.IJobManager;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.ISources;
import org.eclipse.ui.handlers.HandlerUtil;

import com.google.inject.Inject;
import com.google.inject.Injector;

import fr.pragmaticmodeling.pxdoc.runtime.DefaultModelProvider;
import fr.pragmaticmodeling.pxdoc.runtime.IModelProvider;
import fr.pragmaticmodeling.pxdoc.runtime.IPxDocGenerator;
import fr.pragmaticmodeling.pxdoc.runtime.IResourceProvider;
import fr.pragmaticmodeling.pxdoc.runtime.IStylesheetsRegistry;
import fr.pragmaticmodeling.pxdoc.runtime.PxDocConstants;
import fr.pragmaticmodeling.pxdoc.runtime.eclipse.jobs.MutexRule;
import fr.pragmaticmodeling.pxdoc.runtime.eclipse.jobs.PxDocGeneratorJob;
import fr.pragmaticmodeling.pxdoc.runtime.guice.IGuiceAware;
import fr.pragmaticmodeling.pxdoc.runtime.launcher.PxDocLauncher;
import org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.AbstractPxUiPlugin;
import org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.wizard.IPxDocWizard;
import org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.wizard.Messages;
import org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.wizard.PxDocWizardDialog;

public abstract class AbstractPxDocWizardCommand extends AbstractHandler implements IGuiceAware {

	protected Object selectedModel;
	private Shell shell;
	protected MutexRule mutexRule = null;
	@Inject
	private IModelProvider modelProvider;
	@Inject
	IResourceProvider provider;

	private IPxDocGenerator pxDocGenerator;
	
	public IPxDocGenerator getPxDocGenerator() {
		if (pxDocGenerator == null) {
			pxDocGenerator = createGenerator();
			initLauncher(pxDocGenerator.getLauncher());
		}
		return pxDocGenerator;
	}

	@Inject
	IStylesheetsRegistry stylesheetsRegistry;
	private Injector injector;
	@Inject
	private AbstractPxUiPlugin uiActivator;

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		pxDocGenerator = null;
		selectedModel = resolveInputModel(event);
		if (selectedModel == null) {
			ISelection selection = getCurrentStructuredSelection(event);
			this.selectedModel = null;
			// FIXME pourquoi limiter la sélection à un seul élément ?
			if (selection != null && selection instanceof IStructuredSelection
					&& !((IStructuredSelection) selection).isEmpty()) {
				Object currentSelection = ((IStructuredSelection) selection).getFirstElement();
				this.selectedModel = currentSelection;
			}
		}
		if (selectedModel == null) {
			if (event.getApplicationContext() instanceof IEvaluationContext) {
				IEditorInput editorInput = HandlerUtil.getActiveEditorInput(event);
				if (editorInput != null) {
					selectedModel = editorInput;
				}
			}
		}

		// Wizard creation
		IPxDocWizard wizard = createWizard(this.selectedModel);
		wizard.setStylesheetsRegistry(stylesheetsRegistry);

		// Instantiates the wizard container with the wizard and opens it
		PxDocWizardDialog dialog = new PxDocWizardDialog(shell, wizard);
		dialog.create();
		dialog.open();
		if (dialog.getReturnCode() == Window.CANCEL)
			return null;

		final IPxDocGenerator pxdocGenerator = wizard.getPxDocGenerator();
		beforeGenerate(pxdocGenerator);
		PxDocGeneratorJob pxdocJob = new PxDocGeneratorJob("", pxdocGenerator, provider);
		pxdocJob.setUser(true);
		pxdocJob.schedule();
		return null;
	}

	protected Object resolveInputModel(ExecutionEvent event) {
		return null;
	}

	protected void beforeGenerate(IPxDocGenerator pxdocGenerator) {

	}

	protected void cancelJobFamily() {
		IJobManager jobMan = Job.getJobManager();
		jobMan.cancel(PxDocConstants.PXDOC_JOB_FAMILY);
	}

	protected void initLauncher(PxDocLauncher launcher) {
		launcher.setPluginId(uiActivator.getPxDocGeneratorPluginId());
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	protected void reportError(String message) {
		MessageDialog.openError(shell, Messages.pxDocDialogErrorTitle, message);
	}

	protected void reportWarning(String message) {
		MessageDialog.openWarning(shell, Messages.pxDocDialogWarningTitle, message);
	}

	private IPxDocWizard createWizard(Object selection) {
		IPxDocWizard pxDocWizard = injector.getInstance(IPxDocWizard.class);
		pxDocWizard.setSelection(selection);
		pxDocWizard.setPxDocGenerator(getPxDocGenerator());
		pxDocWizard.setModelProvider(getModelProvider());
		pxDocWizard.setUiActivator(uiActivator);
		return pxDocWizard;
	}

	private IPxDocGenerator createGenerator() {
		IPxDocGenerator pxDocGenerator = injector.getInstance(IPxDocGenerator.class);
		return pxDocGenerator;
	}

	private IModelProvider getModelProvider() {
		if (modelProvider == null) {
			modelProvider = new DefaultModelProvider();
		}
		return modelProvider;
	}

	protected static IStructuredSelection getCurrentStructuredSelection(ExecutionEvent event) {
		ISelection selection = getCurrentSelection(event);
		if (selection instanceof IStructuredSelection) {
			return (IStructuredSelection) selection;
		}
		return StructuredSelection.EMPTY;
	}

	protected static ISelection getCurrentSelection(ExecutionEvent event) {
		Object o = getVariable(event, ISources.ACTIVE_CURRENT_SELECTION_NAME);
		if (o instanceof ISelection) {
			return (ISelection) o;
		}
		return null;
	}

	protected static Object getVariable(ExecutionEvent event, String name) {
		if (event.getApplicationContext() instanceof IEvaluationContext) {
			Object var = ((IEvaluationContext) event.getApplicationContext()).getVariable(name);
			return var == IEvaluationContext.UNDEFINED_VARIABLE ? null : var;
		}
		return null;
	}

	@Override
	public void initialize(Injector injector) {
		this.injector = injector;
		getPxDocGenerator().initialize(injector);
	}
}
