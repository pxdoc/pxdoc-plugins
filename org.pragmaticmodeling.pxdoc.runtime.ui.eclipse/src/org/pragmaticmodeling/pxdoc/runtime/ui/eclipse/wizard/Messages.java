package org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.wizard;

import org.eclipse.osgi.util.NLS;


public class Messages extends NLS {


	static {
		NLS.initializeMessages("messages", Messages.class); //$NON-NLS-1$
	}


	private Messages() {
	}


	public static String pxDocWizardTitle;

	public static String pxDocDialogErrorTitle;

	public static String pxDocDialogWarningTitle;

	public static String pxDocDialogSelectionWarning;

	public static String pxDocErrorNoSelection;
	
    public static String selectAll;
    public static String deselectAll;

}
