package org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.commands;

import org.eclipse.core.expressions.IPropertyTester;

@Deprecated
public interface IPxDocInputProvider extends IPropertyTester {

	<T extends Object> T adapt(Object object);
}
