package org.pragmaticmodeling.pxdoc.runtime.ui.eclipse;

/**
 * Valid selection modes. Can be a File selection or a Directory selection
 * @author Romain BERNARD
 */
public enum SelectionMode {
    file,
    directory;
}