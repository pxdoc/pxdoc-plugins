document "pxDoc - Common Libraries and Plugins" {

	@html '<p>pxDoc Common Libraries and Plugins are a set of pxDoc Modules and/or Java classes or interfaces providing useful capabilities reusable in any pxDoc Generator. Among others, you can:</p>		
		<ul>
			<li>query and include diagrams from your models,</li>
			<li>provide generic templates for EMF model,</li>
			<li>manipulate images (convert to AWT, split large images on several pages...)</li>
			<li>...</li>
		</ul> 
		<p><i>The language renderers are part of the pxDoc common plugins but they are described in a dedicated section.</i></p>
		'
		
	@html class:note 'There is no obligation to use these common libraries. You can also develop your own libraries, or extend the provided ones to match your specific needs.'
	
	section importModules  "Importing sources into your workspace" {
		@html '	<p>You can of course import the common libraries in your workspace to get a view on the common Modules and libraries included in pxDoc</p>'
		image "images/common-libs/import-libs-as-source.PNG" width:400
	}  
	
	section pxDocCommonLib @html "Common Services / <i>org.pragmaticmodeling.pxdoc.common.lib</i>" {
		@html class:note 'The Common Services library is provided for demonstration purpose, and as a support for the set of generator examples shipped with pxDoc. You are free to use or not this library, or find inspiration to define your own <i>"common lib"</i>.'
		section pxDocCommonServices "CommonServices.pxdoc Module" {
			table {
				row{
					cell {
						@html 'The <i>CommonServices.pxdoc</i> module provides generic templates and functions that can be used by every documentation generator:
						<ul><li>manage basic presentation of an object (so far, bookmarks management and objects descriptions retrieval). This is useful if your generator consumes many different java objects, with different ways of storing documentation, occasionally in many different documentation formats (raw text, html, markdown...)</li>
						<li>provide basic reusable templates for optional or required documentation</li>
						<li>provide a way to improve your documents, with the <i>suggestImprovements</i> boolean parameter. When set to true, warnings are generated in the document to enforce writing a documentation for the concerned objects.</li>
						<li>... and more useful generic services to come in the next releases!</li>
						</ul>'
						
						}
					cell {
						image "images/common-libs/commonServices.PNG"
					}
				}
			}
			@html class:note '<p><b>CommonServices setup</b></p>
			<p><b>OPTIONAL</b> - When using the CommonServices module in your generator, you can inject your custom instance of IDescriptionProvider (see example below).</p>'
					
		}
		
		section pxDocDescriptionProvider @html "The <i>IDescriptionProvider</i> Interface" {
			table {
				row{
					cell {
						@html '<p>Among all properties that an object can hold, its human readable descriptions play a particular role. They are usually generated early when documenting an object, while other business properties are treated differently in following document sections.</p>'
						@html '<p>The <i>IDescriptionProvider</i> interface gives the ability to return a description for every object processed by a pxDoc generator, as well as the expected description format (e.g., html, raw text, markdown...)</p>'
						@html '<p>It is also responsible to provide a valid bookmark id for every object processed during generation, and decide which objects should be generated with a bookmark.</p>'
					}
					cell {
						image "images/common-libs/IDescriptProvider.PNG"
					}
				}
			}
			@html '<p>The class <i>DefaultDescriptionProvider</i> is the default implementation of <i>IDescriptionProvider</i>. It returns null as a description for every object, but is able to generate valid bookmarks for every object.</p>
 			<p>If you plan to use the CommonServices module, you will need to :
			<ul><li>provide your own <i>IDescriptionProvider</i> by subclassing <i>DefaultDescriptionProvider</i> and provide your own implementations of the getDescription*(*) methods.
			<li>inject your IDescriptionProvider instance to the CommonServices when starting the generation (in the root template):</li></ul>
			</p>'
 			@pxdoc 'root template main(Object model) {

	// Commons lib configuration : set a description provider able to get information about object description
	descriptionProvider = new MyDescriptionProvider(this)
	
	document {
		// content...
	}

}'
 			
		}
		
		
//		@html '<i>/org.pragmaticmodeling.pxdoc.common.lib</i>'
//		@html '<p>The Common Lib provides the basis to include the description of an element or object. It has been re-used in all of the generic generators provided with the enablers (as <a href="#EmfServices">EMF Service Library</a> for all environments based on EMF). The Common library consists of:</p>
//				<ul><li>a Description Provider interface that specifies how to get and to render to object description</li>
//				<li>an implementation of this interface as a default Description Provider
//				<li>the CommonServices pxDoc Module, providing basic templates to include the description in the document</li>
//				</ul>'
//		section commonServices "CommonServices Module"{
//			table {
//				row{
//					cell {
//						@html '<p>This pxDoc Module proposes few templates to provide the documentation (description) of an object.</p>
//							<p> A <i>suggestImprovement</i> boolean allow to include things differently in your document, according to you need to highlight a missing description or not:</p>
//							<ul><li><i>documentation</i>: if it is not the case, a default string is included (not the same if <i>suggestImprovement</i> is true or false.</li>
//							<li><i>optionalDocumentation</i>: nothing is included if there is no description</li>
//							<li>smartDocumentation</i>: you want either to highlight a missing description or to do nothing (mix of the 2 above)</li>
//							</ul>
//							<p>You also have functions to directly get and set things in the Description Provider.</p>'
//					}
//					cell {
//						image "images/common-libs/commonServices.PNG"
//					}
//				}
//			}
//		}
	} // end of common.lib
	
	section emfCommonLib @html "EMF Services / <i>org.pragmaticmodeling.pxdoc.emf.lib</i>" {
		@html class:note 'The Emf Services library is provided for demonstration purpose, and as a support for the set of generator examples shipped with pxDoc. You are free to use or not this library, or find inspiration to define your own <i>"emf lib"</i>.'
		
		table {
			row{
				cell {
					@html '<p>The EmfServices module is a set of templates and functions that allow to generate documentation for any EMF EObject. It takes benefit of the reflective EMF API to transform EObjects into documentation.</p>'
					@html '<p>It defines two ways of EObject-to-doc transformation:
					<ul><li><i>DocKind.form</i>, for a form-like presentation of EObject features (attributes, references, methods)</li>
					<li><i>DocKind.table</i>, for a table regrouping all EObject features</li></ul></p>'
					@html '<p>You can get your documentation for an EObject by invoking the <i>genericDoc</i> template:</p>'
					@pxdoc 'var int level = 1
var boolean recursive = true
apply genericDoc(anEObject, DocKind.^table, level, recursive)'
					@html "<p>With:
					<ul><li><i>anEObject</i>, the model object</li>
					<li><i>docKind</i>, between <i>form</i> or <i>table</i></li>
					<li><i>level</i> gives the heading (title) level that should be applied to the model object</li>
					<li><i>recursive</i>, to generate the documentation recursively on the object's children</li>
					</ul></p>"
					
					
//					@html '<p>Module providing some very interesting basic templates to include the name and description of an element:</p>
//							
//							<ul>
//							<li>Display the icon of the element just before its name</li>
//							<li>Display element attributes in a table or in a bullet list</li>
//							<li>
//							</ul>
//							'
					
				}
				cell {
					image "images/common-libs/emfServices.PNG"
				}
			}
		}
		@html "<p>And because some EObject's attributes are too technical, or rarely/never used, the EmfServices module provide a way to filter some EStructuralFeature from the generation. This is the role of the field <i>excludedFeatures</i>, which you can access the following way:</p>"
					@pxdoc 'root template main(Object model) {

	// exclude some EClasses
	excludedEClasses += EcorePackage.Literals.EANNOTATION
	// exclude EStructualFeatures
	excludedFeatures += EcorePackage.Literals.ECLASSIFIER__DEFAULT_VALUE
	excludedFeatures += EcorePackage.Literals.EMODEL_ELEMENT__EANNOTATIONS
	
	document {
		// content...
	}

}
'
		@html '<p>The EmfServices.pxdoc files contains comments to explain each template. Do not hesitate to have a look at it.</p>
		<p>You can also refer to the EMF Example to see how these templates can be used.</p>'
		
		@html '<p><b>EmfServices setup</b></p>
		<p><b>OPTIONAL</b> - When using the EmfServices module in your generator, you can inject your custom instances of:
		<ul><li>labelProvider = myLabelProvider</li>
		<li>contentProvider = myContentProvider</li></ul></p>
		<p><b>OPTIONAL</b> - You can also define filters to exclude some model elements or features from the generation, by adding EClass or EStructuralFeature to the two following sets:
		<ul><li>excludedEClasses += MyEmfPackage.Literals.MY_CLASS</li>
		<li>excludedFeatures += MyEmfPackage.Literals.MY_CLASS__FEATURE</li></ul></p>'
	}
	
	section diagramServices @html "Diagram Services / <i>org.pragmaticmodeling.pxdoc.diagrams.lib</i>" {
		
		@html class:note 'The Diagram Services library is provided for demonstration purpose, and as a support for the set 
		of generator examples shipped with pxDoc. You are free to use or not this library, or find inspiration to define 
		your own <i>"diagrams lib"</i>.'
		
		section pxDocDiagramsMetamodel 'pxDoc Diagrams metamodel' {
			
			@html '<p>A very simple EMF metamodel has been defined to manipulate diagrams in pxDoc. It is composed of a root object <i>Diagrams</i>, which contains a list of <i>Diagram</i>.</p>'
			image "images/common-libs/diagram-metamodel.png"
			
		}
		
		section pxDocDiagramsMetamodel 'pxDoc Diagrams runtime' {
			@html '<p>The diagrams library provides a set of interfaces to query diagrams from an object.</p>'
			
			table {
				row{
					cell width:"60%"{
						
						@html '<p>The diagrams library provides a set of interfaces to query diagrams from an object:
						<ul><li><b>IDiagramProvider</b>: this is the entry point to query diagrams, by invoking its <i>createQuery(modelObject)</i> method, which returns an <i>IDiagramQueryBuilder</i> object. </li>
							<li><b>IDiagramQueryBuilder</b>: defines a set of simple queries (by name, by type, by non empty diagrams...), that can be stacked to the query. When the query is complete, the <i>execute()</i> method has to be called to run the query. For example, there are 2 unit queries in the following query: <i>diagramProvider.createQuery(sourceObjectWithDiagrams)<b>.type("CDB").notEmpty()</b>.execute</i>.</li>
							<li><b>IDiagramQuery</b>: a POJO for the diagram query</li>
							<li><b>IDiagramQueryRunner</b>: will run the <i>IDiagramQuery</i></li>
							</ul>'
						
						@html '<p>The <a href="#diagramServices">Diagram library</a> provides useful templates to include diagrams.</p>
						<p>You can also find samples in the examples provided with the pxDoc enablers</p>'		
					}
					cell {
						image "images/common-libs/DiagramsPlugin-classdiagram.png" width:600
					}
				}
			}
		}
		
		section pxDocDiagramServices 'DiagramServices.pxdoc Module' {
			
			section @html "Diagram queries with <i>queryDiagrams(Object)</i>" {
				@html '<p>Samples of queries:</p>'
				
					@pxdoc "
// diagrams owned by the 'model' object
var List<Diagram> ownedDiagrams = queryDiagrams(model).execute;
		
// all diagrams owned directly and indirectly by the 'model' object
var allOwnedDiagrams = queryDiagrams(model).searchNested.execute;
		
// diagrams owned by the 'model' object, with some content
var ownedNonEmptyDiagrams = queryDiagrams(model).notEmpty.execute;
		
// look for a diagram owned by 'model' and named 'A diagram name'
var diagramsByName = queryDiagrams(model).name('A diagram name').execute;
		
// look for a diagram owned by 'model' with name staring with 'ABC'
var diagramsByNameStartingWith = queryDiagrams(model).nameStartsWith('ABC').execute;
		
// diagrams by type
var diagramsByType_CDB = queryDiagrams(model).type('CDB').execute;
var diagramsByType_ES = queryDiagrams(model).type('ES').execute;
var diagramsByType_CDI = queryDiagrams(model).type('CDI').execute;
		
// diagrams with type 'CDB' and name starting with 'MyString'
var cdbStartingWithMyString = queryDiagrams(model).type('CDB').nameStartsWith('MyString').execute;"
				}
			
			section "Include and manipulate diagrams" {
				
				@html "<p>You can use the includeDiagram template to include your diagrams:</p>"
				@pxdoc "
// Get a list of matching diagrams
var List<Diagram> myDiagrams = queryDiagrams(model).execute;
	
// insert the list of diagrams
apply includeDiagram(myDiagrams)"
				
				@html "<p>Or you can also insert diagrams with your own template, as shown in the following example:</p>"
				
				@pxdoc "
// To insert the diagram image by yourself:
for (diagram : myDiagrams) {
					
	// use the pxdoc 'image' keyword with diagram image file path 
	// stored in the 'path' diagram attribute (was 'data' in pxDoc v1.1 and previous).
	image path:diagram.path  
	// add a title with a style
	#Figure keepWithNext align:center {
	diagram.name
	}
	// include diagram documentation if any
	if (!diagram.documentation.empty) {
		#BodyText {diagram.documentation}
	}
}"

@html "<p>And you can get the source diagram object to extract some information from it (get the semantic elements...):</p>"
				
				@pxdoc "
for (diagram : myDiagrams) {
	//Below, 'MyDiagramType' to be replace according to your environment: 'DDiagram' for Sirius, 'Diagram' for Papyrus...
	var myDiagramObject=diagram.diagramObject as MyDiagramType 

	// Do whatever you need with the object...
}"
				
			}
			
			@html class:note '<p><b>DiagramServices setup</b></p>
						<p><b>REQUIRED</b> - When using the DiagramServices module in your generator, you must inject an <i>IDiagramProvider</i> suited to your target platform.</p>
						<p>diagramProvider = myToolDiagramProvider</p>'
			
//			table {
//				row{
//					cell {
//						
//						@html '<p>Once properly configured (see DiagramServices setup below), the DiagramServices module allows:
//						<u><li>query diagrams with queryDiagrams function:</li></u></p>'
//						@pxdoc '// query for 
//var List<Diagram> myDiagrams = queryDiagrams(myObject).execute'
//
//						@html '<p>A pxDoc Module provides the entry point to query diagrams (see the <a href="#diagramPlugin">Diagram Plugin</a> section) and some templates to include diagrams in your document:</p>
//								
//								<ul>
//								<li>Query all non empty diagrams directly owned by an element (Object or EObject)</li>
//								<li>Include each diagram with a bookmark, a title (with a dedicated style to be bound with the <i>Caption</i> style, so Word can generate a Table of Figures) and a description if any.</li>
//								</ul>
//								'
//						@html '<p>Of course, you can create in your own templates and modules to easily: apply other typical queries, include diagrams in different ways (title before, no description...).</p>
//						<p>You can find samples in the examples provided with the pxDoc enablers</p>'
//						@html class:note '<p><b>DiagramServices setup</b></p>
//						<p><b>REQUIRED</b> - When using the DiagramServices module in your generator, you must inject an <i>IDiagramProvider</i> suited to your target platform.</p>
//						<p>diagramProvider = myToolDiagramProvider</p>'
//					}
//					cell {
//						image "images/common-libs/DiagramsLib-overview.PNG"
//					}
//				}
//			}
			
		}
		
			
	}
	
//	section pxDocAndDiagrams "pxDoc and Diagrams" {
//		
//		section diagramPlugin "Diagrams plugin"{
//			@html '<i>/org.pragmaticmodeling.pxdoc.plugins.diagrams</i>'
//			table {
//				row{
//					cell {
//						@html '<p>Set of interfaces to query diagrams from an object. <br/>At least <i>IQueryRunner</i> has to be implemented for each tool/environment. You can find sample implementations in many pxDoc enablers (Sirius, Papyrus...).</p>
//							
//							<ul><li><b>IDiagramProvider</b>: defines the way to create a diagram query (e.g. get a set of diagrams as result)</li>
//							<li><b>IQuery</b>: POJO to provide the query parameters</li>
//							<li><b>IQueryBuilder</b>: functions defining unit queries (query by name, by type, only non empty diagrams...). These unit queries can be stacked, so each function returns the query itself until the <i>execute()</i> function is called. For example, there are 2 unit queries in the following query: <i>DiagramProvider.CreateQuery(sourceObjectWithDiagrams)<b>.type("CDB").notEmpty()</b>.execute</i>.</li>
//							<li><b>IQueryRunner</b>: runs the Query</li>
//							</ul>'
//					@html '<p>The <a href="#diagramServices">Diagram library</a> provides useful templates to include diagrams.</p>
//					<p>You can also find samples in the examples provided with the pxDoc enablers</p>
//					'		
//					}
//					cell {
//						image "images/common-libs/DiagramsPlugin-classdiagram.png"
//					}
//				}
//			}
//			
//		}
//	}
	
	section imagesLibrary @html "Images Services / <i>org.pragmaticmodeling.pxdoc.images.lib</i>"{
		
		@html class:note 'The Images Services library is provided for demonstration purpose, and as a support for the set 
		of generator examples shipped with pxDoc. You are free to use or not this library, or find inspiration to define 
		your own <i>"images lib"</i>.'
		
		table {
			row{
				cell {
					@html 'Set of function allowing to:
						<ul><li>Split large images in to several ones (and calculate automatically if split is necessary)</li>
						<li>Crop an image, to display only a part of it</li>
						<li>Scale an image to keep its aspect ratio</li>
						<li>Convert a Java Image object into an AWT object (pxDoc can manipulate image files or AWT objects)</li>
						</ul>'
				}
				cell {
					image "images/common-libs/imagesLib-classDiagram.PNG"
				}
			}
		}
	}
	
	
//	section pxDocPlugins "pxDoc common plugins"{
//		
//		
//		section pict2Ascii "PictureToAscii  plugin"{
//			@html '<i>/org.pragmaticmodeling.pxdoc.plugins.picturetoascii</i>'
//			table {
//				row{
//					cell {
//						@html '<p>This plugin is a fun example to show that you can potentially directly include Java objects (consistent with the pxDoc metamodel) that describes a part to include in the generated document.</p>
//									<p>This is part of the incredible capability of extension of pxDoc. It will be better documented soon...</p>
//									'
//					}
//					cell {
//						image "images/common-libs/pict2Ascii-overview2.PNG"
//						image "images/common-libs/pict2Ascii-overview.PNG"
//					}
//				}
//			}
//		}
//	} // end of Plugins
	
//	section pxDocLibraries "pxDoc common libraries"{
//		
//		
//		section emfCommonLib "EMF Services library"{
//			@html'/org.pragmaticmodeling.pxdoc.emf.lib'
//			table {
//				row{
//					cell {
//						@html '<p>Module providing some very interesting basic templates to include the name and description of an element:</p>
//								
//								<ul>
//								<li>Display the icon of the element just before its name</li>
//								<li>Display element attributes in a table or in a bullet list</li>
//								<li>
//								</ul>
//								'
//						@html '<p>The EmfServices.pxdoc files contains comments to explain each template. Do not hesitate to have a look at it.</p>
//								<p>You can also refer to the EMF Example to see how these templates can be used.</p>
//				'
//					}
//					cell {
//						image "images/common-libs/emfServices.PNG"
//					}
//				}
//			}
//		}
//		
//		
//		section diagramServices "DiagramServices library"{
//			@html '<i>/org.pragmaticmodeling.pxdoc.diagrams.lib</i>'
//			table {
//				row{
//					cell {
//						@html '<p>A pxDoc Module provides the entry point to query diagrams (see the <a href="#diagramPlugin">Diagram Plugin</a> section) and some templates to include diagrams in your document:</p>
//								
//								<ul>
//								<li>Query all non empty diagrams directly owned by an element (Object or EObject)</li>
//								<li>Include each diagram with a bookmark, a title (with a dedicated style to be bound with the <i>Caption</i> style, so Word can generate a Table of Figures) and a description if any.</li>
//								</ul>
//								'
//						@html '<p>Of course, you can create in your own templates and modules to easily: apply other typical queries, include diagrams in different ways (title before, no description...).</p>
//						<p>You can find samples in the examples provided with the pxDoc enablers</p>
//				'
//					}
//					cell {
//						image "images/common-libs/DiagramsLib-overview.PNG"
//					}
//				}
//			}
//			
//		}
//
//		section imagesLibrary "Images library"{
//			@html '<i>/org.pragmaticmodeling.pxdoc.images.lib</i>'
//			table {
//				row{
//					cell {
//						@html 'Set of function allowing to:
//							<ul><li>Split large images in to several ones (and calculate automatically if split is necessary)</li>
//							<li>Crop an image, to display only a part of it</li>
//							<li>Scale an image to keep its aspect ratio</li>
//							<li>Convert a Java Image object into an AWT object (pxDoc can manipulate image files or AWT objects)</li>
//							</ul>'
//					}
//					cell {
//						image "images/common-libs/imagesLib-classDiagram.PNG"
//					}
//				}
//			}
//		}
//	}//end of common libraries
}
