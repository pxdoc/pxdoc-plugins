package org.pragmaticmodeling.pxdoc.plugins.html2pxdoc;

import org.eclipse.jface.dialogs.DialogSettings;
import org.eclipse.jface.dialogs.IDialogPage;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.IPxDocWizardPage;
import org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.wizard.IPxDocWizard;

import fr.pragmaticmodeling.pxdoc.IPxdocStylesheet;
import fr.pragmaticmodeling.pxdoc.runtime.eclipse.PxDocEclipsePlugin;
import fr.pragmaticmodeling.pxdoc.runtime.launcher.PxDocLauncher;
import fr.pragmaticmodeling.pxdoc.runtime.util.PxDocLauncherHelper;

/**
 * Class representing the first page of the wizard
 */

public class HtmlStylesPage extends WizardPage implements IPxDocWizardPage {

	public static final String SETTINGS_ID = "HtmlStylesPage_Settings";

	/**
	 * 
	 */
	public static final String SETTINGS_ID_DESTINATION = "SETTINGS_ID_DESTINATION";

	private Label normalStyleLabel = null;
	private Combo normalStyleCombo = null;
	private Label bulletStyleLabel = null;
	private Combo bulletStyleCombo = null;
	private Label numberedStyleLabel = null;
	private Combo numberedStyleCombo = null;
	private Label sectionStyleLabel = null;
	private Combo sectionStyleCombo = null;
	private Label subSectionStyleLabel = null;
	private Combo subSectionStyleCombo = null;
	private Label subSubSectionStyleLabel = null;
	private Combo subSubSectionStyleCombo = null;

	private HtmlStylesMapping stylesMapping;

	private IPxdocStylesheet stylesheet = null;

	/**
	 * Constructor for HtmlStylesPage.
	 */
	public HtmlStylesPage(IPxdocStylesheet pStylesheet, HtmlStylesMapping stylesMapping) {
		super("HTML Styles Mapping Page");
		setTitle(PxDocEclipsePlugin.getResourceString("pxdoc.wizard.destinationpage.title"));
		setDescription(PxDocEclipsePlugin.getResourceString("pxdoc.wizard.destinationpage.description"));
		this.stylesheet = pStylesheet;
		this.stylesMapping = stylesMapping;
	}

	/**
	 * Constructor for HtmlStylesPage.
	 */
	public HtmlStylesPage(IPxdocStylesheet pStylesheet) {
		this(pStylesheet, null);
	}

	/**
	 * @see IDialogPage#createControl(Composite)
	 */
	@Override
	public void createControl(Composite parent) {
		Composite composite = new Composite(parent, SWT.NULL);
		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 2;
		composite.setLayout(gridLayout);
		GridData gridData = new GridData();
		gridData.verticalAlignment = GridData.VERTICAL_ALIGN_END;
		gridData.grabExcessVerticalSpace = false;
		gridData.horizontalAlignment = GridData.FILL;
		gridData.grabExcessHorizontalSpace = true;
		gridData.horizontalSpan = 1;

		// normal
		normalStyleLabel = new Label(composite, SWT.WRAP);
		normalStyleLabel.setText("Normal Style");
		normalStyleLabel.setLayoutData(gridData);
		normalStyleCombo = new Combo(composite, SWT.DROP_DOWN | SWT.READ_ONLY);
		loadCombo(normalStyleCombo);
		normalStyleCombo.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event arg0) {
				validatePage();
			}
		});

		// bullet list
		bulletStyleLabel = new Label(composite, SWT.WRAP);
		bulletStyleLabel.setText("Bullet Style");
		bulletStyleLabel.setLayoutData(gridData);
		bulletStyleCombo = new Combo(composite, SWT.DROP_DOWN | SWT.READ_ONLY);
		loadCombo(bulletStyleCombo);
		bulletStyleCombo.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event arg0) {
				validatePage();
			}
		});

		// numbered list
		numberedStyleLabel = new Label(composite, SWT.WRAP);
		numberedStyleLabel.setText("Numbered Style");
		numberedStyleLabel.setLayoutData(gridData);
		numberedStyleCombo = new Combo(composite, SWT.DROP_DOWN | SWT.READ_ONLY);
		loadCombo(numberedStyleCombo);
		numberedStyleCombo.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event arg0) {
				validatePage();
			}
		});

		// section list
		sectionStyleLabel = new Label(composite, SWT.WRAP);
		sectionStyleLabel.setText("Header Style");
		sectionStyleLabel.setLayoutData(gridData);
		sectionStyleCombo = new Combo(composite, SWT.DROP_DOWN | SWT.READ_ONLY);
		loadCombo(sectionStyleCombo);
		sectionStyleCombo.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event arg0) {
				validatePage();
			}
		});

		// sub section list
		subSectionStyleLabel = new Label(composite, SWT.WRAP);
		subSectionStyleLabel.setText("Sub Header Style");
		subSectionStyleLabel.setLayoutData(gridData);
		subSectionStyleCombo = new Combo(composite, SWT.DROP_DOWN | SWT.READ_ONLY);
		loadCombo(subSectionStyleCombo);
		subSectionStyleCombo.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event arg0) {
				validatePage();
			}
		});

		// sub sub section list
		subSubSectionStyleLabel = new Label(composite, SWT.WRAP);
		subSubSectionStyleLabel.setText("Sub Sub Header Style");
		subSubSectionStyleLabel.setLayoutData(gridData);
		subSubSectionStyleCombo = new Combo(composite, SWT.DROP_DOWN | SWT.READ_ONLY);
		loadCombo(subSubSectionStyleCombo);
		subSubSectionStyleCombo.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event arg0) {
				validatePage();
			}
		});

		fillValues();
		// set the composite as the control for this page
		setControl(composite);
	}

	protected void validatePage() {
		if (isEmpty(getNormalStyle()) || isEmpty(getBulletedListStyle()) || isEmpty(getOrderedListStyle()) || isEmpty(getHeading1Style())
				|| isEmpty(getHeading2Style()) || isEmpty(getHeading3Style())) {
			setErrorMessage("All html styles must be binded to a stylesheet style.");
		} else {
			setErrorMessage(null);
		}

	}

	private boolean isEmpty(String text) {
		return text == null || text.trim().length() == 0;
	}

	private void fillValues() {
		if (stylesMapping != null) {
			selectStyle(normalStyleCombo, stylesMapping.getNormalStyle());
			selectStyle(bulletStyleCombo, stylesMapping.getBulletStyle());
			selectStyle(numberedStyleCombo, stylesMapping.getNumberedStyle());
			selectStyle(sectionStyleCombo, stylesMapping.getTitleStyle());
			selectStyle(subSectionStyleCombo, stylesMapping.getSubTitleStyle());
			selectStyle(subSubSectionStyleCombo, stylesMapping.getSubSubTitleStyle());
		}
	}

	private void loadCombo(Combo pCombo) {
		for (String s : stylesheet.getParagraphStyles()) {
			pCombo.add(s);
		}
	}

	@Override
	public boolean isPageComplete() {
		return getErrorMessage() == null;
	}

	/**
	 * All styles must be binded
	 * 
	 * @see IWizardPage#canFlipToNextPage()
	 */
	@Override
	public boolean canFlipToNextPage() {
		return (getNextPage() != null && isPageComplete());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.wizard.WizardPage#getDialogSettings()
	 */
	@Override
	protected IDialogSettings getDialogSettings() {
		DialogSettings settings = new DialogSettings(SETTINGS_ID);
		return settings;
	}

	protected IPxDocWizard getXdocWizard() {
		return (IPxDocWizard) getWizard();
	}

	/**
	 * Rendre ou non authorise l'element
	 * 
	 * @param enabled
	 */
	public final void setEnabled(boolean enabled) {
	}

	public String getNormalStyle() {
		return normalStyleCombo.getItem(normalStyleCombo.getSelectionIndex());
	}

	public String getBulletedListStyle() {
		return bulletStyleCombo.getItem(bulletStyleCombo.getSelectionIndex());
	}

	public String getOrderedListStyle() {
		return numberedStyleCombo.getItem(numberedStyleCombo.getSelectionIndex());
	}

	public String getHeading1Style() {
		return sectionStyleCombo.getItem(sectionStyleCombo.getSelectionIndex());
	}

	public String getHeading2Style() {
		return subSectionStyleCombo.getItem(subSectionStyleCombo.getSelectionIndex());
	}

	public String getHeading3Style() {
		return subSubSectionStyleCombo.getItem(subSubSectionStyleCombo.getSelectionIndex());
	}

	private void selectStyle(Combo pCombo, String pStyleName) {
		pCombo.setText(pStyleName);
	}

	@Override
	public IDialogSettings buildDialogSettings() {
		DialogSettings settings = new DialogSettings(getSectionId());
		settings.put("style.normal", getNormalStyle());
		settings.put("style.bulletedList", getBulletedListStyle());
		settings.put("style.orderedList", getOrderedListStyle());
		settings.put("style.heading1", getHeading1Style());
		settings.put("style.heading2", getHeading2Style());
		settings.put("style.heading3", getHeading3Style());
		return settings;
	}

	@Override
	public void loadDialogSettings(IDialogSettings pSettings) {
		if (pSettings != null) {
			selectStyle(normalStyleCombo, pSettings.get("style.normal"));
			selectStyle(bulletStyleCombo, pSettings.get("style.bulletedList"));
			selectStyle(numberedStyleCombo, pSettings.get("style.orderedList"));
			selectStyle(sectionStyleCombo, pSettings.get("style.heading1"));
			selectStyle(subSectionStyleCombo, pSettings.get("style.heading2"));
			selectStyle(subSubSectionStyleCombo, pSettings.get("style.heading3"));
		}
	}

	@Override
	public String getSectionId() {
		return "html-styles";
	}

	@Override
	public void fillLauncher(PxDocLauncher pLauncher) {
		PxDocLauncherHelper.addProperty(pLauncher, "style.normal", getNormalStyle());
		PxDocLauncherHelper.addProperty(pLauncher, "style.bulletedList", getBulletedListStyle());
		PxDocLauncherHelper.addProperty(pLauncher, "style.orderedList", getOrderedListStyle());
		PxDocLauncherHelper.addProperty(pLauncher, "style.heading1", getHeading1Style());
		PxDocLauncherHelper.addProperty(pLauncher, "style.heading2", getHeading2Style());
		PxDocLauncherHelper.addProperty(pLauncher, "style.heading3", getHeading3Style());

	}

}
