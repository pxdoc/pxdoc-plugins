package org.pragmaticmodeling.pxdoc.plugins.html2pxdoc;

import org.eclipse.emf.ecore.EObject;

import fr.pragmaticmodeling.pxdoc.Document;
import fr.pragmaticmodeling.pxdoc.Font;
import fr.pragmaticmodeling.pxdoc.HeadingN;
import fr.pragmaticmodeling.pxdoc.IPxdocStylesheet;
import fr.pragmaticmodeling.pxdoc.NumberingKind;
import fr.pragmaticmodeling.pxdoc.StyleApplication;
import fr.pragmaticmodeling.pxdoc.runtime.IPxDocGenerator;
import fr.pragmaticmodeling.pxdoc.runtime.IPxdocFactory;
import fr.pragmaticmodeling.pxdoc.runtime.PxdocFactory;
import fr.pragmaticmodeling.pxdoc.util.EcoreUtil2;

public class DefaultHtml2PxdocExtension implements IHtml2PxdocLanguageRenderer {

	private IPxdocFactory factory;
	private IPxDocGenerator generator;

	public DefaultHtml2PxdocExtension(IPxDocGenerator generator) {
		this.generator = generator;
		if (generator != null && generator.getPxdocFactory() != null) {
			this.factory = generator.getPxdocFactory();
		} else {
			this.factory = new PxdocFactory();
		}
	}

	@Override
	public String getH1Style(EObject context) {
		IPxdocStylesheet stylesheet = getStylesheet(context);
		String style = stylesheet != null ? stylesheet.getHeadingStyleId(1) :  null;
		return style != null ? style : "Normal";
	}

	int getHeadingLevelContext(EObject context) {
		StyleApplication sa = EcoreUtil2.getContainerOfType(context, StyleApplication.class);
		if (sa != null && getStylesheet(context).isTocElligibleStyle(sa.getStyle())) {
			return getStylesheet(context).getHeadingStyleLevel(sa.getStyle());
		} else {
			HeadingN headingN = EcoreUtil2.getContainerOfType(context, HeadingN.class);
			if (headingN != null) {
				return headingN.getLevel();
			}
		}
		return 0;
	}

	IPxdocStylesheet getStylesheet(EObject object) {
		Document doc = EcoreUtil2.getContainerOfType(object, Document.class);
		if (doc != null) {
			return doc.getStylesheet();
		}
		return null;
	}

	@Override
	public String getH2Style(EObject context) {
		IPxdocStylesheet stylesheet = getStylesheet(context);
		String style = stylesheet != null ? stylesheet.getHeadingStyleId(2) :  null;
		return style != null ? style : "Normal";
	}

	@Override
	public String getH3Style(EObject context) {
		IPxdocStylesheet stylesheet = getStylesheet(context);
		String style = stylesheet != null ? stylesheet.getHeadingStyleId(3) :  null;
		return style != null ? style : "Normal";
	}

	@Override
	public String getH4Style(EObject context) {
		IPxdocStylesheet stylesheet = getStylesheet(context);
		String style = stylesheet != null ? stylesheet.getHeadingStyleId(4) :  null;
		return style != null ? style : "Normal";
	}

	@Override
	public String getH5Style(EObject context) {
		IPxdocStylesheet stylesheet = getStylesheet(context);
		String style = stylesheet != null ? stylesheet.getHeadingStyleId(5) :  null;
		return style != null ? style : "Normal";
	}

	@Override
	public String getH6Style(EObject context) {
		IPxdocStylesheet stylesheet = getStylesheet(context);
		String style = stylesheet != null ? stylesheet.getHeadingStyleId(6) :  null;
		return style != null ? style : "Normal";
	}

	@Override
	public String getStyle(EObject context) {
		String style = "Normal";
		StyleApplication sa = EcoreUtil2.getContainerOfType(context, StyleApplication.class);
		if (sa != null) {
			style = sa.getStyle();
		}
		return style;
	}

	@Override
	public void decorate(Font font, String className) {

	}

	@Override
	public IPxdocFactory getPxdocFactory() {
		return factory;
	}

	@Override
	public String adaptHref(String value) {
		return value;
	}

	@Override
	public String getHrefComment(String href) {
		return null;
	}

	@Override
	public String getHrefBookmark(String href) {
		if (href != null) {
			int index = href.indexOf("#");
			if (index > -1 && index < href.length() - 1) {
				return href.substring(index + 1);
			}
		}
		return null;
	}

	@Override
	public String getNumberingStyle(EObject context, NumberingKind kind) {
		return null;
	}

	@Override
	public IPxDocGenerator getPxDocGenerator() {
		return generator;
	}

}
