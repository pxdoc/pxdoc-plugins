package org.pragmaticmodeling.pxdoc.plugins.html2pxdoc;

import org.eclipse.emf.ecore.EObject;

import fr.pragmaticmodeling.pxdoc.Font;
import fr.pragmaticmodeling.pxdoc.NumberingKind;
import fr.pragmaticmodeling.pxdoc.runtime.IPxDocRendererExtension;

public interface IHtml2PxdocLanguageRenderer extends IPxDocRendererExtension {

	String getH1Style(EObject context);
	String getH2Style(EObject context);
	String getH3Style(EObject context);
	String getH4Style(EObject context);
	String getH5Style(EObject context);
	String getH6Style(EObject context);
	String getStyle(EObject context);
	String getNumberingStyle(EObject context, NumberingKind kind);
	
	void decorate(Font font, String className);
	String adaptHref(String href);
	String getHrefComment(String href);
	String getHrefBookmark(String href);
}
