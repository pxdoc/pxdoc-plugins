package org.pragmaticmodeling.pxdoc.plugins.html2pxdoc;

public class HtmlStylesMapping {
	private String normalStyle = null;
	private String bulletStyle = null;
	private String numberedStyle = null;
	private String titleStyle = null;
	private String subTitleStyle = null;
	private String subSubTitleStyle = null;

	public HtmlStylesMapping(String normalStyle, String bulletStyle, String numberedStyle, String titleStyle, String subTitleStyle, String subSubTitleStyle) {
		super();
		this.normalStyle = normalStyle;
		this.bulletStyle = bulletStyle;
		this.numberedStyle = numberedStyle;
		this.titleStyle = titleStyle;
		this.subTitleStyle = subTitleStyle;
		this.subSubTitleStyle = subSubTitleStyle;
	}

	public String getNormalStyle() {
		return normalStyle;
	}

	public String getBulletStyle() {
		return bulletStyle;
	}

	public String getNumberedStyle() {
		return numberedStyle;
	}

	public String getTitleStyle() {
		return titleStyle;
	}

	public String getSubTitleStyle() {
		return subTitleStyle;
	}

	public String getSubSubTitleStyle() {
		return subSubTitleStyle;
	}
}
