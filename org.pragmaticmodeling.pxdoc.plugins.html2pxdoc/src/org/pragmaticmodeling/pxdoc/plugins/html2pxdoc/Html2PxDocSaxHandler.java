package org.pragmaticmodeling.pxdoc.plugins.html2pxdoc;

import java.util.LinkedList;
import java.util.List;

import javax.swing.text.AttributeSet;
import javax.swing.text.html.CSS;
import javax.swing.text.html.StyleSheet;

import org.apache.commons.lang.StringEscapeUtils;
import org.eclipse.emf.ecore.EObject;
import org.jdom2.input.sax.SAXHandler;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import fr.pragmaticmodeling.pxdoc.CellDef;
import fr.pragmaticmodeling.pxdoc.ContainerElement;
import fr.pragmaticmodeling.pxdoc.Document;
import fr.pragmaticmodeling.pxdoc.Font;
import fr.pragmaticmodeling.pxdoc.IPxdocStylesheet;
import fr.pragmaticmodeling.pxdoc.MergeKind;
import fr.pragmaticmodeling.pxdoc.NumberingKind;
import fr.pragmaticmodeling.pxdoc.Row;
import fr.pragmaticmodeling.pxdoc.Section;
import fr.pragmaticmodeling.pxdoc.StrikeType;
import fr.pragmaticmodeling.pxdoc.Table;
import fr.pragmaticmodeling.pxdoc.TemplateElement;
import fr.pragmaticmodeling.pxdoc.UnderlineType;
import fr.pragmaticmodeling.pxdoc.UnitKind;
import fr.pragmaticmodeling.pxdoc.VerticalAlignmentType;
import fr.pragmaticmodeling.pxdoc.runtime.IPxdocFactory;
import fr.pragmaticmodeling.pxdoc.util.EcoreUtil2;

public class Html2PxDocSaxHandler extends SAXHandler {

	public enum ListKind {
		ol, ul
	}

	private LinkedList<TemplateElement> pxdocObjects = null;
	private LinkedList<ListKind> nestedLists = null;
	private IPxdocFactory factory;
	private int numberingLevel = 0;
	private IHtml2PxdocLanguageRenderer helper;
	private EObject context;
	private IPxdocStylesheet stylesheet;
	private Font currentSpanFont;
	private String currentClassName;
	private boolean inScript;
	private boolean inHead;
	private boolean inSvg;

	public Html2PxDocSaxHandler(IHtml2PxdocLanguageRenderer helper) {
		this(helper, null);
	}

	public Html2PxDocSaxHandler(IHtml2PxdocLanguageRenderer helper, EObject context) {
		super();
		this.pxdocObjects = new LinkedList<TemplateElement>();
		this.nestedLists = new LinkedList<ListKind>();
		this.helper = helper;
		this.context = context;
		this.factory = helper.getPxdocFactory();
	}

	@Override
	public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException {
		super.startElement(namespaceURI, localName, qName, atts);
		if (inHead || inScript || inSvg)
			return;
		if (qName.equalsIgnoreCase("p") || qName.equalsIgnoreCase("div")) {
			if (getActiveList() == null) {
				push(factory.createStyleApplication(getParent(), false, helper.getStyle(context), null, null, null));
			}
		} else if (qName.equalsIgnoreCase("img")) {
			String lSrc = (String) atts.getValue("src");
			lSrc = lSrc.replaceAll("%20", " ");
			int width = -1;
			if (atts.getValue("width") != null) {
				try {
					width = Integer.parseInt(atts.getValue("width"));
				} catch (NumberFormatException e) {

				}
			}
			int height = -1;
			if (atts.getValue("height") != null) {
				try {
					height = Integer.parseInt(atts.getValue("height"));
				} catch (NumberFormatException e) {

				}
			}
			push(factory.createImage(getParent(), lSrc, null,
					width != -1 ? factory.createMeasure(width, UnitKind.TWIPS) : null,
					height != -1 ? factory.createMeasure(height, UnitKind.TWIPS) : null, null, null));
		} else if (qName.equalsIgnoreCase("b")) {
			push(factory.createBold(getParent()));
		} else if (qName.equalsIgnoreCase("strong")) {
			push(factory.createBold(getParent()));
		} else if (qName.equalsIgnoreCase("span")) {
			String className = null;
			String fontName = null;
			Integer fontSize = null;
			boolean isBold = false;
			boolean isItalic = false;
			boolean isUnderline = false;
			boolean isStrike = false;
			String color = "";
			String backgroundColor = "";
			int length = atts.getLength();
			// process each attribute
			for (int i = 0; i < length; i++) {
				String name = atts.getQName(i);
				String value = atts.getValue(i);
				if ("class".equals(name)) {
					className = value;
				} else if ("style".equals(name)) {
					StyleSheet styleSheet = new StyleSheet();
					AttributeSet attSet = styleSheet.getDeclaration(value);
					Object oColor = attSet.getAttribute(CSS.Attribute.COLOR);
					if (oColor != null) {
						color = colorToRgb(oColor.toString());
					}
					Object oBackgroundColor = attSet.getAttribute(CSS.Attribute.BACKGROUND_COLOR);
					if (oBackgroundColor != null) {
						backgroundColor = colorToRgb(oBackgroundColor.toString());
					}
					Object oFontName = attSet.getAttribute(CSS.Attribute.FONT_FAMILY);
					if (oFontName != null) {
						String[] fontNames = oFontName.toString().split(",");
						for (String fName : fontNames) {
							if (getStylesheet().getFonts().contains(fName.trim())) {
								fontName = fName.trim();
								break;
							}
						}
					}
					Object oFontSize = attSet.getAttribute(CSS.Attribute.FONT_SIZE);
					if (oFontSize != null) {
						String v = oFontSize.toString().trim();
						if (v.endsWith("px")) {
							v = v.substring(0, v.length() - 2);
							fontSize = Integer.parseInt(v);
						}
					}
					Object oFontStyle = attSet.getAttribute(CSS.Attribute.FONT_STYLE);
					if (oFontStyle != null) {
						if (oFontStyle.toString().indexOf("italic") > -1) {
							isItalic = true;
						}
					}
					Object oFontWeight = attSet.getAttribute(CSS.Attribute.FONT_WEIGHT);
					if (oFontWeight != null) {
						if (oFontWeight.toString().indexOf("bold") > -1) {
							isBold = true;
						}
					}
					Object oTextDecoration = attSet.getAttribute(CSS.Attribute.TEXT_DECORATION);
					if (oTextDecoration != null) {
						if (oTextDecoration.toString().indexOf("underline") > -1) {
							isUnderline = true;
						}
						if (oTextDecoration.toString().indexOf("line-through") > -1) {
							isStrike = true;
						}
					}
				}
			}
			currentSpanFont = factory.createFont(getParent(), fontName, fontSize, isBold, isItalic, isUnderline,
					UnderlineType.SINGLE, isStrike, StrikeType.SINGLE, color, backgroundColor);
			currentClassName = className;
			push(currentSpanFont);

		} else if (qName.equalsIgnoreCase("i") || qName.equalsIgnoreCase("em")) {
			push(factory.createItalic(getParent()));
		} else if (qName.equalsIgnoreCase("u")) {
			push(factory.createUnderline(getParent(), UnderlineType.SINGLE));
		} else if (qName.equalsIgnoreCase("h1")) {
			push(factory.createStyleApplication(getParent(), false, helper.getH1Style(context), null, null, null));
		} else if (qName.equalsIgnoreCase("h2")) {
			push(factory.createStyleApplication(getParent(), false, helper.getH2Style(context), null, null, null));
		} else if (qName.equalsIgnoreCase("h3")) {
			push(factory.createStyleApplication(getParent(), false, helper.getH3Style(context), null, null, null));
		} else if (qName.equalsIgnoreCase("h4")) {
			push(factory.createStyleApplication(getParent(), false, helper.getH4Style(context), null, null, null));
		} else if (qName.equalsIgnoreCase("h5")) {
			push(factory.createStyleApplication(getParent(), false, helper.getH5Style(context), null, null, null));
		} else if (qName.equalsIgnoreCase("h6")) {
			push(factory.createStyleApplication(getParent(), false, helper.getH6Style(context), null, null, null));
		} else if (qName.equalsIgnoreCase("table")) {
			push(factory.createTable(getParent(), null, null, null, null, null));
		} else if (qName.equalsIgnoreCase("tr")) {
			Table lTable = (Table) getParent();
			Row lRow = factory.createRow(lTable, null, false, null, null, true, null);
			copyCellDefs(lTable, lRow);
			push(lRow);
		} else if (qName.equalsIgnoreCase("td") || qName.equalsIgnoreCase("th")) {
			push(factory.createCell(getParent()));
		} else if (qName.equalsIgnoreCase("ol")) {
			pushList(ListKind.ol);
		} else if (qName.equalsIgnoreCase("ul")) {
			pushList(ListKind.ul);
		} else if (qName.equalsIgnoreCase("li")) {
			String customStyle = helper.getNumberingStyle(context, getNumberingKind());
			if (customStyle == null) {
				customStyle = helper.getStyle(context);
			}
			push(factory.createStyleApplication(getParent(), false, customStyle, null, null,
					factory.createNumbering(numberingLevel, getNumberingKind())));

		} else if (qName.equalsIgnoreCase("col"))

		{
			try {
				Float width = Float.parseFloat(atts.getValue("width"));
				push(factory.createCellDef(getParent(), MergeKind.NONE, factory.createMeasure(width, UnitKind.TWIPS),
						null, null, VerticalAlignmentType.TOP));
			} catch (NumberFormatException e) {

			}
		} else if (qName.equalsIgnoreCase("a")) {
			String href = helper.adaptHref(atts.getValue("href"));
			href = StringEscapeUtils.escapeXml(href);
			String comment = StringEscapeUtils.escapeXml(helper.getHrefComment(atts.getValue("href")));
			String bookmark = StringEscapeUtils.escapeXml(helper.getHrefBookmark(atts.getValue("href")));
			push(factory.createHyperlink(getParent(), bookmark, comment, href));
		} else if (qName.equalsIgnoreCase("sub")) {
			push(factory.createSubscript(getParent()));
		} else if (qName.equalsIgnoreCase("sup")) {
			push(factory.createSuperscript(getParent()));
		} else if (qName.equalsIgnoreCase("head")) {
			inHead = true;
		} else if (qName.equalsIgnoreCase("script")) {
			inScript = true;
		} else if (qName.equalsIgnoreCase("svg")) {
			inSvg = true;
		}
	}

	private NumberingKind getNumberingKind() {
		return getActiveList() == ListKind.ol ? NumberingKind.NUMERIC : NumberingKind.BULLET;
	}

	public static String colorToRgb(String colorStr) {
		if (colorStr.trim().startsWith("rgb(")) {
			return colorStr.substring(4, colorStr.lastIndexOf(")"));
		}
		String rgb = "";
		rgb += Integer.valueOf(colorStr.substring(1, 3), 16) + ",";
		rgb += Integer.valueOf(colorStr.substring(3, 5), 16) + ",";
		rgb += Integer.valueOf(colorStr.substring(5, 7), 16);
		return rgb;
	}

	private void copyCellDefs(Table pTable, Row pRow) {
		for (int i = 0; i < pTable.getCellDefs().size(); i++) {
			CellDef lTableCellDef = (CellDef) pTable.getCellDefs().get(i);
			factory.createCellDef(pRow, MergeKind.NONE, lTableCellDef.getWidth(), null, null,
					VerticalAlignmentType.TOP);
		}
	}

	@Override
	public void endElement(String namespaceURI, String localName, String qName) throws SAXException {
		super.endElement(namespaceURI, localName, qName);
		if (qName.equalsIgnoreCase("p") || qName.equalsIgnoreCase("div")) {
			if (getActiveList() == null)
				pop();
		} else if (qName.equalsIgnoreCase("b")) {
			pop();
		} else if (qName.equalsIgnoreCase("strong")) {
			pop();
		} else if (qName.equalsIgnoreCase("span")) {
			if (currentClassName != null && currentSpanFont != null) {
				helper.decorate(currentSpanFont, currentClassName);
				currentClassName = null;
				currentSpanFont = null;
			}
			pop();
		} else if (qName.equalsIgnoreCase("i") || qName.equalsIgnoreCase("em")) {
			pop();
		} else if (qName.equalsIgnoreCase("u")) {
			pop();
		} else if (qName.equalsIgnoreCase("h1")) {
			pop();
		} else if (qName.equalsIgnoreCase("h2")) {
			pop();
		} else if (qName.equalsIgnoreCase("h3")) {
			pop();
		} else if (qName.equalsIgnoreCase("h4")) {
			pop();
		} else if (qName.equalsIgnoreCase("h5")) {
			pop();
		} else if (qName.equalsIgnoreCase("h6")) {
			pop();
		} else if (qName.equalsIgnoreCase("table")) {
			pop();
		} else if (qName.equalsIgnoreCase("tr")) {
			pop();
		} else if (qName.equalsIgnoreCase("td") || qName.equalsIgnoreCase("th")) {
			//pop();// the style application
			pop();// the cell
		} else if (qName.equalsIgnoreCase("ol")) {
			popList();
		} else if (qName.equalsIgnoreCase("ul")) {
			popList();
		} else if (qName.equalsIgnoreCase("img")) {
			pop();
		} else if (qName.equalsIgnoreCase("col")) {
			pop();
		} else if (qName.equalsIgnoreCase("li")) {
			pop();
		} else if (qName.equalsIgnoreCase("a")) {
			pop();
		} else if (qName.equalsIgnoreCase("sub")) {
			pop();
		} else if (qName.equalsIgnoreCase("sup")) {
			pop();
		} else if (qName.equalsIgnoreCase("script")) {
			inScript = false;
		} else if (qName.equalsIgnoreCase("head")) {
			inHead = false;
		} else if (qName.equalsIgnoreCase("svg")) {
			inSvg = false;
		}
	}

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		super.characters(ch, start, length);
		if (inScript || inHead || inSvg)
			return;
		String lText = new String(ch, start, length);
		if (lText.length() > 0) {
			String unescapeHtml = StringEscapeUtils.unescapeHtml(lText);
			push(factory.createText(getParent(), unescapeHtml));
			pop();
		}
	}

	private void push(TemplateElement pEObject) {
		pxdocObjects.addFirst(pEObject);
	}

	private void pop() {
		pxdocObjects.removeFirst();
	}

	private void pushList(ListKind list) {
		nestedLists.addFirst(list);
		numberingLevel++;
	}

	private void popList() {
		nestedLists.removeFirst();
		numberingLevel--;
	}

	private ListKind getActiveList() {
		if (!nestedLists.isEmpty()) {
			return nestedLists.peek();
		}
		return null;
	}

	private ContainerElement getParent() {
		if (pxdocObjects.isEmpty()) {
			if (context instanceof Document) {
				Document parentDoc = (Document) context;
				List<TemplateElement> elts = parentDoc.getOwnedElements();
				if (!elts.isEmpty()) {
					TemplateElement last = elts.get(elts.size() - 1);
					if (last instanceof Section) {
						return (Section) last;
					}
				}
			}
			return (ContainerElement) context;
		}
		TemplateElement e = pxdocObjects.peek();
		if (e instanceof ContainerElement)
			return (ContainerElement) e;
		throw new RuntimeException("should not happen !!!");
	}

	IPxdocStylesheet getStylesheet() {
		if (stylesheet == null) {
			Document doc = EcoreUtil2.getContainerOfType(context, Document.class);
			if (doc != null) {
				stylesheet = doc.getStylesheet();
			}
		}
		return stylesheet;
	}
	
	public LinkedList<TemplateElement> getPxdocObjects() {
		return pxdocObjects;
	}
}
