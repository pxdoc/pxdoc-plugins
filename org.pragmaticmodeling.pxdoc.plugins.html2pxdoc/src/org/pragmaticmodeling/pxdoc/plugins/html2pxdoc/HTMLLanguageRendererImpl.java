package org.pragmaticmodeling.pxdoc.plugins.html2pxdoc;

import java.util.List;

import org.eclipse.emf.ecore.EObject;

import fr.pragmaticmodeling.pxdoc.TemplateElement;
import fr.pragmaticmodeling.pxdoc.runtime.IPxDocLanguageRenderer;
import fr.pragmaticmodeling.pxdoc.runtime.IPxDocRendererExtension;
import fr.pragmaticmodeling.pxdoc.runtime.impl.AbstractLanguageRenderer;

public class HTMLLanguageRendererImpl extends AbstractLanguageRenderer implements IPxDocLanguageRenderer {

	public HTMLLanguageRendererImpl() {
		super();
	}

	@Override
	public String getName() {
		return "html";
	}
	
	@Override
	public List<TemplateElement> render(EObject context, String text) {
		return render(context, text, new DefaultHtml2PxdocExtension(getPxDocGenerator()));
	}

	@Override
	public List<TemplateElement> render(EObject context, String text, IPxDocRendererExtension extension) {
		if (extension == null)
			extension = new DefaultHtml2PxdocExtension(getPxDocGenerator());
		return new Html2PxDocBuilder(context, extension).html2pxdoc(text);
	}

}
