package org.pragmaticmodeling.pxdoc.plugins.html2pxdoc;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.LinkedList;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.eclipse.emf.ecore.EObject;
import org.w3c.tidy.Tidy;
import org.xml.sax.InputSource;

import fr.pragmaticmodeling.pxdoc.TemplateElement;
import fr.pragmaticmodeling.pxdoc.runtime.IPxDocRendererExtension;

public class Html2PxDocBuilder {

	private EObject context;
	private IHtml2PxdocLanguageRenderer extension;

	public Html2PxDocBuilder(EObject context, IPxDocRendererExtension extension) {
		this.context = context;
		this.extension = (IHtml2PxdocLanguageRenderer) extension;
	}

	public List<TemplateElement> html2pxdoc(String pHtml) {
		LinkedList<TemplateElement> xdocResult = null;
		if (pHtml != null) {
			String lHtml = pHtml;
			lHtml.replaceAll("> <", ">___BLANK___<");
			lHtml = lHtml.replaceAll("&quot;", "\"");
			lHtml = cleanPI(lHtml);

			Tidy lTidy = new Tidy();
			lTidy.setXHTML(true);
			lTidy.setDropProprietaryAttributes(true);
			lTidy.setDropFontTags(false);
			lTidy.setHideComments(true);
			lTidy.setSpaces(0);
			lTidy.setNumEntities(true);
			lTidy.setIndentContent(false);
			lTidy.setQuoteNbsp(false);
			lTidy.setSmartIndent(false);
			lTidy.setWord2000(true);
			lTidy.setShowErrors(0);
			lTidy.setShowWarnings(false);
			lTidy.setQuiet(true);
			//lTidy.setXmlTags(true);
			lTidy.setForceOutput(true);
			StringWriter lSW = new StringWriter();
			lTidy.parse(new StringReader(lHtml), lSW);
			String lCleanHtml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"
					+ clean(lSW.toString().replaceAll("> <", ">___BLANK___<"));
			lCleanHtml = lCleanHtml.replaceAll("<!DOCTYPE((.|\n|\r)*?)\">", "");
			InputSource inputSource = new InputSource(new StringReader(lCleanHtml));
			inputSource.setEncoding("UTF-8");

			try {
				Html2PxDocSaxHandler saxHandler = null;
				saxHandler = new Html2PxDocSaxHandler(extension, context);
				SAXParserFactory factory = SAXParserFactory.newInstance();
				SAXParser parser = factory.newSAXParser();
				parser.parse(inputSource, saxHandler);
				xdocResult = saxHandler.getPxdocObjects();
			} catch (Exception ex) {

			}
		}
		return xdocResult;

	}

	private String cleanPI(String pHtml) {
		String lText = pHtml;
		int lastIndex = 0;
		int index = lText.indexOf("<?");
		String lResult = "";
		if (index == -1)
			return pHtml;
		while (index > -1) {
			lResult += lText.substring(lastIndex, index);
			lastIndex = lText.indexOf("/>", index) + 2;
			lText = lText.substring(lastIndex);
			index = lText.indexOf("<?");
			lastIndex = 0;
		}
		lResult += lText.substring(lastIndex);
		return lResult;
	}

	// FIXME remplacer tous les N espaces en 1 espace
	private String clean(String pText) {
		String lText = pText.replaceAll("\n", " ");
		lText = lText.replaceAll("\r", " ");
		lText = lText.replaceAll("\\s+", " ");
		lText = lText.replaceAll("> <", "><");
		lText = lText.replaceAll("___BLANK___", " ");
		return lText;
	}

//	public static void main(String[] args) {
//		PxdocFactory factory = new PxdocFactory();
//		DefaultHtml2PxdocExtension extension = new DefaultHtml2PxdocExtension(null);
//		StyleApplication sa = factory.createStyleApplication(null, false, "Normal", null, null, null);
//		Document doc = factory.createDocument(null, null, null, null, null, null);
//		Html2PxDocBuilder builder = new Html2PxDocBuilder(doc, extension);
////		String html = "<div style=\"background: rgb(238, 238, 238); padding: 5px 10px; border: 1px solid rgb(204, 204, 204); border-image: none;\">Special container</div>";
//		String html = "<p><em><span style=\"color: rgb(0, 128, 0);\">This is the <strong>rich text </strong></span><span style=\"color: rgb(0, 0, 255);\"> description </span>of this <span style=\"background-color: rgb(255, 255, 0);\">Manage Passenger Service Lifecycle</span></em></p>";
////		html += "<p><em><span style=\"background-color: rgb(255, 255, 0);\"><a href=\"http://www.google.com\">www.google.com</a> </span></em></p>";
////		html += "<ol><li>list 1</li><li>list 2<ol><li>list 2.1</li><li>list 2.2</li></ol></li></ol><ul><li>bullet 1</li><li>bullet 2<ul><li>bullet 3</li></ul></li></ul>";
////		html += "<p>test <sub>subscript</sub> <sup>superscript</sup></p>";
//		html += "<h2 style=\"font-style: italic;\">italic style</h2>";
//		html += "<h3 style=\"color: rgb(170, 170, 170); font-style: italic;\">&nbsp;</h3>";
//		html += "<h3 style=\"color: rgb(170, 170, 170); font-style: italic;\">subtitle</h3>";
////		html += "<div align=\"LEFT\" style=\"background: rgb(238, 238, 238); padding: 5px 10px; border: 1px solid rgb(204, 204, 204); border-image: none;\"><font size=\"1\">SPECIAL CNOTAINER</font></div>";
////		html += "<p align=\"LEFT\"><font size=\"1\"><span class=\"marker\">marker</span></font></p>";
////		html += "<p><big>big</big></p>";
////		html += "<p><big><small>small</small></big></p>";
////		html += "<p>&nbsp;</p>";
////		html += "<p><big><small><span dir=\"rtl\">language RTL</span></small></big></p>";
////		html += "<p><big><small><span dir=\"rtl\"><span dir=\"ltr\">language LTR</span></span></small></big></p>";
////		html += "<p><big><small><span dir=\"rtl\"><span dir=\"ltr\"><cite>cited work</cite></span></span></small></big></p>";
////		html += "<p><big><small><span dir=\"rtl\"><span dir=\"ltr\"><cite><q>inline quotation</q></cite></span></span></small></big></p>";
////		html += "<p>&nbsp;</p>";
//		List<TemplateElement> result = builder.html2pxdoc(html);
//		System.out.println(result);
//	}

}
