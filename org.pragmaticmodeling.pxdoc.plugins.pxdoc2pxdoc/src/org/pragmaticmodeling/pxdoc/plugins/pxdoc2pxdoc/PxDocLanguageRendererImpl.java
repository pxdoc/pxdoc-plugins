package org.pragmaticmodeling.pxdoc.plugins.pxdoc2pxdoc;

import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import fr.pragmaticmodeling.pxdoc.Bold;
import fr.pragmaticmodeling.pxdoc.Cell;
import fr.pragmaticmodeling.pxdoc.CellBorderLocation;
import fr.pragmaticmodeling.pxdoc.CellDef;
import fr.pragmaticmodeling.pxdoc.Color;
import fr.pragmaticmodeling.pxdoc.Italic;
import fr.pragmaticmodeling.pxdoc.PxDocFactory;
import fr.pragmaticmodeling.pxdoc.Row;
import fr.pragmaticmodeling.pxdoc.ShadingPattern;
import fr.pragmaticmodeling.pxdoc.StyleApplication;
import fr.pragmaticmodeling.pxdoc.Table;
import fr.pragmaticmodeling.pxdoc.TemplateElement;
import fr.pragmaticmodeling.pxdoc.Text;
import fr.pragmaticmodeling.pxdoc.runtime.IPxDocLanguageRenderer;
import fr.pragmaticmodeling.pxdoc.runtime.IPxDocRendererExtension;
import fr.pragmaticmodeling.pxdoc.runtime.impl.AbstractLanguageRenderer;
import fr.pragmaticmodeling.pxdoc.util.EcoreUtil2;

public class PxDocLanguageRendererImpl extends AbstractLanguageRenderer implements IPxDocLanguageRenderer {

	private static List<String> pxdocKeywords = null;
	private static List<String> pxdocKeywordFeatures = null;
	private static List<String> javaKeywords = null;
	private static PxDocFactory factory = PxDocFactory.eINSTANCE;
//	private IPxDocGenerator generator;


	public PxDocLanguageRendererImpl() {
		// keywords
		pxdocKeywords = new ArrayList<String>();
		pxdocKeywords.add("§");
		pxdocKeywords.add("bold");
		pxdocKeywords.add("bookmark");
		pxdocKeywords.add("cell");
		pxdocKeywords.add("color");
		pxdocKeywords.add("docObjects");
		pxdocKeywords.add("document");
		pxdocKeywords.add("field");
		pxdocKeywords.add("font");
		pxdocKeywords.add("header");
		pxdocKeywords.add("HeadingN");
		pxdocKeywords.add("hyperlink");
		pxdocKeywords.add("image");
		pxdocKeywords.add("italic");
		pxdocKeywords.add("language");
		pxdocKeywords.add("newLine");
		pxdocKeywords.add("newPage");
		pxdocKeywords.add("nextColumn");
		pxdocKeywords.add("$property");
		pxdocKeywords.add("row");
		pxdocKeywords.add("samePage");
		pxdocKeywords.add("section");
		pxdocKeywords.add("strike");
		pxdocKeywords.add("subDocument");
		pxdocKeywords.add("subscript");
		pxdocKeywords.add("superscript");
		pxdocKeywords.add("table");
		pxdocKeywords.add("toc");
		pxdocKeywords.add("underline");
		pxdocKeywords.add("pxDocGenerator");
		pxdocKeywords.add("pxDocModule");
		// properties
		pxdocKeywordFeatures = new ArrayList<String>();
		pxdocKeywordFeatures.add("align:");
		pxdocKeywordFeatures.add("attachedTemplate:");
		pxdocKeywordFeatures.add("backgroundColor:");
		pxdocKeywordFeatures.add("bookmark:");
		pxdocKeywordFeatures.add("borders");
		pxdocKeywordFeatures.add("bottomBorder");
		pxdocKeywordFeatures.add("bullet");
		pxdocKeywordFeatures.add("cantSplit");
		pxdocKeywordFeatures.add("class:");
		pxdocKeywordFeatures.add("color:");
		pxdocKeywordFeatures.add("column:");
		pxdocKeywordFeatures.add("columns:");
		pxdocKeywordFeatures.add("columnsSpace:");
		pxdocKeywordFeatures.add("comment:");
		pxdocKeywordFeatures.add("description:");
		pxdocKeywordFeatures.add("display:");
		pxdocKeywordFeatures.add("fileName:");
		pxdocKeywordFeatures.add("footerHeight:");
		pxdocKeywordFeatures.add("fromTemplate:");
		pxdocKeywordFeatures.add("headerHeight:");
		pxdocKeywordFeatures.add("height:");
		pxdocKeywordFeatures.add("hPosition");
		pxdocKeywordFeatures.add("href:");
		pxdocKeywordFeatures.add("java:");
		pxdocKeywordFeatures.add("keepWithNext");
		pxdocKeywordFeatures.add("kind:");
		pxdocKeywordFeatures.add("leftBorder");
		pxdocKeywordFeatures.add("landscape");
		pxdocKeywordFeatures.add("left:");
		pxdocKeywordFeatures.add("level:");
		pxdocKeywordFeatures.add("name:");
		pxdocKeywordFeatures.add("numbering");
		pxdocKeywordFeatures.add("merge:");
		pxdocKeywordFeatures.add("parameters:");
		pxdocKeywordFeatures.add("path:");
		pxdocKeywordFeatures.add("properties:");
		pxdocKeywordFeatures.add("relativeFrom:");
		pxdocKeywordFeatures.add("rightBorder");
		pxdocKeywordFeatures.add("shadingColor:");
		pxdocKeywordFeatures.add("shadingPattern:");
		pxdocKeywordFeatures.add("shift:");
		pxdocKeywordFeatures.add("size:");
		pxdocKeywordFeatures.add("styleBindings:");
		pxdocKeywordFeatures.add("styles:");
		pxdocKeywordFeatures.add("stylesheet:");
		pxdocKeywordFeatures.add("title:");
		pxdocKeywordFeatures.add("titleStyle:");
		pxdocKeywordFeatures.add("topBorder");
		pxdocKeywordFeatures.add("type:");
		pxdocKeywordFeatures.add("vAlign:");
		pxdocKeywordFeatures.add("variables:");
		pxdocKeywordFeatures.add("vPosition");
		pxdocKeywordFeatures.add("width:");
		pxdocKeywordFeatures.add("with:");
		// java keywords
		javaKeywords = new ArrayList<String>();
		javaKeywords.add("for");
		javaKeywords.add("if");
		javaKeywords.add("it");
		javaKeywords.add("else");
		javaKeywords.add("function");
		javaKeywords.add("apply");
		javaKeywords.add("override");
		javaKeywords.add("public");
		javaKeywords.add("private");
		javaKeywords.add("root");
		javaKeywords.add("package");
		javaKeywords.add("return");
		javaKeywords.add("separator");
		javaKeywords.add("template");
		javaKeywords.add("var");
		javaKeywords.add("val");
		javaKeywords.add("while");
	}

	private TemplateElement createComment(String textComment) {
		Color comment = factory.createColor();
		comment.setRgb("63,127,95");
		Text text = factory.createText();
		text.setContent(textComment);
		comment.getOwnedElements().add(text);
		return comment;
	}

	private TemplateElement createString(String textString) {
		Color comment = factory.createColor();
		comment.setRgb("42,0,255");
		Text text = factory.createText();
		text.setContent(textString);
		comment.getOwnedElements().add(text);
		return comment;
	}

	private TemplateElement createJavaKeyword(String word) {
		Bold keyword = factory.createBold();
		Color color = factory.createColor();
		color.setRgb("127,0,85");
		Text text = factory.createText();
		text.setContent(word);
		keyword.getOwnedElements().add(color);
		color.getOwnedElements().add(text);
		return keyword;
	}

	private TemplateElement createXdocFeatureKeyword(String word) {
		Italic keyword = factory.createItalic();
		Color color = factory.createColor();
		color.setRgb("0,164,242");
		Text text = factory.createText();
		text.setContent(word);
		keyword.getOwnedElements().add(color);
		color.getOwnedElements().add(text);
		return keyword;
	}

	private TemplateElement createXdocKeyword(String word) {
		Bold keyword = factory.createBold();
		Color color = factory.createColor();
		color.setRgb("0,128,192");
		Text text = factory.createText();
		text.setContent(word);
		keyword.getOwnedElements().add(color);
		color.getOwnedElements().add(text);
		return keyword;
	}

	private String flushCurrentText(String currentText, List<TemplateElement> result) {
		Text text = factory.createText();
		text.setContent(currentText);
		result.add(text);
		return "";
	}

	private boolean isJavaKeyword(String word) {
		return javaKeywords.contains(word);
	}

	private boolean isKeyWordAttribute(String word) {
		return pxdocKeywordFeatures.contains(word);
	}

	private boolean isKeyword(String word) {
		return pxdocKeywords.contains(word);
	}


	@Override
	public List<TemplateElement> render(EObject context, String text) {
		List<TemplateElement> codeElements = new ArrayList<TemplateElement>();
		final CharacterIterator it = new StringCharacterIterator(text);
		boolean buildingWord = false;
		String word = "";
		String currentText = "";
		boolean openingComment = false;
		String lastKeyword = null;
		boolean buildingSingleLineComment = false;
		boolean buildingMultiLineComment = false;
		boolean buildingString = false;
		boolean closingComment = false;
		boolean openingParagraphBreak = false;
		boolean openingString = false;
		String currentComment = "";
		String currentString = "";
		for (char c = it.first(); c != CharacterIterator.DONE; c = it.next()) {
			boolean keyWordAttributeFinished = c == ':' && isKeyWordAttribute(word + c);
			openingString = !buildingString && (c == '"');
			if (!buildingString && (Character.isWhitespace(c) || isParenthesis(c) || keyWordAttributeFinished)) {
				if (!openingParagraphBreak)
					openingParagraphBreak = (c == '\r');
				if (buildingWord) {
					// word finished
					if (keyWordAttributeFinished) {
						word += c;
					}
					buildingWord = false;
					if (isKeyword(word)) {
						// append text
						if (currentText.length() > 0) {
							currentText = flushCurrentText(currentText, codeElements);
						}
						currentText += c;
						if ("font".equals(lastKeyword) && isModifierKeyword(word)) {
							codeElements.add(createXdocFeatureKeyword(word));
						} else {
							lastKeyword = word;
							codeElements.add(createXdocKeyword(word));
						}
						word = "";
					} else if (isKeyWordAttribute(word)) {
						currentText = flushCurrentText(currentText, codeElements);
						if (!keyWordAttributeFinished)
							currentText += c;
						codeElements.add(createXdocFeatureKeyword(word));
						word = "";
					} else if (isJavaKeyword(word)) {
						currentText = flushCurrentText(currentText, codeElements);
						currentText += c;
						codeElements.add(createJavaKeyword(word));
						word = "";
					} else {
						currentText += word + c;
					}
				} else if (buildingMultiLineComment) {
					currentComment += c;
					closingComment = false;
					openingParagraphBreak = false;
				} else if (buildingSingleLineComment) {
					if (c == '\n' || c == '\r') {
						codeElements.add(createComment(currentComment));
						buildingSingleLineComment = false;
						currentComment = "";
						currentText += c;
					} else {
						currentComment += c;
					}
					// } else if (buildingString) {
					// currentString += c;
					// if (c == '"') {
					// createString(currentString);
					// currentString = "";
					// buildingString = false;
					// }
				} else {
					if (openingParagraphBreak && c == '\n') {
						currentText = currentText.substring(0, currentText.lastIndexOf('\r'));
						if (currentText.length() > 0) {
							currentText = flushCurrentText(currentText, codeElements);
						}
						codeElements.add(factory.createLineBreak());
						openingParagraphBreak = false;
						// } else if (buildingString) {
						// currentString += c;
					} else {
						currentText += c;
					}
				}
			} else {
				if (buildingWord) {
					word += c;
					if (c == ':' && isKeyWordAttribute(word)) {
						currentText = flushCurrentText(currentText, codeElements);
						codeElements.add(createXdocFeatureKeyword(word));
						buildingWord = false;
						word = "";
					}
				} else if (openingComment) {
					openingComment = false;
					if (c == '/') {
						currentComment += c;
						currentText = flushCurrentText(currentText, codeElements);
						buildingSingleLineComment = true;
					} else if (c == '*') {
						currentComment += c;
						currentText = flushCurrentText(currentText, codeElements);
						buildingMultiLineComment = true;
					} else {
						currentText += currentComment + c;
					}
				} else if (closingComment) {
					if (c == '/') {
						buildingMultiLineComment = false;
						currentComment += c;
						codeElements.add(createComment(currentComment));
						closingComment = false;
						currentComment = "";
					} else if (c == '*' && buildingMultiLineComment) {
						closingComment = true;
						currentComment += c;
					} else {
						openingComment = false;
						currentText += currentComment + c;
					}
				} else if (buildingMultiLineComment) {
					currentComment += c;
					if (c == '*') {
						closingComment = true;
					}
				} else if (buildingSingleLineComment) {
					currentComment += c;
				} else if (openingString) {
					currentText = flushCurrentText(currentText, codeElements);
					currentString += c;
					buildingString = true;
				} else if (buildingString) {
					currentString += c;
					if (c == '"') {
						codeElements.add(createString(currentString));
						currentString = "";
						buildingString = false;
					}
				} else {
					if (c == '/') {
						openingComment = true;
						currentComment += c;
					} else {
						word = "" + c;
						buildingWord = true;
					}
				}
			}

		}
		if (buildingSingleLineComment && currentComment.length() > 0) {
			codeElements.add(createComment(currentComment));
		}
		if (buildingWord && word.length() > 0) {
			if (isKeyword(word)) {
				codeElements.add(createXdocKeyword(word));
				currentText = "";
			} else if (isKeyWordAttribute(word)) {
				codeElements.add(createXdocFeatureKeyword(word));
				currentText = "";
			} else if (isJavaKeyword(word)) {
				codeElements.add(createJavaKeyword(word));
				currentText = "";
			} else {
				currentText += word;
			}
		}
		if (!currentText.isEmpty()) {
			flushCurrentText(currentText, codeElements);
		}
		if (EcoreUtil2.getContainerOfType(context, Table.class) == null) {
			List<TemplateElement> result = new ArrayList<TemplateElement>();
			Table table = factory.createTable();
			table.setShadingColor("237,237,237");
			table.setShadingPattern(ShadingPattern.SOLID);
			Row row = factory.createRow();
			table.getRows().add(row);
			Cell cell = factory.createCell();
			CellDef cellDef = factory.createCellDef();
			cell.setCellDef(cellDef);
			cellDef.getNoBorder().add(CellBorderLocation.BOTTOM);
			cellDef.getNoBorder().add(CellBorderLocation.LEFT);
			cellDef.getNoBorder().add(CellBorderLocation.RIGHT);
			cellDef.getNoBorder().add(CellBorderLocation.TOP);
			row.getCells().add(cell);
			//row.setShadingColor("236,236,236");
			StyleApplication sa = factory.createStyleApplication();
			sa.setStyle(getContextStyle(context));
			cell.getOwnedElements().add(sa);
			sa.getOwnedElements().addAll(codeElements);
			result.add(table);
			return result;
		} else {
			return codeElements;
		}
	}

	private boolean isModifierKeyword(String word) {
		return "#bold#italic#underline#strike#subscript#superscript#".indexOf(word)>-1;
	}

	private String getContextStyle(EObject context) {
		StyleApplication sa = EcoreUtil2.getContainerOfType(context, StyleApplication.class);
		if (sa != null) {
			return sa.getStyle();
		}
		return "Normal";
	}

	private boolean isParenthesis(char c) {
		return c == '(' || c == ')' || c == '[' || c == ']' || c == '|' || c == '{' || c == '}' || c=='<' || c== '>';
	}

	@Override
	public String getName() {
		return "pxdoc";
	}

	@Override
	public List<TemplateElement> render(EObject context, String text, IPxDocRendererExtension extension) {
		return render(context, text);
	}

}
