package org.pragmaticmodeling.pxdoc.diagrams.lib;

import fr.pragmaticmodeling.pxdoc.AlignmentType;
import fr.pragmaticmodeling.pxdoc.Bookmark;
import fr.pragmaticmodeling.pxdoc.ContainerElement;
import fr.pragmaticmodeling.pxdoc.StyleApplication;
import fr.pragmaticmodeling.pxdoc.runtime.AbstractPxDocModule;
import fr.pragmaticmodeling.pxdoc.runtime.IPxDocGenerator;
import fr.pragmaticmodeling.pxdoc.runtime.PxDocGenerationException;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.EObject;
import org.pragmaticmodeling.pxdoc.common.lib.CommonServices;
import org.pragmaticmodeling.pxdoc.common.lib.IDescriptionProvider;
import org.pragmaticmodeling.pxdoc.diagrams.Diagram;
import org.pragmaticmodeling.pxdoc.diagrams.IDiagramProvider;
import org.pragmaticmodeling.pxdoc.diagrams.IDiagramQueryBuilder;

@SuppressWarnings("all")
public class DiagramServices extends AbstractPxDocModule {
  private Logger logger = Logger.getLogger(getClass());
  
  public IDiagramProvider<? extends IDiagramQueryBuilder> diagramProvider;
  
  private List<String> _stylesToBind;
  
  public CommonServices _CommonServices = (CommonServices)getGenerator().getModule(CommonServices.class);
  
  public String getDescription(final Object element) {
    return _CommonServices.getDescription(element);
  }
  
  public IDescriptionProvider getDescriptionProvider() {
    return _CommonServices.getDescriptionProvider();
  }
  
  public boolean getSuggestImprovements() {
    return _CommonServices.getSuggestImprovements();
  }
  
  public boolean hasBookmark(final Object object) {
    return _CommonServices.hasBookmark(object);
  }
  
  public boolean isEmptyDocumentation(final String text) {
    return _CommonServices.isEmptyDocumentation(text);
  }
  
  public void setDescriptionProvider(final IDescriptionProvider descProvider) {
    _CommonServices.setDescriptionProvider(descProvider);
  }
  
  public void setSuggestImprovements(final boolean value) {
    _CommonServices.setSuggestImprovements(value);
  }
  
  public String suffixedFile(final String doc, final String suffix) {
    return _CommonServices.suffixedFile(doc, suffix);
  }
  
  public String validBookmark(final Object object) {
    return _CommonServices.validBookmark(object);
  }
  
  public DiagramServices(final IPxDocGenerator generator) {
    super(generator);
  }
  
  public IDiagramProvider<? extends IDiagramQueryBuilder> getDiagramProvider() {
    return this.diagramProvider;
  }
  
  public void setDiagramProvider(final IDiagramProvider<? extends IDiagramQueryBuilder> diagramProvider) {
    this.diagramProvider = diagramProvider;
  }
  
  /**
   * Provides the entry point to query diagrams for a sementic element.
   */
  public IDiagramQueryBuilder queryDiagrams(final Object namespace) {
    if ((this.diagramProvider != null)) {
      return this.diagramProvider.createDiagramsQuery(namespace);
    }
    return IDiagramQueryBuilder.NullDiagramQueryBuilder;
  }
  
  public void error(final String message) {
    logger.error(message);
  }
  
  public void info(final String message) {
    logger.info(message);
  }
  
  public void defaultDiagrams(final ContainerElement parent, final Object object) throws PxDocGenerationException {
    List<Diagram> diagrams = this.queryDiagrams(object).notEmpty().execute();
    boolean _isEmpty = diagrams.isEmpty();
    boolean _not = (!_isEmpty);
    if (_not) {
      Iterator<org.pragmaticmodeling.pxdoc.diagrams.Diagram> lObj0 = getIterator(diagrams);
      while (lObj0.hasNext()) {
        includeDiagram(parent, lObj0.next());
        
      }
    }
  }
  
  /**
   * Include a diagram, with bookmark, a title and the diagram description if any.
   */
  public void includeDiagram(final ContainerElement parent, final Diagram diagram) throws PxDocGenerationException {
    StyleApplication lObj0 = createStyleApplication(parent, true, getBindedStyle("BodyText"), AlignmentType.CENTER, null, null);
    String lObj1 = this.validBookmark(diagram);
    Bookmark lObj2 = createBookmark(lObj0, getBookmarkString(lObj1));
    if (((diagram.getPath() != null) && (!diagram.getPath().isEmpty()))) {
      createImage(lObj2, diagram.getPath(), null, null, null, null, null);
      createParagraphBreak(lObj2);
    }
    StyleApplication lObj3 = createStyleApplication(parent, true, getBindedStyle("Figure"), AlignmentType.CENTER, null, null);
    createText(lObj3, diagram.getName());
    _CommonServices.optionalDocumentation(parent, diagram);
  }
  
  /**
   * Includes all non empty diagrams directly owned by this element.
   */
  public void diagrams(final ContainerElement parent, final EObject pNamespace) throws PxDocGenerationException {
    List<Diagram> diagrams = this.queryDiagrams(pNamespace).notEmpty().execute();
    Iterator<org.pragmaticmodeling.pxdoc.diagrams.Diagram> lObj0 = getIterator(diagrams);
    while (lObj0.hasNext()) {
      includeDiagram(parent, lObj0.next());
      
    }
  }
}
