package org.pragmaticmodeling.pxdoc.picturetoasscii.plugins;

import ascii.ASCII;
import fr.pragmaticmodeling.pxdoc.ContainerElement;
import fr.pragmaticmodeling.pxdoc.runtime.AbstractPxDocModule;
import fr.pragmaticmodeling.pxdoc.runtime.IPxDocGenerator;
import fr.pragmaticmodeling.pxdoc.runtime.PxDocGenerationException;
import java.awt.image.BufferedImage;
import java.net.URL;
import java.util.List;
import javax.imageio.ImageIO;
import org.apache.log4j.Logger;
import org.eclipse.xtext.xbase.lib.Exceptions;

@SuppressWarnings("all")
public class PictureToAscii extends AbstractPxDocModule {
  private Logger logger = Logger.getLogger(getClass());
  
  private List<String> _stylesToBind;
  
  public PictureToAscii(final IPxDocGenerator generator) {
    super(generator);
  }
  
  public BufferedImage loadImage(final String httpPath) {
    try {
      URL _uRL = new URL(httpPath);
      return ImageIO.read(_uRL);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  public void error(final String message) {
    logger.error(message);
  }
  
  public void info(final String message) {
    logger.info(message);
  }
  
  public void pictureToAscii(final ContainerElement parent, final String path) throws PxDocGenerationException {
    ASCII converter = new ASCII();
    addAllToParent(converter.convertToPxDoc2(this.loadImage(path), "Normal"), parent);
  }
  
  public void pictureToAscii(final ContainerElement parent, final BufferedImage img) throws PxDocGenerationException {
    ASCII converter = new ASCII();
    addAllToParent(converter.convertToPxDoc2(img, "Normal"), parent);
  }
}
