package org.pragmaticmodeling.pxdoc.diagrams;

import java.util.List;

public interface IDiagramsQueryRunner {

	/**
	 * Specifies how to run the query
	 * @param query
	 * @return
	 */
	List<Diagram> execute(IDiagramQueryBuilder query);
}
