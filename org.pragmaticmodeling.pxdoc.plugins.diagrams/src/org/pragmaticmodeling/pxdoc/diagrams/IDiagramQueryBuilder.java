package org.pragmaticmodeling.pxdoc.diagrams;

import java.util.Collections;
import java.util.List;

public interface IDiagramQueryBuilder {
	
	/**
	 * Execute the query
	 * @return The list of matching Diagrams
	 */
	List<Diagram> execute();
	
	/**
	 * Query by Diagram name
	 * @param name
	 * 
	 */
	IDiagramQueryBuilder name(String name);
	
	/**
	 * Query by type of Diagram
	 * @param type
	 * @return
	 */
	IDiagramQueryBuilder type(String type);
	
	/**
	 * Query diagrams with name starting with given String
	 * @param nameStartsWith
	 * @return
	 */
	IDiagramQueryBuilder nameStartsWith(String nameStartsWith);
	
	/**
	 * Set if elements (object types,...) that have been tagged as 'To be ignored' shall be included or not
	 * @return
	 */
	IDiagramQueryBuilder ignoreCase();
	
	/**
	 * Set if diagrams nested in other diagrams shall be included or not
	 * @return
	 */
	IDiagramQueryBuilder searchNested();
	
	/**
	 * Set if empty diagrams shall be included or not
	 * @return
	 */
	IDiagramQueryBuilder notEmpty();
	
	/**
	 * Get the query object
	 * @return
	 */
	IDiagramQuery getQuery();

	// NULL implementation of this interface, to avoid getting Null as result 

	public static final IDiagramQueryBuilder NullDiagramQueryBuilder = new IDiagramQueryBuilder() {
		
	
		@Override
		public List<Diagram> execute() {
			return Collections.emptyList();
		}

		@Override
		public IDiagramQueryBuilder name(String name) {
			return this;
		}

		@Override
		public IDiagramQueryBuilder type(String type) {
			return this;
		}

		@Override
		public IDiagramQueryBuilder nameStartsWith(String nameStartsWith) {
			return this;
		}

		@Override
		public IDiagramQueryBuilder ignoreCase() {
			return this;
		}

		@Override
		public IDiagramQueryBuilder searchNested() {
			return this;
		}

		@Override
		public IDiagramQueryBuilder notEmpty() {
			return this;
		}

		@Override
		public IDiagramQuery getQuery() {
			return null;
		}
		
	};

}
