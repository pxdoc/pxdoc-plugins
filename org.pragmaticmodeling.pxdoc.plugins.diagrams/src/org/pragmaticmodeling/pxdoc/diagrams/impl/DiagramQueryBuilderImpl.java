package org.pragmaticmodeling.pxdoc.diagrams.impl;

import java.util.List;

import org.pragmaticmodeling.pxdoc.diagrams.Diagram;
import org.pragmaticmodeling.pxdoc.diagrams.IDiagramQuery;
import org.pragmaticmodeling.pxdoc.diagrams.IDiagramQueryBuilder;
import org.pragmaticmodeling.pxdoc.diagrams.IDiagramsQueryRunner;

public class DiagramQueryBuilderImpl implements IDiagramQueryBuilder {

	private IDiagramQuery query;
	
	private IDiagramsQueryRunner runner;
	
	public DiagramQueryBuilderImpl(Object namespace, IDiagramsQueryRunner runner) {
		super();
		this.query = createQuery();
		this.query.setNamespace(namespace);
		this.runner = runner;
		
	}

	protected IDiagramQuery createQuery() {
		return new DiagramQueryImpl();
	}

	@Override
	public List<Diagram> execute() {
		return runner.execute(this);
	}

	@Override
	public IDiagramQueryBuilder name(String name) {
		this.query.setName(name);
		return this;
	}

	@Override
	public IDiagramQueryBuilder type(String type) {
		this.query.setType(type);
		return this;
	}

	@Override
	public IDiagramQueryBuilder nameStartsWith(String nameStartsWith) {
		this.query.setNameStartsWith(nameStartsWith);
		return this;
	}

	@Override
	public IDiagramQueryBuilder ignoreCase() {
		this.query.setIgnoreCase(true);
		return this;
	}

	@Override
	public IDiagramQueryBuilder searchNested() {
		this.query.setSearchNested(true);
		return this;
	}

	@Override
	public IDiagramQueryBuilder notEmpty() {
		this.query.setNotEmpty(true);
		return this;
	}

	@Override
	public IDiagramQuery getQuery() {
		return query;
	}
	
}
