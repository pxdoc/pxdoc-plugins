package org.pragmaticmodeling.pxdoc.diagrams.impl;

import org.pragmaticmodeling.pxdoc.diagrams.IDiagramQuery;

public class DiagramQueryImpl implements IDiagramQuery {

	private String name;
	private String nameStartsWith;
	private Object namespace;
	private boolean searchNested;
	private boolean notEmpty;
	private String type;
	private boolean ignoreCase;
	
	@Override
	public String getName() {
		return name;
	}
	@Override
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String getNameStartsWith() {
		return nameStartsWith;
	}
	@Override
	public void setNameStartsWith(String nameStartsWith) {
		this.nameStartsWith = nameStartsWith;
	}
	@Override
	public Object getNamespace() {
		return namespace;
	}
	@Override
	public void setNamespace(Object namespace) {
		this.namespace = namespace;
	}
	@Override
	public boolean searchNested() {
		return searchNested;
	}
	@Override
	public void setSearchNested(boolean searchNested) {
		this.searchNested = searchNested;
	}
	@Override
	public boolean notEmpty() {
		return notEmpty;
	}
	@Override
	public void setNotEmpty(boolean notEmpty) {
		this.notEmpty = notEmpty;
	}
	@Override
	public String getType() {
		return type;
	}
	@Override
	public void setType(String type) {
		this.type = type;
	}
	
	@Override
	public boolean ignoreCase() {
		return ignoreCase;
	}
	
	@Override
	public void setIgnoreCase(boolean ignoreCase) {
		this.ignoreCase = ignoreCase;
	}

}
