package org.pragmaticmodeling.pxdoc.diagrams;

public interface IDiagramQuery {

	/**
	 * Get the object on which the diagram query will be launched
	 * @return
	 */
	Object getNamespace();
	
	/**
	 * Set the object on which the diagram query will be launched
	 * @return
	 */
	void setNamespace(Object namespace);
	
	/**
	 * Get the name for the query by name
	 * @return
	 */
	String getName();
	
	/**
	 * Set the name for the query by name
	 * @param name
	 */
	void setName(String name);
	
	/**
	 * Get the string for the query by "name starting with"
	 * @return
	 */
	String getNameStartsWith();
	
	/**
	 * Get the string for the query by "name starting with"
	 * @return
	 */
	void setNameStartsWith(String nameStartsWith);
	
	/**
	 * Get the the type for the query by type
	 * @return
	 */
	String getType();
	
	/**
	 * Set the type for the query by type
	 * @param type
	 */
	void setType(String type);
	
	/**
	 * Get the boolean for the "Search nested diagrams" query
	 * @return
	 */
	boolean searchNested();
	/**
	 * Set the boolean for the "Search nested diagrams" query
	 * @param searchNested
	 */
	void setSearchNested(boolean searchNested);
	
	/**
	 * Get the boolean for "notEmpty" query
	 * @param searchNested
	 */
	boolean notEmpty();
	
	/**
	 * Set the boolean for "notEmpty" query
	 * @param notEmpty
	 */
	void setNotEmpty(boolean notEmpty);
	/**
	 * Get the boolean for "ignoreCase" query
	 */
	boolean ignoreCase();
	
	/**
	 * Set the boolean for "ignoreCase" query
	 * @param ignoreCase
	 */
	void setIgnoreCase(boolean ignoreCase);
}
