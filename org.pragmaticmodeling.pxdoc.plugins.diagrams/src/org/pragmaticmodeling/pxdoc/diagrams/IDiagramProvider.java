package org.pragmaticmodeling.pxdoc.diagrams;

/**
 * Provides a way to query diagrams for an object.
 */
public interface IDiagramProvider <T extends IDiagramQueryBuilder> {

	/**
	 * Create the diagrams query 
	 * @param namespace
	 * @return
	 * @Deprecated use {@link #createQuery(Object)}
	 */
	T createDiagramsQuery(final Object namespace);
	
	/**
	 * Create the diagrams query 
	 * @since 1.0.2
	 * @param namespace
	 * @return
	 */
	T createQuery(final Object namespace);
}
