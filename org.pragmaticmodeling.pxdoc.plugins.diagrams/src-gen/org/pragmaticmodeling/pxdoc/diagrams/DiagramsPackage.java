/**
 */
package org.pragmaticmodeling.pxdoc.diagrams;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.pragmaticmodeling.pxdoc.diagrams.DiagramsFactory
 * @model kind="package"
 * @generated
 */
public interface DiagramsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "diagrams";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.pragmaticmodeling.org/pxdoc/diagrams/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "diagrams";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DiagramsPackage eINSTANCE = org.pragmaticmodeling.pxdoc.diagrams.impl.DiagramsPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.pragmaticmodeling.pxdoc.diagrams.impl.DiagramImpl <em>Diagram</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.pragmaticmodeling.pxdoc.diagrams.impl.DiagramImpl
	 * @see org.pragmaticmodeling.pxdoc.diagrams.impl.DiagramsPackageImpl#getDiagram()
	 * @generated
	 */
	int DIAGRAM = 0;

	/**
	 * The feature id for the '<em><b>EAnnotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__EANNOTATIONS = EcorePackage.EMODEL_ELEMENT__EANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__ID = EcorePackage.EMODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__NAME = EcorePackage.EMODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__PATH = EcorePackage.EMODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Documentation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__DOCUMENTATION = EcorePackage.EMODEL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__KIND = EcorePackage.EMODEL_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__WIDTH = EcorePackage.EMODEL_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Height</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__HEIGHT = EcorePackage.EMODEL_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Diagram Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__DIAGRAM_OBJECT = EcorePackage.EMODEL_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>Diagram</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM_FEATURE_COUNT = EcorePackage.EMODEL_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The meta object id for the '{@link org.pragmaticmodeling.pxdoc.diagrams.impl.DiagramsImpl <em>Diagrams</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.pragmaticmodeling.pxdoc.diagrams.impl.DiagramsImpl
	 * @see org.pragmaticmodeling.pxdoc.diagrams.impl.DiagramsPackageImpl#getDiagrams()
	 * @generated
	 */
	int DIAGRAMS = 1;

	/**
	 * The feature id for the '<em><b>Diagrams</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAMS__DIAGRAMS = 0;

	/**
	 * The feature id for the '<em><b>Root Directory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAMS__ROOT_DIRECTORY = 1;

	/**
	 * The feature id for the '<em><b>Model ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAMS__MODEL_ID = 2;

	/**
	 * The number of structural features of the '<em>Diagrams</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAMS_FEATURE_COUNT = 3;


	/**
	 * Returns the meta object for class '{@link org.pragmaticmodeling.pxdoc.diagrams.Diagram <em>Diagram</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Diagram</em>'.
	 * @see org.pragmaticmodeling.pxdoc.diagrams.Diagram
	 * @generated
	 */
	EClass getDiagram();

	/**
	 * Returns the meta object for the attribute '{@link org.pragmaticmodeling.pxdoc.diagrams.Diagram#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see org.pragmaticmodeling.pxdoc.diagrams.Diagram#getId()
	 * @see #getDiagram()
	 * @generated
	 */
	EAttribute getDiagram_Id();

	/**
	 * Returns the meta object for the attribute '{@link org.pragmaticmodeling.pxdoc.diagrams.Diagram#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.pragmaticmodeling.pxdoc.diagrams.Diagram#getName()
	 * @see #getDiagram()
	 * @generated
	 */
	EAttribute getDiagram_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.pragmaticmodeling.pxdoc.diagrams.Diagram#getPath <em>Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Path</em>'.
	 * @see org.pragmaticmodeling.pxdoc.diagrams.Diagram#getPath()
	 * @see #getDiagram()
	 * @generated
	 */
	EAttribute getDiagram_Path();

	/**
	 * Returns the meta object for the attribute '{@link org.pragmaticmodeling.pxdoc.diagrams.Diagram#getDocumentation <em>Documentation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Documentation</em>'.
	 * @see org.pragmaticmodeling.pxdoc.diagrams.Diagram#getDocumentation()
	 * @see #getDiagram()
	 * @generated
	 */
	EAttribute getDiagram_Documentation();

	/**
	 * Returns the meta object for the attribute '{@link org.pragmaticmodeling.pxdoc.diagrams.Diagram#getKind <em>Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Kind</em>'.
	 * @see org.pragmaticmodeling.pxdoc.diagrams.Diagram#getKind()
	 * @see #getDiagram()
	 * @generated
	 */
	EAttribute getDiagram_Kind();

	/**
	 * Returns the meta object for the attribute '{@link org.pragmaticmodeling.pxdoc.diagrams.Diagram#getWidth <em>Width</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Width</em>'.
	 * @see org.pragmaticmodeling.pxdoc.diagrams.Diagram#getWidth()
	 * @see #getDiagram()
	 * @generated
	 */
	EAttribute getDiagram_Width();

	/**
	 * Returns the meta object for the attribute '{@link org.pragmaticmodeling.pxdoc.diagrams.Diagram#getHeight <em>Height</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Height</em>'.
	 * @see org.pragmaticmodeling.pxdoc.diagrams.Diagram#getHeight()
	 * @see #getDiagram()
	 * @generated
	 */
	EAttribute getDiagram_Height();

	/**
	 * Returns the meta object for the attribute '{@link org.pragmaticmodeling.pxdoc.diagrams.Diagram#getDiagramObject <em>Diagram Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Diagram Object</em>'.
	 * @see org.pragmaticmodeling.pxdoc.diagrams.Diagram#getDiagramObject()
	 * @see #getDiagram()
	 * @generated
	 */
	EAttribute getDiagram_DiagramObject();

	/**
	 * Returns the meta object for class '{@link org.pragmaticmodeling.pxdoc.diagrams.Diagrams <em>Diagrams</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Diagrams</em>'.
	 * @see org.pragmaticmodeling.pxdoc.diagrams.Diagrams
	 * @generated
	 */
	EClass getDiagrams();

	/**
	 * Returns the meta object for the containment reference list '{@link org.pragmaticmodeling.pxdoc.diagrams.Diagrams#getDiagrams <em>Diagrams</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Diagrams</em>'.
	 * @see org.pragmaticmodeling.pxdoc.diagrams.Diagrams#getDiagrams()
	 * @see #getDiagrams()
	 * @generated
	 */
	EReference getDiagrams_Diagrams();

	/**
	 * Returns the meta object for the attribute '{@link org.pragmaticmodeling.pxdoc.diagrams.Diagrams#getRootDirectory <em>Root Directory</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Root Directory</em>'.
	 * @see org.pragmaticmodeling.pxdoc.diagrams.Diagrams#getRootDirectory()
	 * @see #getDiagrams()
	 * @generated
	 */
	EAttribute getDiagrams_RootDirectory();

	/**
	 * Returns the meta object for the attribute '{@link org.pragmaticmodeling.pxdoc.diagrams.Diagrams#getModelID <em>Model ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Model ID</em>'.
	 * @see org.pragmaticmodeling.pxdoc.diagrams.Diagrams#getModelID()
	 * @see #getDiagrams()
	 * @generated
	 */
	EAttribute getDiagrams_ModelID();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	DiagramsFactory getDiagramsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.pragmaticmodeling.pxdoc.diagrams.impl.DiagramImpl <em>Diagram</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.pragmaticmodeling.pxdoc.diagrams.impl.DiagramImpl
		 * @see org.pragmaticmodeling.pxdoc.diagrams.impl.DiagramsPackageImpl#getDiagram()
		 * @generated
		 */
		EClass DIAGRAM = eINSTANCE.getDiagram();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DIAGRAM__ID = eINSTANCE.getDiagram_Id();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DIAGRAM__NAME = eINSTANCE.getDiagram_Name();

		/**
		 * The meta object literal for the '<em><b>Path</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DIAGRAM__PATH = eINSTANCE.getDiagram_Path();

		/**
		 * The meta object literal for the '<em><b>Documentation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DIAGRAM__DOCUMENTATION = eINSTANCE.getDiagram_Documentation();

		/**
		 * The meta object literal for the '<em><b>Kind</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DIAGRAM__KIND = eINSTANCE.getDiagram_Kind();

		/**
		 * The meta object literal for the '<em><b>Width</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DIAGRAM__WIDTH = eINSTANCE.getDiagram_Width();

		/**
		 * The meta object literal for the '<em><b>Height</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DIAGRAM__HEIGHT = eINSTANCE.getDiagram_Height();

		/**
		 * The meta object literal for the '<em><b>Diagram Object</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DIAGRAM__DIAGRAM_OBJECT = eINSTANCE.getDiagram_DiagramObject();

		/**
		 * The meta object literal for the '{@link org.pragmaticmodeling.pxdoc.diagrams.impl.DiagramsImpl <em>Diagrams</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.pragmaticmodeling.pxdoc.diagrams.impl.DiagramsImpl
		 * @see org.pragmaticmodeling.pxdoc.diagrams.impl.DiagramsPackageImpl#getDiagrams()
		 * @generated
		 */
		EClass DIAGRAMS = eINSTANCE.getDiagrams();

		/**
		 * The meta object literal for the '<em><b>Diagrams</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DIAGRAMS__DIAGRAMS = eINSTANCE.getDiagrams_Diagrams();

		/**
		 * The meta object literal for the '<em><b>Root Directory</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DIAGRAMS__ROOT_DIRECTORY = eINSTANCE.getDiagrams_RootDirectory();

		/**
		 * The meta object literal for the '<em><b>Model ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DIAGRAMS__MODEL_ID = eINSTANCE.getDiagrams_ModelID();

	}

} //DiagramsPackage
