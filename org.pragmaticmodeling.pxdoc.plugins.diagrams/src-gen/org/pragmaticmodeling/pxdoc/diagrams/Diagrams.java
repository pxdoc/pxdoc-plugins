/**
 */
package org.pragmaticmodeling.pxdoc.diagrams;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Diagrams</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.pragmaticmodeling.pxdoc.diagrams.Diagrams#getDiagrams <em>Diagrams</em>}</li>
 *   <li>{@link org.pragmaticmodeling.pxdoc.diagrams.Diagrams#getRootDirectory <em>Root Directory</em>}</li>
 *   <li>{@link org.pragmaticmodeling.pxdoc.diagrams.Diagrams#getModelID <em>Model ID</em>}</li>
 * </ul>
 *
 * @see org.pragmaticmodeling.pxdoc.diagrams.DiagramsPackage#getDiagrams()
 * @model
 * @generated
 */
public interface Diagrams extends EObject {
	/**
	 * Returns the value of the '<em><b>Diagrams</b></em>' containment reference list.
	 * The list contents are of type {@link org.pragmaticmodeling.pxdoc.diagrams.Diagram}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Diagrams</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Diagrams</em>' containment reference list.
	 * @see org.pragmaticmodeling.pxdoc.diagrams.DiagramsPackage#getDiagrams_Diagrams()
	 * @model containment="true"
	 * @generated
	 */
	EList<Diagram> getDiagrams();

	/**
	 * Returns the value of the '<em><b>Root Directory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Root Directory</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Root Directory</em>' attribute.
	 * @see #setRootDirectory(String)
	 * @see org.pragmaticmodeling.pxdoc.diagrams.DiagramsPackage#getDiagrams_RootDirectory()
	 * @model
	 * @generated
	 */
	String getRootDirectory();

	/**
	 * Sets the value of the '{@link org.pragmaticmodeling.pxdoc.diagrams.Diagrams#getRootDirectory <em>Root Directory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Root Directory</em>' attribute.
	 * @see #getRootDirectory()
	 * @generated
	 */
	void setRootDirectory(String value);

	/**
	 * Returns the value of the '<em><b>Model ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model ID</em>' attribute.
	 * @see #setModelID(String)
	 * @see org.pragmaticmodeling.pxdoc.diagrams.DiagramsPackage#getDiagrams_ModelID()
	 * @model
	 * @generated
	 */
	String getModelID();

	/**
	 * Sets the value of the '{@link org.pragmaticmodeling.pxdoc.diagrams.Diagrams#getModelID <em>Model ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Model ID</em>' attribute.
	 * @see #getModelID()
	 * @generated
	 */
	void setModelID(String value);

} // Diagrams
