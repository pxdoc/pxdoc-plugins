/**
 */
package org.pragmaticmodeling.pxdoc.diagrams.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.pragmaticmodeling.pxdoc.diagrams.Diagram;
import org.pragmaticmodeling.pxdoc.diagrams.Diagrams;
import org.pragmaticmodeling.pxdoc.diagrams.DiagramsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Diagrams</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.pragmaticmodeling.pxdoc.diagrams.impl.DiagramsImpl#getDiagrams <em>Diagrams</em>}</li>
 *   <li>{@link org.pragmaticmodeling.pxdoc.diagrams.impl.DiagramsImpl#getRootDirectory <em>Root Directory</em>}</li>
 *   <li>{@link org.pragmaticmodeling.pxdoc.diagrams.impl.DiagramsImpl#getModelID <em>Model ID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DiagramsImpl extends EObjectImpl implements Diagrams {
	/**
	 * The cached value of the '{@link #getDiagrams() <em>Diagrams</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDiagrams()
	 * @generated
	 * @ordered
	 */
	protected EList<Diagram> diagrams;

	/**
	 * The default value of the '{@link #getRootDirectory() <em>Root Directory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRootDirectory()
	 * @generated
	 * @ordered
	 */
	protected static final String ROOT_DIRECTORY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRootDirectory() <em>Root Directory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRootDirectory()
	 * @generated
	 * @ordered
	 */
	protected String rootDirectory = ROOT_DIRECTORY_EDEFAULT;

	/**
	 * The default value of the '{@link #getModelID() <em>Model ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModelID()
	 * @generated
	 * @ordered
	 */
	protected static final String MODEL_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getModelID() <em>Model ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModelID()
	 * @generated
	 * @ordered
	 */
	protected String modelID = MODEL_ID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DiagramsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DiagramsPackage.Literals.DIAGRAMS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Diagram> getDiagrams() {
		if (diagrams == null) {
			diagrams = new EObjectContainmentEList<Diagram>(Diagram.class, this, DiagramsPackage.DIAGRAMS__DIAGRAMS);
		}
		return diagrams;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getRootDirectory() {
		return rootDirectory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setRootDirectory(String newRootDirectory) {
		String oldRootDirectory = rootDirectory;
		rootDirectory = newRootDirectory;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DiagramsPackage.DIAGRAMS__ROOT_DIRECTORY, oldRootDirectory, rootDirectory));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getModelID() {
		return modelID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setModelID(String newModelID) {
		String oldModelID = modelID;
		modelID = newModelID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DiagramsPackage.DIAGRAMS__MODEL_ID, oldModelID, modelID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DiagramsPackage.DIAGRAMS__DIAGRAMS:
				return ((InternalEList<?>)getDiagrams()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DiagramsPackage.DIAGRAMS__DIAGRAMS:
				return getDiagrams();
			case DiagramsPackage.DIAGRAMS__ROOT_DIRECTORY:
				return getRootDirectory();
			case DiagramsPackage.DIAGRAMS__MODEL_ID:
				return getModelID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DiagramsPackage.DIAGRAMS__DIAGRAMS:
				getDiagrams().clear();
				getDiagrams().addAll((Collection<? extends Diagram>)newValue);
				return;
			case DiagramsPackage.DIAGRAMS__ROOT_DIRECTORY:
				setRootDirectory((String)newValue);
				return;
			case DiagramsPackage.DIAGRAMS__MODEL_ID:
				setModelID((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DiagramsPackage.DIAGRAMS__DIAGRAMS:
				getDiagrams().clear();
				return;
			case DiagramsPackage.DIAGRAMS__ROOT_DIRECTORY:
				setRootDirectory(ROOT_DIRECTORY_EDEFAULT);
				return;
			case DiagramsPackage.DIAGRAMS__MODEL_ID:
				setModelID(MODEL_ID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DiagramsPackage.DIAGRAMS__DIAGRAMS:
				return diagrams != null && !diagrams.isEmpty();
			case DiagramsPackage.DIAGRAMS__ROOT_DIRECTORY:
				return ROOT_DIRECTORY_EDEFAULT == null ? rootDirectory != null : !ROOT_DIRECTORY_EDEFAULT.equals(rootDirectory);
			case DiagramsPackage.DIAGRAMS__MODEL_ID:
				return MODEL_ID_EDEFAULT == null ? modelID != null : !MODEL_ID_EDEFAULT.equals(modelID);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (rootDirectory: ");
		result.append(rootDirectory);
		result.append(", modelID: ");
		result.append(modelID);
		result.append(')');
		return result.toString();
	}

} //DiagramsImpl
