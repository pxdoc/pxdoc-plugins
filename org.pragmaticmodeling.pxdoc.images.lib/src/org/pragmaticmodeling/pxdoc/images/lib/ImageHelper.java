package org.pragmaticmodeling.pxdoc.images.lib;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.DirectColorModel;
import java.awt.image.IndexColorModel;
import java.awt.image.WritableRaster;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.PaletteData;
import org.eclipse.swt.graphics.RGB;

public class ImageHelper {

	private static final Map<Image, BufferedImage> map = new HashMap<Image, BufferedImage>();
	
	// private static final float bestRatio = 1.2f;
	/** Used to accurately resize or split an image
	 * 
	 */
	private static final float maxRatio = 1.4f;

	/** Convert an Image to a BufferedImage
	 * 
	 * @param image
	 * @return
	 */
	public static BufferedImage convertToAWT(Image image) {
		if (map.containsKey(image)) {
			return map.get(image);
		}
		ImageData data = image.getImageData();
		ColorModel colorModel = null;
		PaletteData palette = data.palette;
		if (palette.isDirect) {
			colorModel = new DirectColorModel(data.depth, palette.redMask, palette.greenMask, palette.blueMask);
			BufferedImage bufferedImage = new BufferedImage(colorModel,
					colorModel.createCompatibleWritableRaster(data.width, data.height), false, null);
			RGB whiteRgb = new RGB(255, 255, 255);
			for (int y = 0; y < data.height; y++) {
				for (int x = 0; x < data.width; x++) {
					int pixel = data.getPixel(x, y);
					RGB rgb = (pixel != 0) ? palette.getRGB(pixel) : whiteRgb;
					bufferedImage.setRGB(x, y, rgb.red << 16 | rgb.green << 8 | rgb.blue);
				}
			}
			return bufferedImage;
		} else {
			RGB[] rgbs = palette.getRGBs();
			byte[] red = new byte[rgbs.length];
			byte[] green = new byte[rgbs.length];
			byte[] blue = new byte[rgbs.length];
			for (int i = 0; i < rgbs.length; i++) {
				RGB rgb = rgbs[i];
				red[i] = (byte) rgb.red;
				green[i] = (byte) rgb.green;
				blue[i] = (byte) rgb.blue;
			}
			if (data.transparentPixel != -1) {
				colorModel = new IndexColorModel(data.depth, rgbs.length, red, green, blue, data.transparentPixel);
			} else {
				colorModel = new IndexColorModel(data.depth, rgbs.length, red, green, blue);
			}
			BufferedImage bufferedImage = new BufferedImage(colorModel,
					colorModel.createCompatibleWritableRaster(data.width, data.height), false, null);
			WritableRaster raster = bufferedImage.getRaster();
			int[] pixelArray = new int[1];
			for (int y = 0; y < data.height; y++) {
				for (int x = 0; x < data.width; x++) {
					int pixel = data.getPixel(x, y);
					pixelArray[0] = pixel;
					raster.setPixel(x, y, pixelArray);
				}
			}
			map.put(image, bufferedImage);
			return bufferedImage;
		}
	}

	/** Useful to automatically split a large image on several pages
	 * 
	 * @param image
	 * @param availableWidth
	 * @param availableHeight
	 * @return
	 */
	public static List<BufferedImage> splitImage(BufferedImage image, int availableWidth, int availableHeight) {
		List<BufferedImage> splited = new ArrayList<BufferedImage>();
		if (image == null) {
			return splited;
		}
		if (!requiresSplitting(image, availableHeight)) {
			splited.add(image);
			return splited;
		}

		// first resize the image to fit the available width
		float ratio = ((float) availableWidth) / ((float) image.getWidth());
		int maxChunkHeight = (int) (((float) availableHeight) / ratio);
		// int totalHeight = (int)(image.getHeight() * ratio);

		float nbImages = ((float) image.getHeight()) / ((float) maxChunkHeight);
		float lastImageSizeInPagePercent = nbImages - ((int) nbImages);
		int nbRows = lastImageSizeInPagePercent < 0.2f ? ((int) nbImages) : ((int) nbImages) + 1;

		int chunkWidth = image.getWidth(); // determines the chunk width and
											// height
		int count = 0;
		BufferedImage imgs[] = new BufferedImage[nbRows]; // Image array to hold
															// image chunks
		for (int x = 0; x < nbRows; x++) {
			int chunkHeight = x == nbRows - 1 ? image.getHeight() - ((nbRows - 1) * maxChunkHeight) : maxChunkHeight;
			int heightToCopy = x == nbRows - 1 ? image.getHeight() : chunkHeight * x + chunkHeight;
			// Initialize the image array with image chunks
			imgs[count] = new BufferedImage(chunkWidth, chunkHeight, BufferedImage.TYPE_INT_ARGB);
			// draws the image chunk
			Graphics2D gr = imgs[count++].createGraphics();
			gr.drawImage(image, 0, 0, chunkWidth, chunkHeight, 0, maxChunkHeight * x, chunkWidth, heightToCopy, null);
			gr.dispose();
			splited.add(imgs[x]);
		}
		return splited;
	}

	/**Ensures to keep the aspect ratio of the image
	 *
	 * @param image
	 *            The image to be scaled
	 * @param imageType
	 *            Target image type, e.g. TYPE_INT_RGB
	 * @param newWidth
	 *            The required width
	 * @param newHeight
	 *            The required width
	 *
	 * @return The scaled image
	 */
	public static BufferedImage scaleImage(BufferedImage image, int imageType, int newWidth, int newHeight) {
		// Make sure the aspect ratio is maintained, so the image is not distorted
		double thumbRatio = (double) newWidth / (double) newHeight;
		int imageWidth = image.getWidth(null);
		int imageHeight = image.getHeight(null);
		double aspectRatio = (double) imageWidth / (double) imageHeight;

		if (thumbRatio < aspectRatio) {
			newHeight = (int) (newWidth / aspectRatio);
		} else {
			newWidth = (int) (newHeight * aspectRatio);
		}

		// Draw the scaled image
		BufferedImage newImage = new BufferedImage(newWidth, newHeight, imageType);
		Graphics2D graphics2D = newImage.createGraphics();
		graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		graphics2D.drawImage(image, 0, 0, newWidth, newHeight, null);

		return newImage;
	}
	
	/** Determines if image splitting is necessary, according to the 'maxRatio' defined
	 * 
	 * @param image Source image
	 * @param availableHeight Available height in the document
	 * @return Boolean
	 */
	private static boolean requiresSplitting(BufferedImage image, int availableHeight) {
		// float width = image.getWidth();
		float height = image.getHeight();
		boolean requiresSplitting = (height / availableHeight) > maxRatio;
		return requiresSplitting;
	}

	/** Useful to get only a part of a large image (e.g. when you cannot get/generate sub-diagrams from a model...)
	 * 
	 * @param src
	 * 		Source image
	 * @param rect
	 * 		a java.awt.Rectangle Rectangle
	 * @return Part of the source image, as defined by the Rectangle
	 */
	
	public static BufferedImage cropImage(BufferedImage src, Rectangle rect) {
		BufferedImage dest = src.getSubimage(rect.x, rect.y, rect.width, rect.height);
		return dest;
	}

}
